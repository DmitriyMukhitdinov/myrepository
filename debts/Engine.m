//
//  Engine.m
//  debts
//
//  Created by Evgeny on 09.11.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "Engine.h"
#import "PurchaseHelper.h"
#import "AdViewController.h"

@implementation Engine
+ (instancetype)appEngine {
    static dispatch_once_t once;
    static Engine *singleton;
    dispatch_once(&once, ^ {
        singleton = [[Engine alloc] init];
        [singleton loadProducts];
    });
    
    return singleton;
}

- (void)createIAdBannerView {
    CGRect bounds = [[UIScreen mainScreen] bounds];
    ADBannerView *_banner = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
    _banner.frame = CGRectMake(0.0, bounds.size.height, bounds.size.width, _banner.frame.size.width);
    _banner.delegate = self;
    _banner.tag = 12;
    _banner.hidden = YES;
    _bannerView = _banner;
}

- (void)createGADBanner {
    
}

- (void)loadProducts {
    [[PurchaseHelper shared] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            self.products = products;
        }
    }];
}

- (BOOL)isPaid {
    return YES;
//    return [[PurchaseHelper shared] productPurchased:@"ru.izobov.debts.fullversion"];
}

- (void)layoutBanner {
    AdViewController *viewController = (AdViewController *)[[[[UIApplication sharedApplication] delegate] window ]rootViewController];
    [viewController layoutBanner];
}

#pragma mark - iAd delegate methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"Did load");
    if ([banner isHidden]) {
        AdViewController *viewController = (AdViewController *)[[[[UIApplication sharedApplication] delegate] window ]rootViewController];
        CGRect bounds = [[UIScreen mainScreen] bounds];
        CGRect bannerFrame = CGRectMake(0.0,
                                        bounds.size.height-self.bannerView.frame.size.height,
                                        bounds.size.width,
                                        self.bannerView.frame.size.width);
        viewController.bottomConstraint.constant = 50.0;
        banner.hidden = NO;
        [UIView animateWithDuration:0.25 animations:^{
            self.bannerView.frame = bannerFrame;
            [viewController.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }
    else [self layoutBanner];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    self.iAdFailed = YES;
    AdViewController *viewController = (AdViewController *)[[[[UIApplication sharedApplication] delegate] window ]rootViewController];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGRect bannerFrame = CGRectMake(0.0,
                                    bounds.size.height,
                                    bounds.size.width,
                                    self.bannerView.frame.size.width);
    viewController.bottomConstraint.constant = 0.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.bannerView.frame = bannerFrame;
        [viewController.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        ADBannerView *_banner = (ADBannerView *)[viewController.view viewWithTag:12];
        _banner.hidden = YES;
        [_banner removeFromSuperview];
        [viewController showiAds];
    }];
    
    NSLog(@"Did not load");
    
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    NSLog(@"View did transtion");
    [self layoutBanner];
}



@end
