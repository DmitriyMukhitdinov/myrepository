//
//  AdViewController.m
//  debts
//
//  Created by Evgeny on 09.11.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "AdViewController.h"
#import "PurchaseHelper.h"

@implementation AdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    
    if (![[Engine appEngine].bannerView isDescendantOfView:self.view] && ![Engine appEngine].iAdFailed) {
        [self.view addSubview:[Engine appEngine].bannerView];
        [self.view bringSubviewToFront:[Engine appEngine].bannerView];
    }
    else if ([Engine appEngine].iAdFailed) {
        [self showiAds];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)productPurchased:(NSNotification *)notification {
    NSString * productIdentifier = notification.object;
    NSLog(@"product %@ did purchased", productIdentifier);
    self.bottomConstraint.constant = 0;
    [self.view updateConstraints];
    [self.view layoutSubviews];
    UIView *_banner = [self.view viewWithTag:12];
    [_banner removeFromSuperview];
}


- (void)showiAds
{
    if (!self.bannerView) {
        _bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        _bannerView.rootViewController = self;
        _bannerView.delegate = self;
        _bannerView.adUnitID = @"ca-app-pub-1798585663224500/9440696878";
        _bannerView.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height+25.0f);
        GADRequest *request = [GADRequest request];
        //request.testDevices = @[@"3bcad2d3abe3c9529d99ee78c0cf3597"];
        [_bannerView loadRequest:request];
        _bannerView.tag = 12;
        
    }
}

- (void)addGADBannerView {
    [self.view addSubview:[Engine appEngine].bannerView];
}

- (void)layoutBanner
{

}

#pragma mark - AdMob delegate
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    
}


- (void)adViewDidReceiveAd:(GADBannerView *)view {
    if (![self.bannerView isDescendantOfView:self.view]) {
        [self.view addSubview:_bannerView];
        CGRect bounds = [[UIScreen mainScreen] bounds];
        CGRect bannerFrame = CGRectMake(0.0,
                                        bounds.size.height-self.bannerView.frame.size.height,
                                        bounds.size.width,
                                        50.0f);
        self.bottomConstraint.constant = 50.0;
        [UIView animateWithDuration:0.25 animations:^{
            self.bannerView.frame = bannerFrame;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }
}


- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"adView:didFailToReceiveAdWithError:%@", [error localizedDescription]);
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"AdMob" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    [alertView show];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGRect bannerFrame = CGRectMake(0.0,
                                    bounds.size.height,
                                    bounds.size.width,
                                    self.bannerView.frame.size.width);
    self.bottomConstraint.constant = 0.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.bannerView.frame = bannerFrame;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {

    }];
}


@end
