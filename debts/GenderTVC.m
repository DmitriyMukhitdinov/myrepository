//
//  GenderTVC.m
//  debts
//
//  Created by Evgeny on 10.04.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "GenderTVC.h"

@interface GenderTVC ()

@end

@implementation GenderTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Укажите пол контрагента";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    if (indexPath.row==0) {
        cell.textLabel.text = @"Мужской";
        if (self.contractor.sex && [self.contractor.sex intValue] == 1) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    else{
        cell.textLabel.text = @"Женский";
        if (self.contractor.sex && [self.contractor.sex intValue] == 0) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = FONT(14.0f);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *number = nil;;
    if (indexPath.row==0) {
        number = [[NSNumber alloc] initWithBool:YES];
    }
    else{
        number = [[NSNumber alloc] initWithBool:NO];
    }
    self.contractor.sex = number;
    [self.tableView reloadData];
    
    if (self.isModal) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
