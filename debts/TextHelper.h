//
//  TextHelper.h
//  debts
//
//  Created by Evgeny on 10.02.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextHelper : NSObject
+ (NSString *)returnDateStringForDebtType:(DebtType)debtType;
+ (NSString *)debtStatusCellForDebtType:(DebtType)debtType withGender:(NSNumber *)gender;
+ (NSString *)titleForNotoficationForDebtType:(DebtType)debtType debt:(Debenture *)debt;
@end
