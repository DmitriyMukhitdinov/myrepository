//
//  Rate.m
//  debts
//
//  Created by Evgeny on 02.02.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "Rate.h"
#import "AFNetworking.h"
#import "Debenture.h"
#import "Amount.h"

@implementation Rate
@synthesize queue = _queue;


#pragma mark - Singleton

+ (id)sharedInstance
{
    static Rate *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        sharedInstance.queue = [NSOperationQueue new];
        sharedInstance.queue.maxConcurrentOperationCount = 8;
        sharedInstance.loadedCodes = [NSMutableArray new];
    });
    return sharedInstance;
}

#pragma mark - Helpers

- (void)setNetworkActivityIndicatorVisible:(BOOL)setVisible {
    static NSInteger NumberOfCallsToSetVisible = 0;
    if (setVisible)
        NumberOfCallsToSetVisible++;
    else
        NumberOfCallsToSetVisible--;
    
    // The assertion helps to find programmer errors in activity indicator management.
    // Since a negative NumberOfCallsToSetVisible is not a fatal error,
    // it should probably be removed from production code.
    
    //TODO: ONLY DEBUG
    NSAssert(NumberOfCallsToSetVisible >= 0, @"Network Activity Indicator was asked to hide more often than shown");
    
    // Display the indicator as long as our static counter is > 0.
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:(NumberOfCallsToSetVisible > 0)];
}


 
#pragma mark - Rates
 
- (NSNumber *)rateForCurrencyCode:(NSString *)code
{
    NSNumber *rate = [[DataStore sharedStore] getObjectforKey:code inFile:kPlistRates];
    if (!rate) {
        [self updateRateWithCode:code];
    }
    return rate;
}

- (NSOperation *)rateOperationForCode:(NSString *)code
{
    NSString *urlString = [NSString stringWithFormat:@"http://rate-exchange.appspot.com/currency?from=%@&to=%@", code, [Preferences defaultCurrencyCode]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [self setNetworkActivityIndicatorVisible:YES];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSNumber *rate = [NSNumber numberWithFloat:[[responseObject objectForKey:@"rate"]floatValue]];
        [[DataStore sharedStore] setObject:rate forKey:code inFile:kPlistRates];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ratesUpdated" object:nil];
        [self setNetworkActivityIndicatorVisible:NO];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ratesUpdateFailed" object:nil];
        [self setNetworkActivityIndicatorVisible:NO];
    }];
    return operation;
}


- (void)updateAllRates
{
    NSArray *allDebts = [Debenture MR_findAll];
    NSMutableSet *set = [[NSMutableSet alloc] init];
    for (Debenture *debt in allDebts) {
        if (debt.total.code) {
            [set addObject:debt.total.code];
        }
    }
    NSMutableArray *operationsArray = [NSMutableArray new];
    for (NSString *code in set) {
        [operationsArray addObject:[self rateOperationForCode:code]];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ratesStartUpdate" object:nil];
    NSArray *operations = [AFURLConnectionOperation batchOfRequestOperations:operationsArray progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations)
                           {
                               //Счетчик завершенных оперераций
                               NSLog(@"NETWORK MANAGER: %u of %u complete", numberOfFinishedOperations, totalNumberOfOperations);
                               
                           } completionBlock:^(NSArray *operations)
                           {
                               //[self setUpRatesPlist];
                               //[[NSNotificationCenter defaultCenter] postNotificationName:@"ratesUpdated" object:nil];
                           }];
    [self.queue addOperations:operations waitUntilFinished:NO];
}



- (void)updateRateWithCode:(NSString *)code
{
    if (![self.loadedCodes containsObject:code]) {
        [self.loadedCodes addObject:code];
        NSOperation *operation = [self rateOperationForCode:code];
        [self.queue addOperation:operation];
    }
}

- (void)deletePList
{
    [[DataStore sharedStore]clearStoreForFile:kPlistRates];
    [self.loadedCodes removeAllObjects];
}

/*
- (void)setUpRatesPlist
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory =  [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"rates.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ( ![fileManager fileExistsAtPath:path] ) {
        NSLog(@"copying database to users documents");
        NSString *pathToSettingsInBundle = [[NSBundle mainBundle] pathForResource:@"rates" ofType:@"plist"];
        [fileManager copyItemAtPath:pathToSettingsInBundle toPath:path error:&error];
    }
    //if file is already there do nothing
    else {
        NSLog(@"users database already configured");
    }
}
*/
@end
