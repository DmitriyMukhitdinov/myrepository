//
//  AssetsTableViewController.m
//  debts
//
//  Created by Evgeny on 15.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

typedef enum AssetCurrentStatus{
    AssetCurrentStatusMineBusy,
    AssetCurrentStatusMineFree,
    AssetCurrentStatusSomebodyBusy,
    AssetCurrentStatusSomebodyFree
}AssetCurrentStatus;


#import "AssetsTVC.h"
#import "AssetCellViewCell.h"
#import "Debenture.h"
#import "DTTableViewController.h"
#import "DebtVC.h"

@interface AssetsTVC ()
@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, strong) NSString *ownerFilter;
@end

@implementation AssetsTVC
@synthesize delegate;

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 60.0f;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    __block NSInteger foundIndex = NSNotFound;
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DebtVC class]]) {
            foundIndex = idx;
            // stop the enumeration
            *stop = YES;
        }
    }];
    
    if (foundIndex != NSNotFound) {
        // You've found the first object of that class in the array
        self.navigationItem.prompt = @"выберите имущество";
        self.tableView.contentInset = UIEdgeInsetsMake(30.0f, 0, 0, 0);
    }
    
    
    /*
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc] initWithItems:@[LSTRING(@"mine"), LSTRING(@"somebody"),LSTRING(@"all together")]];
    segmentControl.frame = CGRectMake(ksViewsLeftRightMargin, ksViewsTopMargin/2, ksViewsWidth, 30.0f);
    [segmentControl setSelectedSegmentIndex:2];
    [segmentControl addTarget:self
                       action:@selector(filterTable:)
             forControlEvents:UIControlEventValueChanged];
    segmentControl.tintColor = [UIColor colorFromHexString:kMainColor];
    [self.view addSubview:segmentControl];
    */
    
    // Remove left offcet on cells Only iOS7
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.tableView.rowHeight = 60.0f;
    
    if (!self.isNoSelect){
        self.isEditing = NO;
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 61.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Select") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(editTable:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
    }
    else self.isEditing = YES;
    [self updateTable];
    self.title = @"Имущество";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIBarButtonItem *button = self.navigationItem.rightBarButtonItem;
    if (self.isEditing){
        [button setTitle:NSLocalizedString(@"Edit", @"")];
    }
    else{
        [button setTitle:NSLocalizedString(@"Select", @"")];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AssetVC"]) {
        AssetVC *assetVC = segue.destinationViewController;
        assetVC.delegate = self;
        if(sender)assetVC.asset = self.tableData[((NSIndexPath *)sender).row];
    }
}


#pragma mark - Table view data source


- (void) updateTable
{
    if (self.ownerFilter == nil) {
        self.tableData = [Asset MR_findAllSortedBy:@"title" ascending:YES];
    }
    else{
        if ([self.ownerFilter isEqualToString:@"me"]) {
            self.tableData =  [Asset MR_findAllSortedBy:@"title"
                                              ascending:YES
                                          withPredicate:[NSPredicate predicateWithFormat:@"owner =%@", [AppHelper contractorMe]]];
        }
        else{
            self.tableData = [Asset MR_findAllSortedBy:@"title"
                                             ascending:YES
                                         withPredicate:[NSPredicate predicateWithFormat:@"owner !=%@", [AppHelper contractorMe]]];
        }
    }
    
    if (self.tableData.count==0){
        self.isEditing = YES;
    }
    else{
        if (!self.isNoSelect) {
            self.isEditing = NO;
        }
    }
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.tableData.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Select last row in table
    if (indexPath.section == tableView.numberOfSections-1 &&
        indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1)
    {
        //Строка "Добавить"
        static NSString *cellIdentifier = @"addRowCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (nil == cell)
        {
            UINib *customCellNib = [UINib nibWithNibName:@"addRow" bundle:nil];
            [self.tableView registerNib:customCellNib forCellReuseIdentifier:cellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        UILabel *label = (UILabel *)[cell viewWithTag:1010];
        label.text = @"Добавить новое имущество";
        return cell;
    }
    else{
        AssetCellViewCell *cell;
        Asset *asset = self.tableData[indexPath.row];
        //Photos *photo = [[asset.photos allObjects] lastObject];
        static NSString *cellIdentifier = @"AssetCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (nil == cell)
        {
            //TODO: move to viewDidLoad
            UINib *customCellNib = [UINib nibWithNibName:@"AssetCellPhoto" bundle:nil];
            [self.tableView registerNib:customCellNib forCellReuseIdentifier:cellIdentifier];
            cell = (AssetCellViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        if (asset.photo) {
            dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(concurrentQueue, ^{
                UIImage *image = [UIImage imageFromDocsByName:asset.photo];
                if (image != nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell.photoImageView setImage:image];
                    });
                }
            });
        }
        
        if (self.isEditing) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.titleLabel.text = asset.title;
        cell.backgroundColor = [UIColor whiteColor];
        
        if (asset.history.count>0) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deadline" ascending:NO];
            Debenture *debt = [asset.history sortedArrayUsingDescriptors:@[sortDescriptor]][0]; //Последний долг
            
            
            if ([debt.finished isEqual:[NSNumber numberWithInt:1]]) {
                //Находится у владельца
                cell.dateLabel.text = @"";
                if (asset.owner == [AppHelper contractorMe])
                    cell.contractorLabel.text = @"у меня";
                else{
                    NSString *name = [NSString stringWithFormat:@"владелец %@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                    cell.contractorLabel.text = name;
                }
            }
            else{
                //Вещь находится у контрагента
                if ([debt.deadline timeIntervalSince1970]<[[NSDate date] timeIntervalSince1970] && debt.finished!=[NSNumber numberWithInt:1]) {
                    //прострочено
                    cell.backgroundColor = [UIColor colorFromHexString:kOutdatedCellColor];
                    cell.dateLabel.highlighted = YES;
                    cell.titleLabel.highlighted = YES;
                    cell.contractorLabel.highlighted = YES;
                }
                else{
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.dateLabel.highlighted = NO;
                    cell.titleLabel.highlighted = NO;
                    cell.contractorLabel.highlighted = NO;
                }
            
                if ([debt debtType] == DebtTypeAssetSomeone) {
                    cell.contractorLabel.text = @"у меня";
                }
                else{
                    NSString *name = [NSString stringWithFormat:@"у %@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                    cell.contractorLabel.text = name;
                }
                cell.dateLabel.text = [[AppHelper dateFormatter] stringFromDate:debt.deadline];
            }
        }
        else{
            //Находится у владельца
            cell.dateLabel.text = @"";
            if (asset.owner == [AppHelper contractorMe])
                cell.contractorLabel.text = @"у меня";
            else
            {
                NSString *name = [NSString stringWithFormat:@"владелец %@ %@", (asset.owner.name!=nil)?asset.owner.name:@" ", (asset.owner.lastName!=nil)?asset.owner.lastName:@" "];
                cell.contractorLabel.text = name;
            }
        }
        return cell;
    }
}


#pragma mark - UI Table View delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Select last row in table
    if (indexPath.section == tableView.numberOfSections-1 &&
        indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1)
    {
        //Выбрана последняя ячейка в режиме редактирования
        [self performSegueWithIdentifier:@"AssetVC" sender:nil];
    }
    else if(!self.isEditing){
        //Режим выбора
        //Проверка не занятя ли вещь
        AssetCurrentStatus status = [self currentStatus:self.tableData[indexPath.row]];
        if (!(self.debtType == DebtTypeAssetSomeone && status == AssetCurrentStatusMineFree) &&
            !(self.debtType == DebtTypeAssetMine && status == AssetCurrentStatusSomebodyFree)
            ) {
            if (status != AssetCurrentStatusMineBusy &&
                status != AssetCurrentStatusSomebodyBusy) {
                //Если вещь не находится у кого то в долгу
                [self.delegate didSelectAsset:self.tableData[indexPath.row]];
                if (self.isModal) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else{
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
    }
    else{
        [self performSegueWithIdentifier:@"AssetVC" sender:indexPath];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - IB Actions
- (void)setIsEditing:(BOOL)isEditing
{
    _isEditing = isEditing;
    UIBarButtonItem *button = self.navigationItem.rightBarButtonItem;
    BarButton *btn = (BarButton *)button.customView;
    if (isEditing) {
        [btn setTitle:LSTRING(@"Select") forState:UIControlStateNormal];
    }
    else{
        [btn setTitle:LSTRING(@"Edit") forState:UIControlStateNormal];
    }
}

- (void) editTable:(id)sender
{
    [self.tableView reloadData];
    self.isEditing = (self.isEditing)?NO:YES;
    return;
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self.tableView indexPathsForVisibleRows]];
    if (self.isEditing)
    {
        self.isEditing = NO;
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.tableData.count inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        [array removeLastObject];
    }
    else
    {
        self.isEditing = YES;
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.tableData.count inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
    }
    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
}



- (void)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void) filterTable:(id)sender
{
    UISegmentedControl * segmentedControl = (UISegmentedControl *)sender;
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            self.ownerFilter = @"me";
            break;
        case 1:
            self.ownerFilter = @"somebody";
            break;
        default:
            self.ownerFilter = nil;
            break;
    }
    [self updateTable];
}

#pragma mark - Asset status
- (AssetCurrentStatus) currentStatus:(Asset *)asset
{
    if (asset.history.count>0) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deadline" ascending:NO];
        Debenture *debt = [asset.history sortedArrayUsingDescriptors:@[sortDescriptor]][0];
        if (debt.refunds.count>0) {
            //Был возврат
            //Находится у владельца
            if (asset.owner == [AppHelper contractorMe])
                return AssetCurrentStatusMineFree;
            else
                return AssetCurrentStatusSomebodyFree;
        }
        else{
            //Вещь находится у контрагента
            if (debt.contractor == [AppHelper contractorMe]) {
                return AssetCurrentStatusMineBusy;
            }
            else{
                return AssetCurrentStatusSomebodyBusy;
            }
        }
    }
    else{
        //Находится у владельца
        if (asset.owner == [AppHelper contractorMe])
            return AssetCurrentStatusMineFree;
        else
            return AssetCurrentStatusSomebodyFree;
        
    }
}

 
#pragma mark - Asset View Delegate
 
- (void)didSaveAsset
{
    if (!self.isNoSelect) {
        self.isEditing = NO;
    }
    
    [self updateTable];
}

@end
