//
//  TextHelper.m
//  debts
//
//  Created by Evgeny on 10.02.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "TextHelper.h"
#import "Debenture.h"
#import "Contractor.h"
#import "Asset.h"
#import "Amount.h"

@implementation TextHelper
+ (NSString *)returnDateStringForDebtType:(DebtType)debtType
{
    NSString *returnDateString = nil;
    switch (debtType) {
        case DebtTypeMoneyMine:
            returnDateString = NSLocalizedString(@"money return date", @"");
            break;
        case DebtTypeMoneySomeone:
            returnDateString = NSLocalizedString(@"asset return date", @"");
            break;
        case DebtTypeAssetMine:
            returnDateString = NSLocalizedString(@"money return date", @"");
            break;
        case DebtTypeAssetSomeone:
        default:
            returnDateString = NSLocalizedString(@"asset return date", @"");
            break;
    }
    return returnDateString;
}

+ (NSString *)debtStatusCellForDebtType:(DebtType)debtType withGender:(NSNumber *)gender
{
    NSString *returnDateString = nil;
    switch (debtType) {
        case DebtTypeMoneyMine:
            if ([gender isEqualToNumber:[NSNumber numberWithInt:0]]) {
                returnDateString = NSLocalizedString(@"she someone money", @"");
            }
            else returnDateString = NSLocalizedString(@"someone money", @"");
            break;
        case DebtTypeMoneySomeone:
            if ([gender isEqualToNumber:[NSNumber numberWithInt:0]])
                returnDateString = NSLocalizedString(@"she i money", @"");
            else returnDateString = NSLocalizedString(@"i money", @"");
            break;
        case DebtTypeAssetMine:
            if ([gender isEqualToNumber:[NSNumber numberWithInt:0]])
                returnDateString = NSLocalizedString(@"she someone asset", @"");
            else returnDateString = NSLocalizedString(@"someone asset", @"");
            break;
        case DebtTypeAssetSomeone:
        default:
            if ([gender isEqualToNumber:[NSNumber numberWithInt:0]])
                returnDateString = NSLocalizedString(@"she i take asset", @"");
            else returnDateString = NSLocalizedString(@"i take asset", @"");
            break;
    }
    return returnDateString;
}


+ (NSString *)titleForNotoficationForDebtType:(DebtType)debtType debt:(Debenture *)debt
{
    NSString *eventTitle = nil;
    switch (debtType) {
        case DebtTypeMoneyMine:
            if ([debt.contractor isWoman]) {
                eventTitle = [NSString stringWithFormat:LSTRING(@"ShePushReturnToMe"),debt.contractor.name,
                              [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber: debt.total.amount]];
            }
            else eventTitle = [NSString stringWithFormat:LSTRING(@"PushReturnToMe"),debt.contractor.name,
                          [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber: debt.total.amount]];
            break;
        case DebtTypeMoneySomeone:
            if ([debt.contractor isWoman]) {
                eventTitle = [NSString stringWithFormat:LSTRING(@"ShePushIReturn"),debt.contractor.name,
                                   [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber: debt.total.amount]];
            }
            else eventTitle = [NSString stringWithFormat:LSTRING(@"PushIReturn"),debt.contractor.name,
                          [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber: debt.total.amount]];
            break;
        case DebtTypeAssetMine:
            if ([debt.contractor isWoman]) {
                eventTitle = [NSString stringWithFormat:LSTRING(@"ShePushReturnToMe"),debt.contractor.name, debt.asset.title];
            }
            else eventTitle = [NSString stringWithFormat:LSTRING(@"PushReturnToMe"),debt.contractor.name, debt.asset.title];
            break;
        case DebtTypeAssetSomeone:
            if ([debt.contractor isWoman]) {
                eventTitle = [NSString stringWithFormat:LSTRING(@"ShePushIReturn"),debt.contractor.name, debt.asset.title];
            }
            else eventTitle = [NSString stringWithFormat:LSTRING(@"PushIReturn"),debt.contractor.name, debt.asset.title];
            break;
        default:
            
            break;
    }
    eventTitle = [eventTitle stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    return eventTitle;
}

@end
