//
//  RedButton.m
//  debts
//
//  Created by Evgeny on 01.02.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "RedButton.h"

@implementation RedButton

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    UIImage *image = [[UIImage imageNamed:@"redButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(8.0f, 9.0f, 8.0f, 9.0f)];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    //[self setBackgroundImage:image forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f]];
}

@end
