//
//  DebtCellPhoto.m
//  debts
//
//  Created by Evgeny on 05.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DebtCellPhoto.h"

@implementation DebtCellPhoto
@synthesize photoImageView = _photoImageView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
