//
//  PushTracking.h
//  debts
//
//  Created by Evgeny on 16.03.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Debenture.h"

@interface PushTracking : NSObject

+ (void)createPushWithText:(NSString *)pushText
                  fireDate:(NSDate *)fireDate
                  userInfo:(id)userInfo;

+ (void)cancelPushWithWserInfo:(id)userInfo;
+ (void)changeDateForPushWithUserInfo:(id)userInfo
                           toFireDate:(NSDate *)newFireDate;

+ (Debenture *)debtForUserInfo:(id)userInfo;

@end
