//
//  TabTypes.m
//  debts
//
//  Created by Evgeny on 15.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "TabTypes.h"

NSString * const DebtType_toString[] = {
    [DebtTypeMoneyMine] = @"DebtTypeMoneyMine",
    [DebtTypeMoneySomeone] = @"DebtTypeMoneySomeone",
    [DebtTypeAssetMine] = @"DebtTypeAssetMine",
    [DebtTypeAssetSomeone] = @"DebtTypeAssetSomeone"
};


@implementation TabTypes

@end
