//
//  DebtCellPhoto.h
//  debts
//
//  Created by Evgeny on 05.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DebtCell.h"

@interface DebtCellPhoto : DebtCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end
