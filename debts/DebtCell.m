//
//  DebtCell.m
//  debts
//
//  Created by Evgeny on 05.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DebtCell.h"

@implementation DebtCell
@synthesize nameLabel = _nameLabel;
@synthesize dateLabel = _dateLabel;
@synthesize amountLabel = _amountLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
