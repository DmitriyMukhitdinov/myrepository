//
//  GenderTVC.h
//  debts
//
//  Created by Evgeny on 10.04.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contractor.h"
#import "DTTableViewController.h"

@interface GenderTVC : DTTableViewController
@property (nonatomic, strong) Contractor *contractor;
@end
