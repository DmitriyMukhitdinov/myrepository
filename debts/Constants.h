//
//  Config.h
//  debts
//
//  Created by Evgeny on 13.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

//Views
#define ksViewsWidth 300.0f
#define ksViewsLeftRightMargin 10.0f
#define ksViewsTopMargin 10.0f

//Images
#define  kUserImageSize 100.0f
#define  kAssetImageSize 70.0f

//Contractor types
#define kContractorMe @"me"
#define kContractorSomebody @"somebody"

//Colors
#define kMainColor          @"#88be32"
#define kGrayColor          @"#f2f4f6"
#define kDarkGrayColor      @"#8e979e"
#define kSeparatorColor     @"#d7dbdd"
#define kOutdatedCellColor  @"#d44545"

//Fonts
#define FONT_UL(s)  [UIFont fontWithName:@"HelveticaNeue-UltraLight"  size:s]
#define FONT_M(s)   [UIFont fontWithName:@"HelveticaNeue-Medium"      size:s]
#define FONT_B(s)   [UIFont fontWithName:@"HelveticaNeue-Bold"        size:s]
#define FONT(s)     [UIFont fontWithName:@"HelveticaNeue"             size:s]
#define FONT_L(s)   [UIFont fontWithName:@"HelveticaNeue-Light"       size:s]