//
//  AppHelper.h
//  debts
//
//  Created by Evgeny on 21.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Contractor.h"



@interface AppHelper : NSObject

+ (Contractor *)contractorMe;
+ (NSNumberFormatter *)numberFormatter;
+ (NSNumberFormatter *)currencyFormatterWithCode:(NSString *)code;
+ (NSDate *)combineDate:(NSDate *)date withTime:(NSDate *)time;
+ (NSDateFormatter *)dateFormatter;
+ (NSString *)localeDecimalSeparator;
+ (void)pushViewControllerForDebt:(Debenture *)debt;
+ (UIViewController*) topMostController;
+ (NSString *)russianMonthFix:(NSString *)dateString;
+ (NSString *)makeUniqueString;
+ (NSNumberFormatter *)currencyFormatter;


@end
