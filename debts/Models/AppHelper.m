//
//  AppHelper.m
//  debts
//
//  Created by Evgeny on 21.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "AppHelper.h"
#import "Preferences.h"
#import "AFNetworking.h"
#import "Debenture.h"
#import "Amount.h"
#import "DebtVC.h"

@implementation AppHelper

+ (Contractor *)contractorMe
{
    return [Contractor MR_findFirstByAttribute:@"type" withValue:@"me"];
}

+ (NSNumberFormatter *)numberFormatter
{
    static NSNumberFormatter *_numberFormatter = nil;
    if (!_numberFormatter) {
        _numberFormatter = [[NSNumberFormatter alloc] init];
        [_numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    }
    if ([Preferences isDecimalEnabled]) {
        [_numberFormatter setPositiveFormat:@"#,##0.00"];
        [_numberFormatter setNegativeFormat:@"#,##0.00"];
    }
    else{
        [_numberFormatter setPositiveFormat:@"#0"];
        [_numberFormatter setNegativeFormat:@"#0"];
    }
    return _numberFormatter;
}

+ (NSNumberFormatter *)currencyFormatterWithCode:(NSString *)code
{
    static NSNumberFormatter *_currencyFormatter = nil;
    if (!_currencyFormatter) {
        _currencyFormatter = [[NSNumberFormatter alloc] init];
        [_currencyFormatter setLocale:[NSLocale currentLocale]];
        [_currencyFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    }
    
    if (code) {
        if ([code isEqualToString:@""]) {
            [_currencyFormatter setCurrencySymbol:@""];
        }
        _currencyFormatter.currencyCode = code;
    }
    else{
        _currencyFormatter.currencyCode = [Preferences defaultCurrencyCode];
    }
    
    
    if ([Preferences isDecimalEnabled]) {
        [_currencyFormatter setMaximumFractionDigits:2];
    }
    else{
        [_currencyFormatter setMaximumFractionDigits:0];
    }
    return _currencyFormatter;
}

+ (NSNumberFormatter *)currencyFormatter
{
    if ([Preferences isDecimalEnabled]) {
        static NSNumberFormatter *_currencyFormatter1 = nil;
        if (!_currencyFormatter1) {
            _currencyFormatter1 = [[NSNumberFormatter alloc] init];
            [_currencyFormatter1 setLocale:[NSLocale currentLocale]];
            [_currencyFormatter1 setFormatterBehavior: NSNumberFormatterBehavior10_4];
            [_currencyFormatter1 setCurrencySymbol:@""];
            [_currencyFormatter1 setNumberStyle:NSNumberFormatterCurrencyStyle];
            [_currencyFormatter1 setMaximumFractionDigits:2];
        }
        
        return _currencyFormatter1;
    }
    else{
        static NSNumberFormatter *_currencyFormatter2 = nil;
        if (!_currencyFormatter2) {
            _currencyFormatter2 = [[NSNumberFormatter alloc] init];
            [_currencyFormatter2 setLocale:[NSLocale currentLocale]];
            [_currencyFormatter2 setFormatterBehavior: NSNumberFormatterBehavior10_4];
            [_currencyFormatter2 setCurrencySymbol:@""];
            [_currencyFormatter2 setNumberStyle:NSNumberFormatterCurrencyStyle];
            [_currencyFormatter2 setMaximumFractionDigits:0];
        }
        
        return _currencyFormatter2;
    }
    
}

+ (NSString *)localeDecimalSeparator
{
    return [[NSLocale currentLocale] objectForKey:NSLocaleDecimalSeparator];
}


+ (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *_dateFormatter = nil;
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        //_dateFormatter.dateFormat = kDateFormat;
        [_dateFormatter setLocale:[NSLocale currentLocale]];
        NSString *dateFmt   = [NSDateFormatter dateFormatFromTemplate: @"ddMMyyyy"
                                                              options: 0
                                                               locale: [NSLocale currentLocale]];
        
        //[_dateFormatter setDateStyle:NSDateFormatterShortStyle];//set current locale
        //[_dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        [_dateFormatter setDateFormat:dateFmt];
    }
    return _dateFormatter;
}


+ (NSDate *)combineDate:(NSDate *)date withTime:(NSDate *)time {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents *dateComponents = [gregorian components:unitFlagsDate fromDate:date];
    unsigned unitFlagsTime = NSHourCalendarUnit | NSMinuteCalendarUnit |  NSSecondCalendarUnit;
    NSDateComponents *timeComponents = [gregorian components:unitFlagsTime fromDate:time];
    
    [dateComponents setSecond:[timeComponents second]];
    [dateComponents setHour:[timeComponents hour]];
    [dateComponents setMinute:[timeComponents minute]];
    
    NSDate *combDate = [gregorian dateFromComponents:dateComponents];
    
    return combDate;
}


+ (void)pushViewControllerForDebt:(Debenture *)debt
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_Storyboard"
                                                         bundle:[NSBundle mainBundle]];
    DebtVC *debViewController;
    if ([debt.type isEqualToString:DebtType_toString[DebtTypeMoneyMine]] || [debt.type isEqualToString:DebtType_toString[DebtTypeMoneySomeone]]) {
        debViewController = [storyboard instantiateViewControllerWithIdentifier:@"DebtScene"];
    }
    else{
        debViewController = [storyboard instantiateViewControllerWithIdentifier:@"DebtScene"];
    }
    
    DebtType types[4] = {DebtTypeMoneyMine, DebtTypeMoneySomeone, DebtTypeAssetMine, DebtTypeAssetSomeone};
    for (int i=0; i<4; i++) {
        if ([debt.type isEqualToString:DebtType_toString[types[i]]]) {
            debViewController.type = types[i];
        }
    }
    debViewController.isModal = YES;
    debViewController.debt = debt;
    MainNavigationController *navController = [[MainNavigationController alloc] initWithRootViewController:debViewController];
    [[AppHelper topMostController] presentViewController:navController animated:YES completion:nil];
}

+ (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

 
#pragma mark - Month fix
 
+ (NSString *)russianMonthFix:(NSString *)dateString
{
    dateString = [dateString lowercaseString];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"января" withString:@"январь"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"февраля" withString:@"февраль"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"марта" withString:@"март"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"апреля" withString:@"апрель"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"мая" withString:@"май"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"июня" withString:@"июнь"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"июля" withString:@"июль"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"августа" withString:@"август"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"сентября" withString:@"сентябрь"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"октября" withString:@"октябрь"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"ноября" withString:@"ноябрь"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"декабря" withString:@"декабрь"];
    
    return [dateString uppercaseString];
}


+ (NSString *)makeUniqueString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyMMddHHmmss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    int randomValue = arc4random() % 1000;
    
    NSString *unique = [NSString stringWithFormat:@"%@.%d",dateString,randomValue];
    
    return unique;
}

@end
