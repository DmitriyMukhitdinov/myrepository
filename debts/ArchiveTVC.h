//
//  ArchiveTVC.h
//  debts
//
//  Created by Evgeny on 31.05.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTTableViewController.h"
@interface ArchiveTVC : DTTableViewController
@property (nonatomic, assign) DebtType type;
@end
