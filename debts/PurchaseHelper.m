//
//  PurchaseHelper.m
//  debts
//
//  Created by Evgeny on 09.11.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "PurchaseHelper.h"

@implementation PurchaseHelper
+ (instancetype)shared {
    static dispatch_once_t once;
    static PurchaseHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"ru.izobov.debts.fullversion",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}
@end
