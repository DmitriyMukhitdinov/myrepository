//
//  PublicDataStore.h
//  tempbank
//
/*
    Запись и чтение plist файлов
 
 */
//  Created by Evgeny Nazarov on 20.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//
#define kPlistRates      @"rates"


#import <Foundation/Foundation.h>

@interface DataStore : NSObject
@property (nonatomic, strong) NSOperationQueue *mainQueue;

+ (id)sharedStore;
- (void)writeDictionary:(NSDictionary *)dictionary toFileNamed:(NSString *)fileName;
- (NSDictionary *)dictionaryForName:(NSString *)fileName;
- (void)setObject:(id)object forKey:(NSString *)key inFile:(NSString *)file;
- (id)getObjectforKey:(NSString *)key inFile:(NSString *)file;
- (void)clearStoreForFile:(NSString *)file;
@end
