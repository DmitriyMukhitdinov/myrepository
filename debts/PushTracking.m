//
//  PushTracking.m
//  debts
//
//  Created by Evgeny on 16.03.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "PushTracking.h"


@implementation PushTracking
+ (void)createPushWithText:(NSString *)pushText
                  fireDate:(NSDate *)fireDate
                  userInfo:(id)userInfo
{
    if ([fireDate timeIntervalSince1970]<[[NSDate date] timeIntervalSince1970]) {
        //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Уведомление не создано" message:@"время выполнения меньше текущего" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alert show];
    }
    else{
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = [AppHelper combineDate:fireDate withTime:[Preferences defaultTime]];;
        notification.alertBody = pushText;
        notification.userInfo = userInfo;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        
        NSString *dateString = [dateFormatter stringFromDate:fireDate];
        NSString *alertMessage = [NSString stringWithFormat:@"время: %@", dateString];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Уведомление создано"
//                                                       message:alertMessage
//                                                      delegate:nil
//                                             cancelButtonTitle:@"OK"
//                                             otherButtonTitles:nil];
//        [alert show];
    }
}

+ (void)cancelPushWithWserInfo:(id)userInfo
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *pushesArray = [app scheduledLocalNotifications];
    for (int i=0; i<[pushesArray count]; i++)
    {
        UILocalNotification* pushNotification = [pushesArray objectAtIndex:i];
        if ([[pushNotification.userInfo objectForKey:@"debtID"] isEqualToString:[userInfo objectForKey:@"debtID"]])
        {
            [[UIApplication sharedApplication] cancelLocalNotification:pushNotification];
            break;
        }
    }
}

+ (void)changeDateForPushWithUserInfo:(id)userInfo
                           toFireDate:(NSDate *)newFireDate
{
    Debenture *debt = [Debenture MR_findFirstByAttribute:@"debtID" withValue:[userInfo objectForKey:@"debtID"]];
    if (debt && debt.finished != [NSNumber numberWithInteger:1]) {
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *pushesArray = [app scheduledLocalNotifications];
        UILocalNotification *notification = nil;
        for (int i=0; i<[pushesArray count]; i++)
        {
            UILocalNotification* pushNotification = [pushesArray objectAtIndex:i];
            if ([[pushNotification.userInfo objectForKey:@"debtID"] isEqualToString:[userInfo objectForKey:@"debtID"]])
            {
                notification = pushNotification;
                break;
            }
        }
        if (notification) {
            [app cancelLocalNotification:notification];
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [AppHelper combineDate:debt.deadline withTime:[Preferences defaultTime]];
            localNotification.alertBody = [TextHelper titleForNotoficationForDebtType:[debt debtType] debt:debt];;
            localNotification.alertAction = @"Перейти";
            NSDictionary *info = @{@"debtID": debt.debtID};
            localNotification.userInfo = info;
            
            if ([localNotification.fireDate timeIntervalSince1970]>[[NSDate date] timeIntervalSince1970]) {
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                NSDateFormatter *dateFormatter = [NSDateFormatter new];
                dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
//                NSString *dateString = [dateFormatter stringFromDate:localNotification.fireDate];
//                NSString *alertMessage = [NSString stringWithFormat:@"время: %@", dateString];
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Уведомление создано"
//                                                               message:alertMessage
//                                                              delegate:nil
//                                                     cancelButtonTitle:@"OK"
//                                                     otherButtonTitles:nil];
//                [alert show];
            }
        }
        else{
            [PushTracking createPushWithText:[TextHelper titleForNotoficationForDebtType:[debt debtType]
                                                                                    debt:debt]
                                    fireDate:debt.deadline
                                    userInfo:@{@"debtID": debt.debtID}];
        }
    }
}

+ (Debenture *)debtForUserInfo:(id)userInfo
{
    Debenture *debt = [Debenture MR_findFirstByAttribute:@"debtID" withValue:[userInfo objectForKey:@"debtID"]];
    return debt;
}

@end
