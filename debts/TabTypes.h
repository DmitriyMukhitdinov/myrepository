//
//  TabTypes.h
//  debts
//
//  Created by Evgeny on 15.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum DebtType {
    DebtTypeMoneyMine,
    DebtTypeMoneySomeone,
    DebtTypeAssetMine,
    DebtTypeAssetSomeone,
} DebtType;

@interface TabTypes : NSObject



extern NSString * const DebtType_toString[];

@end
