//
//  Engine.h
//  debts
//
//  Created by Evgeny on 09.11.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>
#import "GADBannerView.h"
#import "GADBannerViewDelegate.h"

@interface Engine : NSObject <ADBannerViewDelegate, GADBannerViewDelegate>
@property (nonatomic, assign, getter=isPaid) BOOL paid;
@property (strong, nonatomic) NSArray *products;
@property (strong, nonatomic) UIView *bannerView;
@property (nonatomic, assign) BOOL iAdFailed;
+ (instancetype) appEngine;
@end
