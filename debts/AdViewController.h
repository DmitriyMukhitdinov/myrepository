//
//  AdViewController.h
//  debts
//
//  Created by Evgeny on 09.11.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import "GADBannerViewDelegate.h"
@interface AdViewController : UIViewController<GADBannerViewDelegate>
@property (nonatomic, strong) GADBannerView *bannerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
- (void)showiAds;
- (void)layoutBanner;
@end
