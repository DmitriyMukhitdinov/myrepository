//
//  CalcButton.m
//  debts
//
//  Created by Evgeny on 19.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "CalcButton.h"
#import <QuartzCore/QuartzCore.h>
@implementation CalcButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    // Decode the frame
    self = [super initWithCoder:decoder];
    [self.titleLabel setFont:FONT_UL(35.0f)];
    [self setBackgroundColor:[UIColor whiteColor]];
    [self setTitleColor:[UIColor colorFromHexString:kDarkGrayColor] forState:UIControlStateNormal];
    self.layer.borderColor = [UIColor colorFromHexString:kSeparatorColor].CGColor;
    self.layer.borderWidth = .5f;
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
