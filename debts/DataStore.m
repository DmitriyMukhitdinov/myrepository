//
//  PublicDataStore.m
//  tempbank
//
//  Created by Evgeny Nazarov on 20.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DataStore.h"

@implementation DataStore
@synthesize mainQueue = _mainQueue;


#pragma mark - Singleton

+ (id)sharedStore
{
    static DataStore *sharedStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStore = [[self alloc] init];
        sharedStore.mainQueue = [NSOperationQueue new];
        sharedStore.mainQueue.maxConcurrentOperationCount = 3;
    });
    return sharedStore;
}


#pragma mark - Helpers

- (NSString *)documentsDirectory
{
    static NSString *documentsDirectory = nil;
    if (!documentsDirectory) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    return documentsDirectory;
}


#pragma mark - Read/Write Methods

- (void)writeDictionary:(NSDictionary *)dictionary toFileNamed:(NSString *)fileName
{
   //[self.mainQueue addOperationWithBlock:^{
       NSString *plistPath = [[self documentsDirectory] stringByAppendingPathComponent:fileName];
       BOOL success = [NSKeyedArchiver archiveRootObject:dictionary toFile:plistPath];
       NSLog(@"%@ : %@", plistPath, success ? @"YES":@"NO");
   //}];
}

- (NSDictionary *)dictionaryForName:(NSString *)fileName
{
    NSString *plistPath = [[self documentsDirectory] stringByAppendingPathComponent:fileName];
    NSData * data = [NSData dataWithContentsOfFile:plistPath];
    if (data == nil) {
        NSString *plistPath = [[self documentsDirectory] stringByAppendingPathComponent:fileName];
        [NSKeyedArchiver archiveRootObject:[NSDictionary new] toFile:plistPath];
        data = [NSData dataWithContentsOfFile:plistPath];
        //data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"]];
    }
    NSDictionary *plistDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return plistDict;
}

- (void)setObject:(id)object forKey:(NSString *)key inFile:(NSString *)file
{
    NSDictionary *dict = [self dictionaryForName:file];
    NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithDictionary:dict];
    [mDict setValue:object forKey:key];
    [self writeDictionary:mDict toFileNamed:file];
}

- (id)getObjectforKey:(NSString *)key inFile:(NSString *)file
{
    NSDictionary *dict = [self dictionaryForName:file];
    return [dict objectForKey:key];
}

- (void)clearStoreForFile:(NSString *)file
{
    [self writeDictionary:[NSDictionary new] toFileNamed:file];
}

@end
