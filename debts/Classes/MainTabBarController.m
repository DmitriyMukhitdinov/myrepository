//
//  MainTabBarController.m
//  debts
//  
//  Created by Evgeny on 12.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "MainTabBarController.h"
#import "DebtsTableViewController.h"
#import "BackButton.h"

@interface MainTabBarController ()

@end

@implementation MainTabBarController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width,
                                       self.view.frame.size.height-50.0f);
    
    
    if (!self.showNavBar)[self.navigationController setNavigationBarHidden:YES animated:NO];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor colorFromHexString:kGrayColor];
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBar.barStyle = UIBarStyleBlack;
    [self.tabBar setTranslucent:NO];
    self.tabBar.tintColor = [UIColor whiteColor];
    self.tabBar.selectedImageTintColor = [UIColor whiteColor];
    self.tabBar.backgroundColor = [UIColor whiteColor];
    
    //Set Up View Controllers
    DebtType types[4] = {DebtTypeMoneyMine, DebtTypeMoneySomeone, DebtTypeAssetMine, DebtTypeAssetSomeone};

    for (int i=0; i<4; i++) {
        DebtsTableViewController *controller = self.viewControllers[i];
        controller.type = types[i];
        [controller.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -7.0f)];
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_item", DebtType_toString[types[i]]]];
        UIImage *image1 = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        NSString *title = nil;
        //Move to text helper
        switch (types[i]) {
            case DebtTypeMoneyMine:
                title = NSLocalizedString(@"own to me", @"");
                break;
            case DebtTypeMoneySomeone:
                if ([[AppHelper contractorMe] isWoman]) {
                    title = @"я должна";
                }
                else title = NSLocalizedString(@"i own", @"");
                break;
            case DebtTypeAssetMine:
                title = NSLocalizedString(@"pick up", @"");
                break;
            default:
                title = NSLocalizedString(@"give", @"");
                break;
        }
        
        controller.tabBarItem = [[UITabBarItem alloc] initWithTitle:title
                                                              image:image1
                                                      selectedImage:image1];
        controller.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -5);
        if ([controller respondsToSelector:@selector(updateBadgeValues)]) {
            [controller updateBadgeValues];
        }
        
    }
    [self setSelectedIndex:0];
    
    if (self.showNavBar) {
        BackButton *btn = [BackButton button];
        [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.leftBarButtonItem = backButton;
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if(!self.showNavBar)[self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    
    if(!self.showNavBar)[self.navigationController setNavigationBarHidden:NO animated:NO];
    [super viewWillDisappear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (self.navigationController.topViewController != self)
    {
        if(!self.showNavBar)[self.navigationController setNavigationBarHidden:NO animated:animated];
    }
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
