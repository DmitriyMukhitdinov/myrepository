//
//  CalcVC.m
//  debts
//
//  Created by Evgeny on 10.03.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "CalcVC.h"
#import "BarButton.h"


@interface CalcVC ()

@end

@implementation CalcVC
@synthesize delegate;
@synthesize amount              = _amount;
@synthesize amountLabel         = _amountLabel;
@synthesize enterButton         = _enterButton;
@synthesize currencyButton      = _currencyButton;
@synthesize currencyCode        = _currencyCode;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    UIView *container = [self.view viewWithTag:1050];
//    container.layer.shadowColor = [UIColor blackColor].CGColor;
//    container.layer.shadowOffset = CGSizeMake(.222f, 0);
//    container.layer.shadowRadius  = 1.0f;
//    container.layer.shadowOpacity = 0.2222f;

    if (self.amount.refund) {
        self.currencyButton.enabled = NO;
    }

    [self currencyCode];
    NSLocale *locale = [NSLocale currentLocale];
    NSString *currency = [locale displayNameForKey:NSLocaleCurrencySymbol value:self.amount.code];
    [_currencyButton setTitle:currency forState:UIControlStateNormal];
    [_currencyButton setTitle:currency forState:UIControlStateHighlighted];
    
    self.title = @"Введите сумму";
    
    UIView *labelContainer = [self.view viewWithTag:1051];
    labelContainer.backgroundColor = [UIColor colorFromHexString:kGrayColor];
    
    [self.amountLabel setTextColor:[UIColor colorFromHexString:kDarkGrayColor]];
    if (self.placeholderNumber && [self.placeholderNumber intValue]!=0) {
        self.amountLabel.text = [[AppHelper currencyFormatter] stringFromNumber:self.placeholderNumber];
    }
    else{
        self.amountLabel.text = @"0";
    }
    
    BarButton *leftButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
    [leftButton setTitle:LSTRING(@"Cancel") forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
}

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Utilities
- (NSString *)string:(NSString *)sourceString addSign:(NSString *)newSign
{
    NSString *string = [sourceString mutableCopy];
    NSCharacterSet *charSet    = nil;
    NSString *decimalSeperator = [AppHelper localeDecimalSeparator];
    static NSString *numberChars      = @"0123456789";
    
    // create a character set of valid chars (numbers and  a decimal sign)
    NSRange decimalRange = [string rangeOfString:decimalSeperator];
    BOOL isDecimalNumber = (decimalRange.location != NSNotFound);
    
    if (isDecimalNumber)
    {
        if ([string isEqualToString:decimalSeperator]) return string; //no separator again
        charSet = [NSCharacterSet characterSetWithCharactersInString:numberChars];
    }
    else
    {
        numberChars = [numberChars stringByAppendingString:decimalSeperator];
        charSet = [NSCharacterSet characterSetWithCharactersInString:numberChars];
    }
    
    if ([newSign isEqualToString:decimalSeperator]) {
        return [string stringByAppendingString:[AppHelper localeDecimalSeparator]];
    }
    
    // remove amy characters from the string that are not a number or decimal sign ...
    NSCharacterSet *invertedCharSet = [charSet invertedSet];
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:invertedCharSet];
    trimmedString = [trimmedString stringByAppendingString:newSign];
    trimmedString = [trimmedString stringByReplacingOccurrencesOfString:[[self numberFormatter] groupingSeparator] withString:@""];
    
    int decimalNumberLenth = 0;
    if (isDecimalNumber) {
        NSArray *array = [trimmedString componentsSeparatedByString:decimalSeperator];
        if (array.count>1) {
            decimalNumberLenth = ((NSString *)array[1]).length;
            if (decimalNumberLenth>2) {
                return sourceString;
            }
            else{
                return [NSString stringWithFormat:@"%@%@", sourceString, newSign];
            }
        }
    }
    
    if ([trimmedString isEqualToString:decimalSeperator]) //Разделитель в конце строки
    {
        return trimmedString;
    }
    else
    {
        NSString *newString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSNumber *number = [[self numberFormatter] numberFromString:newString];
        NSNumber *n;
        if (number == nil) n = [NSNumber numberWithInt:0];
        else n = [NSNumber numberWithDouble:[number doubleValue]];
        if (isDecimalNumber) {
            trimmedString = [[self numberFormatter] stringFromNumber:n];
        }
        else{
            trimmedString = [[self numberFormatterWithoutDecimal] stringFromNumber:n];
        }
    }
    return trimmedString;
}

#pragma mark - Helpers

- (NSString *)currencyCode
{
    if (!self.amount.code) {
        self.amount.code = [Preferences defaultCurrencyCode];
    }
    return self.amount.code;
}

- (void)setCurrencyCode:(NSString *)currencyCode
{
    if (currencyCode) {
        _currencyCode = currencyCode;
        NSLocale *locale = [NSLocale currentLocale];
        NSString *currency = [locale displayNameForKey:NSLocaleCurrencySymbol value:self.currencyCode];
        [_currencyButton setTitle:currency forState:UIControlStateNormal];
        [_currencyButton setTitle:currency forState:UIControlStateHighlighted];
    }
}

- (NSNumberFormatter *)numberFormatter
{
    static NSNumberFormatter *numberFormatter = nil;
    if (!numberFormatter) {
        numberFormatter = [[AppHelper currencyFormatterWithCode:nil] copy];
        numberFormatter.currencySymbol = @"";
        numberFormatter.positiveSuffix = [numberFormatter.positiveSuffix stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        numberFormatter.negativeSuffix = [numberFormatter.negativeSuffix stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
    }
    return numberFormatter;
}

- (NSNumberFormatter *)numberFormatterWithoutDecimal
{
    static NSNumberFormatter *numberFormatter2 = nil;
    if (!numberFormatter2) {
        numberFormatter2 = [[AppHelper currencyFormatterWithCode:nil] copy];
        numberFormatter2.maximumFractionDigits = 0;
        numberFormatter2.currencySymbol = @"";
        numberFormatter2.positiveSuffix = [numberFormatter2.positiveSuffix stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        numberFormatter2.negativeSuffix = [numberFormatter2.negativeSuffix stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    return numberFormatter2;
}

#pragma mark - Buttons Actions

- (IBAction)numberButtonTap:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if ([self.amountLabel.text isEqualToString:@"0"]) {
        self.amountLabel.text = @"";
    }
    self.amountLabel.text = [self string:self.amountLabel.text addSign:button.titleLabel.text];
}

- (IBAction)separatorPressed:(id)sender
{
    if ([self.amountLabel.text rangeOfString:[AppHelper localeDecimalSeparator]].location != NSNotFound) {
        return;
    }
    self.amountLabel.text = [self string:self.amountLabel.text addSign:[AppHelper localeDecimalSeparator]];
}

- (IBAction)backSpacePressed:(id)sender
{
    self.amountLabel.text = @"0";
}

- (IBAction)enterPressed:(id)sender
{
    self.amount.amount = [NSDecimalNumber decimalNumberWithDecimal:[[[self numberFormatter] numberFromString:self.amountLabel.text]decimalValue]];
    [delegate calcDidPressEnterWithAmount:self.amount];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)currencyPressed:(id)sender
{
    CurrencyViewController *currencyVC = [CurrencyViewController new];
    currencyVC.delegate = self;
    [self.navigationController pushViewController:currencyVC animated:YES];
}

#pragma mark - Currency View Controller Delegate

- (void)didSelectCurrency:(NSString *)code
{
    self.amount.code = code;
    NSLocale *locale = [NSLocale currentLocale];
    NSString *currency = [locale displayNameForKey:NSLocaleCurrencySymbol value:self.amount.code];
    [_currencyButton setTitle:currency forState:UIControlStateNormal];
    [_currencyButton setTitle:currency forState:UIControlStateHighlighted];
}



@end
