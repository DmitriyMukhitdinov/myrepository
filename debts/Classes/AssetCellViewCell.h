//
//  AssetCellViewCell.h
//  debts
//
//  Created by Evgeny on 17.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetCellViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contractorLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
