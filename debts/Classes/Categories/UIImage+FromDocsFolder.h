//
//  UIImage+FromDocsFolder.h
//  debts
//
//  Created by Evgeny on 16.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FromDocsFolder)
+ (UIImage *)imageFromDocsByName:(NSString *)imageName;
@end
