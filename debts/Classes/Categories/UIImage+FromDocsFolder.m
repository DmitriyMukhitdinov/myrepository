//
//  UIImage+FromDocsFolder.m
//  debts
//
//  Created by Evgeny on 16.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "UIImage+FromDocsFolder.h"

@implementation UIImage (FromDocsFolder)

+ (UIImage *)imageFromDocsByName:(NSString *)imageName
{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                            NSUserDomainMask,
                                                            YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", docDir, imageName];
    return [[UIImage alloc] initWithContentsOfFile:filePath];
}
@end
