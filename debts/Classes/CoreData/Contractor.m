//
//  Contractor.m
//  debts
//
//  Created by Evgeny on 10.04.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "Contractor.h"
#import "Asset.h"
#import "Debenture.h"


@implementation Contractor

@dynamic address;
@dynamic lastName;
@dynamic name;
@dynamic phone;
@dynamic photo;
@dynamic type;
@dynamic sex;
@dynamic assets;
@dynamic history;

- (BOOL)isWoman
{
    if ([self.sex isEqualToNumber:[NSNumber numberWithInt:0]]) {
        return YES;
    }
    return NO;
}
@end
