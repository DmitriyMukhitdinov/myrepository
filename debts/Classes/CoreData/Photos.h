//
//  Photos.h
//  debts
//
//  Created by Evgeny on 12.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset;

@interface Photos : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Asset *asset;

@end
