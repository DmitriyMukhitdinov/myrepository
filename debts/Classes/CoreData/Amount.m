//
//  Amount.m
//  debts
//
//  Created by Evgeny on 12.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "Amount.h"
#import "Asset.h"
#import "Debenture.h"
#import "Refund.h"


@implementation Amount

@dynamic amount;
@dynamic code;
@dynamic asset;
@dynamic debt;
@dynamic refund;

@end
