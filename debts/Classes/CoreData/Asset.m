//
//  Asset.m
//  debts
//
//  Created by Evgeny on 01.02.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "Asset.h"
#import "Amount.h"
#import "Contractor.h"
#import "Debenture.h"
#import "Photos.h"


@implementation Asset

@dynamic info;
@dynamic ownerType;
@dynamic title;
@dynamic photo;
@dynamic amount;
@dynamic history;
@dynamic owner;
@dynamic photos;

@end
