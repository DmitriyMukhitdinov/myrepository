//
//  Asset.h
//  debts
//
//  Created by Evgeny on 01.02.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Amount, Contractor, Debenture, Photos;

@interface Asset : NSManagedObject

@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSString * ownerType;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) Amount *amount;
@property (nonatomic, retain) NSSet *history;
@property (nonatomic, retain) Contractor *owner;
@property (nonatomic, retain) NSSet *photos;
@end

@interface Asset (CoreDataGeneratedAccessors)

- (void)addHistoryObject:(Debenture *)value;
- (void)removeHistoryObject:(Debenture *)value;
- (void)addHistory:(NSSet *)values;
- (void)removeHistory:(NSSet *)values;

- (void)addPhotosObject:(Photos *)value;
- (void)removePhotosObject:(Photos *)value;
- (void)addPhotos:(NSSet *)values;
- (void)removePhotos:(NSSet *)values;

@end
