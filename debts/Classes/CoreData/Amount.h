//
//  Amount.h
//  debts
//
//  Created by Evgeny on 12.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset, Debenture, Refund;

@interface Amount : NSManagedObject

@property (nonatomic, retain) NSDecimalNumber * amount;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) Asset *asset;
@property (nonatomic, retain) Debenture *debt;
@property (nonatomic, retain) Refund *refund;

@end
