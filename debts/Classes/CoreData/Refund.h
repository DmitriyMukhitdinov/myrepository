//
//  Refund.h
//  debts
//
//  Created by Evgeny on 12.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Amount, Debenture;

@interface Refund : NSManagedObject

@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) Amount *amount;
@property (nonatomic, retain) Debenture *debt;

@end
