//
//  Contractor.h
//  debts
//
//  Created by Evgeny on 10.04.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset, Debenture;

@interface Contractor : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * sex;
@property (nonatomic, retain) NSSet *assets;
@property (nonatomic, retain) NSSet *history;
@end

@interface Contractor (CoreDataGeneratedAccessors)

- (void)addAssetsObject:(Asset *)value;
- (void)removeAssetsObject:(Asset *)value;
- (void)addAssets:(NSSet *)values;
- (void)removeAssets:(NSSet *)values;

- (void)addHistoryObject:(Debenture *)value;
- (void)removeHistoryObject:(Debenture *)value;
- (void)addHistory:(NSSet *)values;
- (void)removeHistory:(NSSet *)values;
- (BOOL)isWoman;
@end
