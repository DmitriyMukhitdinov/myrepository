//
//  Debenture.h
//  debts
//
//  Created by Evgeny on 03.03.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

typedef enum DebtStatus{
    DebtStatusActive,
    DebtStatusOutdated,
    DebtStatusFinished
}DebtStatus;

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Refund.h"

@class Amount, Asset, Contractor, DateChange;

@interface Debenture : NSManagedObject

@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * deadline;
@property (nonatomic, retain) NSString * eventIdentifier;
@property (nonatomic, retain) NSNumber * finished;
@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSDecimalNumber * money;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * debtID;
@property (nonatomic, retain) Asset *asset;
@property (nonatomic, retain) Contractor *contractor;
@property (nonatomic, retain) NSSet *dateChanges;
@property (nonatomic, retain) NSSet *refunds;
@property (nonatomic, retain) Amount *total;
@end

@interface Debenture (CoreDataGeneratedAccessors)

- (void)addDateChangesObject:(DateChange *)value;
- (void)removeDateChangesObject:(DateChange *)value;
- (void)addDateChanges:(NSSet *)values;
- (void)removeDateChanges:(NSSet *)values;

- (void)addRefundsObject:(Refund *)value;
- (void)removeRefundsObject:(Refund *)value;
- (void)addRefunds:(NSSet *)values;
- (void)removeRefunds:(NSSet *)values;

- (DebtStatus)statusForDebt;
- (BOOL)isAssetDebt;
- (DebtType)debtType;
- (NSDecimalNumber *)debtBalance;



@end
