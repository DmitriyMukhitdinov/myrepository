//
//  DateChange.h
//  debts
//
//  Created by Evgeny on 14.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Debenture;

@interface DateChange : NSManagedObject

@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * toDate;
@property (nonatomic, retain) Debenture *debenture;

@end
