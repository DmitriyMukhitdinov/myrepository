//
//  DateChange.m
//  debts
//
//  Created by Evgeny on 14.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DateChange.h"
#import "Debenture.h"


@implementation DateChange

@dynamic created_at;
@dynamic toDate;
@dynamic debenture;

@end
