//
//  Refund.m
//  debts
//
//  Created by Evgeny on 12.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "Refund.h"
#import "Amount.h"
#import "Debenture.h"


@implementation Refund

@dynamic status;
@dynamic type;
@dynamic created_at;
@dynamic amount;
@dynamic debt;

@end
