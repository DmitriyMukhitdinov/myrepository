//
//  Debenture.m
//  debts
//
//  Created by Evgeny on 03.03.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "Debenture.h"
#import "Amount.h"
#import "Asset.h"
#import "Contractor.h"
#import "DateChange.h"


@implementation Debenture

@dynamic created_at;
@dynamic deadline;
@dynamic eventIdentifier;
@dynamic finished;
@dynamic info;
@dynamic money;
@dynamic type;
@dynamic debtID;
@dynamic asset;
@dynamic contractor;
@dynamic dateChanges;
@dynamic refunds;
@dynamic total;

#pragma mark - Methods
- (DebtStatus)statusForDebt
{
    if (self.deadline && [self.deadline timeIntervalSince1970]<[[NSDate date] timeIntervalSince1970] && self.finished!=[NSNumber numberWithInt:1]) {
        return DebtStatusOutdated;
    }
    else if ([self.finished isEqual:[NSNumber numberWithInt:1]])
    {
        return DebtStatusFinished;
    }
    return DebtStatusActive;
}

- (BOOL)isAssetDebt
{
    if ([self.type isEqualToString:DebtType_toString[DebtTypeAssetMine]] ||
        [self.type isEqualToString:DebtType_toString[DebtTypeAssetSomeone]]) {
        return YES;
    }
    return NO;
}

- (DebtType)debtType
{
    if ([self.type isEqualToString:DebtType_toString[DebtTypeAssetMine]]) {
        return DebtTypeAssetMine;
    }
    if ([self.type isEqualToString:DebtType_toString[DebtTypeAssetSomeone]]) {
        return DebtTypeAssetSomeone;
    }
    if ([self.type isEqualToString:DebtType_toString[DebtTypeMoneyMine]]) {
        return DebtTypeMoneyMine;
    }
    if ([self.type isEqualToString:DebtType_toString[DebtTypeMoneySomeone]]) {
        return DebtTypeMoneySomeone;
    }
    return 0;
}

- (NSDecimalNumber *)debtBalance
{
    float total = [self.total.amount floatValue];
    for (Refund *refund in self.refunds) {
        total -= [refund.amount.amount floatValue];
    }
    NSDecimalNumber *dn = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",total]];
                           
    return dn;
}


@end
