//
//  TableSection.m
//  debts
//
//  Created by Evgeny on 23.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "TableSection.h"

@implementation TableSection
@synthesize titleLabel = _titleLabel;


- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, 320.0f, 21.0f);
        UIImage *image = [[UIImage imageNamed:@"section2"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 5.0f, 0)];
        self.backgroundColor = [UIColor colorWithPatternImage:image];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 0, 250.0f, 20.0f)];
        label.backgroundColor = [UIColor clearColor];
        label.font = FONT(12.0f);
        label.textColor = [UIColor colorFromHexString:kDarkGrayColor];
        label.tag = 1010;
        [self addSubview:label];
    }
    return self;
}

- (void)setTitleLabel:(NSString *)titleLabel
{
    _titleLabel = titleLabel;
    UILabel *label = (UILabel *)[self viewWithTag:1010];
    label.text = titleLabel;
}

@end
