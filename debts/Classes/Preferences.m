//
//  Preferences.m
//  debts
/*
    Класс настроек приложения. Работает с NSUserDefaults
*/
//
//  Created by Evgeny on 18.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//



#import "Preferences.h"
#import "Rate.h"
#import "Debenture.h"

#define kDefaultDate         @"defaultDate"
#define kDecimalEnabled      @"decimalEnabled"
#define kCurrency            @"currency"
#define kShowFinishedEnabled @"finishedDebts"

@implementation Preferences


 
#pragma mark - Default Time
 
+ (NSDate *)defaultTime{
    NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultDate];
    if (date) {
        return date;
    }
    else{
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        comps.minute = 0;
        comps.second = 0;
        comps.hour = 12;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *firstPeriodDate = [calendar dateFromComponents:comps];
        return firstPeriodDate;
    }
}

+ (void)setDefaultTime:(NSDate *)time{
    if (time) {
        [[NSUserDefaults standardUserDefaults] setObject:time forKey:kDefaultDate];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [Preferences changeAllNotificationsToTime:time];
    }
}

 
#pragma mark - Decimal counter

+ (BOOL)isDecimalEnabled
{
    if([[[NSUserDefaults standardUserDefaults] objectForKey:kDecimalEnabled] isEqualToNumber:[NSNumber numberWithBool:YES]]){
        return YES;
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:kDecimalEnabled] isEqualToNumber:[NSNumber numberWithBool:NO]]){
        return NO;
    }
    return YES; //default value
}

+ (void)setDecimalEnabled:(NSNumber *)enabled
{
    [[NSUserDefaults standardUserDefaults] setObject:enabled forKey:kDecimalEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Show finished
+ (BOOL)isShowFinishedEnabled
{
    if([[[NSUserDefaults standardUserDefaults] objectForKey:kShowFinishedEnabled] isEqualToNumber:[NSNumber numberWithInteger:1]]){
        return YES;
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:kShowFinishedEnabled] isEqualToNumber:[NSNumber numberWithInteger:0]]){
        return NO;
    }
    return YES; //default value
}

+ (void)setFinishedShowEnabled:(NSNumber *)enabled
{
    [[NSUserDefaults standardUserDefaults] setObject:enabled forKey:kShowFinishedEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

 
#pragma mark - Default curreny
 
+ (NSString *)defaultCurrencyCode
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kCurrency]) {
        //Default currency code for current locale
        NSString *sym = [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode];
        return sym;
    }
    else{
        return [[NSUserDefaults standardUserDefaults] objectForKey:kCurrency];
    }
}

+ (void)setDefaultCurrencyCode:(NSString *)code{
    if (code) {
        if (![code isEqualToString:[Preferences defaultCurrencyCode]]) {
            [[NSUserDefaults standardUserDefaults] setObject:code forKey:kCurrency];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[Rate sharedInstance] deletePList];
            [[Rate sharedInstance] updateAllRates];
        }
        
    }
}

+ (void)changeAllNotificationsToTime:(NSDate *)time
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *pushesArray = [app scheduledLocalNotifications];
    for (int i=0; i<[pushesArray count]; i++)
    {
        UILocalNotification* pushNotification = [pushesArray objectAtIndex:i];
        [[UIApplication sharedApplication] cancelLocalNotification:pushNotification];
        Debenture *debt = [Debenture MR_findFirstByAttribute:@"debtID" withValue:[pushNotification.userInfo objectForKey:@"debtID"]];
        if (debt && debt.finished != [NSNumber numberWithInteger:1]) {
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = localNotification.fireDate = [AppHelper combineDate:debt.deadline withTime:time];;
            localNotification.alertBody = [TextHelper titleForNotoficationForDebtType:[debt debtType] debt:debt];;
            localNotification.alertAction = @"Перейти";
            NSDictionary *info = @{@"debtID": debt.debtID};
            localNotification.userInfo = info;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
    }
}


@end
