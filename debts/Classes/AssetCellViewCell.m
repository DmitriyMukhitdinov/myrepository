//
//  AssetCellViewCell.m
//  debts
//
//  Created by Evgeny on 17.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "AssetCellViewCell.h"

@implementation AssetCellViewCell
@synthesize dateLabel = _dateLabel,
            contractorLabel = _contractorLabel,
            titleLabel      = _titleLabel,
photoImageView  = _photoImageView;


- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
