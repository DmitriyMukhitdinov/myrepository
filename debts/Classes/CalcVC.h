//
//  CalcVC.h
//  debts
//
//  Created by Evgeny on 10.03.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrencyViewController.h"
#import "Amount.h"

@protocol CalcVCDelegate;

@interface CalcVC : UIViewController<CurrencyViewControllerDelegate>{
    __weak id<CalcVCDelegate> delegate;
}
@property (nonatomic, weak)     id<CalcVCDelegate>      delegate;
@property (weak, nonatomic)     IBOutlet UILabel        *amountLabel;
@property (weak, nonatomic)     IBOutlet UIButton       *enterButton;
@property (weak, nonatomic)     IBOutlet UIButton       *currencyButton;
@property (weak, nonatomic)     IBOutlet UIButton       *separatorButton;
@property (strong, nonatomic)   Amount                  *amount;
@property (strong, nonatomic)   NSNumber                *placeholderNumber;
@property NSString *currencyCode;

@end

@protocol CalcVCDelegate <NSObject>

- (void)calcDidPressEnterWithAmount:(Amount *)amount;

@end