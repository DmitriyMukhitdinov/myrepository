//
//  CalendarViewController.h
//  debts
//
//  Created by Evgeny on 17.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//


@protocol CalendarViewControllerDelegate;

#import <UIKit/UIKit.h>
#import "RDVCalendarView.h"
#import "DTViewController.h"

@interface CalendarViewController : DTViewController<RDVCalendarViewDelegate, UIScrollViewDelegate>
{
    __weak id<CalendarViewControllerDelegate> delegate;
}
@property (strong, nonatomic) UIScrollView *scrollView;
@property (nonatomic, weak) id<CalendarViewControllerDelegate> delegate;
@property (nonatomic, strong) NSDate *currentDate;
@property (nonatomic, assign, getter = isDeadlineDate) BOOL deadlineDate;
@property (nonatomic, strong) id object;
@end



@protocol CalendarViewControllerDelegate <NSObject>
- (void)calendar:(CalendarViewController *)calendar didSelectDate:(NSDate *)date forDeadline:(BOOL)isDeadline;
- (void)calendar:(CalendarViewController *)calendar didCancelDateSelectForDeadline:(BOOL)isDeadline;
@end