//
//  ImageCropController.h
//  debts
//
//  Created by Evgeny on 16.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//
@protocol ImageCropControllerDelegate <NSObject>
- (void)didCropAndSaveImageToDocumentsWithName:(NSString *)filename;
@end

#import <UIKit/UIKit.h>
#import "Photos.h"

@interface ImageCropController : UIViewController<UIScrollViewDelegate, UIGestureRecognizerDelegate>{
    __weak id<ImageCropControllerDelegate> delegate;
}
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) id<ImageCropControllerDelegate> delegate;
@property (nonatomic, strong) Photos *photo;
@end
