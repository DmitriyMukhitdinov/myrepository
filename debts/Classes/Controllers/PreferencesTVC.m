//
//  PreferencesVC.m
//  debts
//
//  Created by Evgeny on 15.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "PreferencesTVC.h"
#import "ContractorsTVC.h"
#import "AssetsTVC.h"
#import "Preferences.h"
#import "BackButton.h"
#import "BarButton.h"
#import "TableSection.h"
#import "MainTabBarController.h"
#import "PurchaseHelper.h"
#import <StoreKit/StoreKit.h>

#define kDatePickerCellHeight 216.0f

static NSString *const kSegueArchiveID = @"ArchiveVC";

@interface PreferencesTVC ()

@property (weak, nonatomic) IBOutlet UISwitch *showFinishedSwitch;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (assign, nonatomic) BOOL isDatePickerPresent;
@property (strong, nonatomic) NSDate *selectedDate;

@end

@implementation PreferencesTVC


 
#pragma mark - Lifecycle
 
- (void)viewDidLoad
{
    self.isModal = YES;
    [super viewDidLoad];
    self.title = LSTRING(@"Preferences");
    //remove line for empty cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = 50.0f;
    self.timeLabel.text = [[self dateFormatter] stringFromDate:[Preferences defaultTime]];
    [self.showFinishedSwitch setOn:[Preferences isShowFinishedEnabled]];
    [self.datePicker setDate:[Preferences defaultTime]];
    self.selectedDate = [Preferences defaultTime];
    self.decimalSwith.on = [Preferences isDecimalEnabled];
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 30, 0)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    NSLog(@"product %@ did purchased", productIdentifier);
    [self.tableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ContractorsVC"]) {
        ContractorsTVC *contractorTVC = segue.destinationViewController;
        contractorTVC.isNoSelect = YES;
    }
    else if ([segue.identifier isEqualToString:@"AssetsVC"]){
        AssetsTVC *assetTVC = segue.destinationViewController;
        assetTVC.isNoSelect = YES;
    }
    else if ([segue.identifier isEqualToString:kSegueArchiveID]) {
        MainTabBarController *destVC = [segue destinationViewController];
        destVC.title = @"Архив";
        destVC.showNavBar = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (![self.selectedDate isEqualToDate:[Preferences defaultTime]]) {
        [Preferences setDefaultTime:self.selectedDate];
    }
}

#pragma mark - Utilities
 
- (void)displayInlineDatePicker
{
    if (self.isDatePickerPresent) {
        self.isDatePickerPresent = NO;
    }
    else{
        self.isDatePickerPresent = YES;
    }
    
    [CATransaction begin];
    
    [CATransaction setCompletionBlock:^{
        [self.tableView reloadData];
    }];
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    
    [CATransaction commit];
}

- (IBAction)dateAction:(id)sender
{
    UIDatePicker *targetedDatePicker = sender;
    NSDate *selectedTime = targetedDatePicker.date;
    self.timeLabel.text = [[self dateFormatter] stringFromDate:selectedTime];
    self.selectedDate = selectedTime;
}

 
#pragma mark - UITableView Delegate
 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        //Справочники
        if (indexPath.row == 0) {
            [self performSegueWithIdentifier:@"ContractorsVC" sender:nil];
        }
        else if (indexPath.row == 2){
            UIViewController *vc = [[CurrencyViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else{
            [self performSegueWithIdentifier:@"AssetsVC" sender:nil];
        }
    }
    else if (indexPath.section==0){
        if (indexPath.row == 4) {
            [self displayInlineDatePicker];
        }
    }
    else if (indexPath.section == 3 && indexPath.row == 1)
    {
        [self sendEmailToAddress:@"izobov@mail.ru"];
    }
    else if (indexPath.section == 3 && indexPath.row == 0)
    {
        NSString *buyString=@"https://itunes.apple.com/us/app/ucet-dolgov/id849734136?l=ru&ls=1&mt=8";
        
        NSURL *url = [[NSURL alloc] initWithString:buyString];
        [[UIApplication sharedApplication] openURL:url];
    }
    else if (indexPath.section == 2) {
        [self performSegueWithIdentifier:kSegueArchiveID sender:nil];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0 && indexPath.row == 0){
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    if (cell.accessoryType == UITableViewCellAccessoryDisclosureIndicator) {
        cell.accessoryView = [[ UIImageView alloc ]
                                initWithImage:[UIImage imageNamed:@"rightArrow"]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.section == 0 && indexPath.row==0) &&
        [Engine appEngine].isPaid) {
        return 0;// Buy row
    }
    if (indexPath.section == 0 && indexPath.row==1) {
        return 0;
    }
    if (indexPath.section == 0 && indexPath.row == 5) {
        if (self.isDatePickerPresent) {
            return kDatePickerCellHeight;
        }
        else{
            return 0;
        }
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    
    return 20.5f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return [UIView new];
    }
    TableSection *sections = [[TableSection alloc] init];
    if (section == 1) {
        sections.titleLabel = @"справочники";
    }
    else if (section == 2){
        sections.titleLabel = @"журналы";
    }
    else if (section == 3){
        sections.titleLabel = @"обратная связь";
    }
    return sections;
}

 
#pragma mark - Helpers
 


- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm";
    }
    return dateFormatter;
}

 
#pragma mark - User Actions
 
- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)decimalValueChanged:(id)sender
{
    UISwitch *switchView = (UISwitch *)sender;
    [Preferences setDecimalEnabled:[NSNumber numberWithBool:switchView.isOn]];
}
- (IBAction)showFinishedChanged:(id)sender
{
    UISwitch *switchView = (UISwitch *)sender;
    [Preferences setFinishedShowEnabled:[NSNumber numberWithBool:switchView.isOn]];
}


#pragma mark - Mail
- (void)sendEmailToAddress:(NSString *)email
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        mailer.modalPresentationStyle = UIModalPresentationFormSheet;
        NSArray *toRecipients = [NSArray arrayWithObjects:email, nil];
        [mailer setToRecipients:toRecipients];
        [mailer setSubject:@"Debts iOS review"];
        [self presentViewController:mailer animated:YES completion:NULL];
    }
    else
    {
        NSString *message = @"Your device doesn't support the composer sheet";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    if (result == MFMailComposeResultSent)
    {
        NSLog(@"It's away!");
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    else
    {
        NSLog(@"error");
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (IBAction)buyFullVersion:(id)sender {
    SKProduct *product = [[Engine appEngine].products firstObject];
    [[PurchaseHelper shared] buyProduct:product];
}

- (void)restoreTapped:(id)sender {
    [[PurchaseHelper shared] restoreCompletedTransactions];
}

@end
