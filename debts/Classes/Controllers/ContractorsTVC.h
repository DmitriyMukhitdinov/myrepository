//
//  ConductorsTableViewController.h
//  debts
//
//  Created by Evgeny on 13.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTTableViewController.h"
#import "ContractorVC.h"

@protocol ContractorTableViewDelegate <NSObject>

@optional
- (void) didSelectContractor:(Contractor *)contractor;
- (void) didCancelSelect;
@end

@interface ContractorsTVC : DTTableViewController<ContractorVCDelegate>
{
    __weak id<ContractorTableViewDelegate> delegate;
}
@property (nonatomic, weak)   id<ContractorTableViewDelegate> delegate;
@property (nonatomic, assign) DebtType debtType;
@property (nonatomic, assign) BOOL isNoSelect;

@end
