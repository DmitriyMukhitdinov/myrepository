//
//  CalendarViewController.m
//  debts
//
//  Created by Evgeny on 17.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "CalendarViewController.h"
#import "GreenButton.h"

@interface CalendarViewController ()
@property (nonatomic, strong) RDVCalendarView *calendarViewLeft;
@property (nonatomic, strong) RDVCalendarView *calendarView;
@property (nonatomic, strong) RDVCalendarView *calendarViewRight;
@property (nonatomic, strong) NSDate *currentCalendarDate;
@end

@implementation CalendarViewController
@synthesize delegate;

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = LSTRING(@"Calendar");
    self.view.backgroundColor =  [UIColor whiteColor];
    [self addDayslabels];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.currentDate];
    
    self.currentCalendarDate = self.currentDate;
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,
                                                                84,
                                                                self.view.frame.size.width,
                                                                400)];
    [self.view addSubview:_scrollView];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width*3, 400)];
    self.calendarViewLeft = [[RDVCalendarView alloc] initWithFrame:CGRectMake(0,
                                                                              -20,
                                                                              self.view.frame.size.width,
                                                                              400)];
    self.calendarView = [[RDVCalendarView alloc] initWithFrame:CGRectMake(320,
                                                                          -20,
                                                                          self.view.frame.size.width,
                                                                          400)];
    self.calendarViewRight = [[RDVCalendarView alloc] initWithFrame:CGRectMake(640,
                                                                          -20,
                                                                          self.view.frame.size.width,
                                                                          400)];
    self.calendarView.delegate = self;
    self.calendarViewLeft.defaultMonth = components.month;
    self.calendarView.defaultMonth = components.month;
    self.calendarViewRight.defaultMonth = components.month;
    

    NSDateComponents *components1 = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                               fromDate:self.currentDate];
    [components1 setMonth:components.month-1];
    NSDate *newDate = [calendar dateFromComponents:components1];
    

    [components1 setMonth:components1.month+2];
    NSDate *newDate1 = [calendar dateFromComponents:components1];
    
    if (self.currentDate)self.calendarViewLeft.selectedDate = newDate;
    if (self.currentDate)self.calendarView.selectedDate = self.currentDate;
    if (self.currentDate)self.calendarViewRight.selectedDate = newDate1;
    
    
    [self.calendarViewLeft setSelectedMonth:-1 forDate:self.calendarView.selectedDate];
    [self.calendarViewRight setSelectedMonth:+1 forDate:self.calendarView.selectedDate];
    
    if (self.isDeadlineDate) {
        GreenButton *button = [GreenButton new];
        button.frame = CGRectMake(ksViewsLeftRightMargin, self.view.frame.size.height-38.0f, ksViewsWidth, 30.0f);
        button.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [button setTitle:LSTRING(@"Without refund date") forState:UIControlStateNormal];
        [button setTitle:LSTRING(@"Without refund date") forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(clearDateButton) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
    }
    
    [self.scrollView addSubview:self.calendarViewLeft];
    [self.scrollView addSubview:self.calendarView];
    [self.scrollView addSubview:self.calendarViewRight];
    [self.scrollView scrollRectToVisible:self.calendarView.frame animated:NO];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MMMM yyyy";
    [self calendarView:nil didChangeMonth:[[[formatter stringFromDate:(self.currentDate)?self.currentDate:[NSDate date]] stringByReplacingOccurrencesOfString:@"." withString:@""] uppercaseString]];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [self scrollViewDidEndDecelerating:self.scrollView];
}

- (void)addDayslabels
{
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 64, 320.0f, 30.0f)];
    [container setBackgroundColor:[UIColor colorFromHexString:kGrayColor]];
    CGFloat labelWidth = 320/7;
    NSArray *days = @[@"пн",@"вт",@"ср",@"чт",@"пт",@"сб",@"вс"];
    for (int i = 0; i<7; i++) {
        UILabel *day = [[UILabel alloc] initWithFrame:CGRectMake(i*labelWidth, 0, labelWidth, 30.0f)];
        day.textColor = [UIColor grayColor];
        day.text = days[i];
        day.textAlignment = NSTextAlignmentCenter;
        day.center = CGPointMake(day.center.x, 15.0f);
        CALayer* layer = [container layer];
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.borderColor = [UIColor colorFromHexString:kDarkGrayColor].CGColor;
        bottomBorder.borderWidth = 0.5f;
        bottomBorder.frame = CGRectMake(-1, layer.frame.size.height-1, layer.frame.size.width+1.0f, 0.5f);
        [bottomBorder setBorderColor:[UIColor lightGrayColor].CGColor];
        [layer addSublayer:bottomBorder];
        [container addSubview:day];
    }
    [self.view addSubview:container];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Actions

- (void)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Calendar Delegate

- (void)calendarView:(RDVCalendarView *)calendarView didSelectDate:(NSDate *)date
{
    self.currentDate = [AppHelper combineDate:date withTime:[Preferences defaultTime]];
    [self.delegate calendar:self didSelectDate:self.currentDate forDeadline:self.isDeadlineDate];
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)clearDateButton
{
    self.currentDate = nil;
    [self.delegate calendar:self didCancelDateSelectForDeadline:self.isDeadlineDate];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)calendarView:(RDVCalendarView *)calendarView didChangeMonth:(NSString *)month
{
    self.title = [AppHelper russianMonthFix:month];
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
	// All data for the documents are stored in an array (documentTitles).
	// We keep track of the index that we are scrolling to so that we
	// know what data to load for each page.
    
	if(self.scrollView.contentOffset.x > self.scrollView.frame.size.width) {
        [self nextMonthButtonTapped:nil];
	}
	if(self.scrollView.contentOffset.x < self.scrollView.frame.size.width) {
        [self previousMonthButtonTapped:nil];
	}
	// Reset offset back to middle page
    
    [self.calendarViewRight setSelectedMonth:+1 forDate:self.currentCalendarDate];
    [self.calendarViewLeft setSelectedMonth:-1 forDate:self.currentCalendarDate];
    [self.scrollView scrollRectToVisible:self.calendarView.frame animated:NO];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    [aScrollView setContentSize:CGSizeMake(aScrollView.contentSize.width, aScrollView.frame.size.height)];
}

- (void)previousMonthButtonTapped:(id)sender {
    
    NSDate *date = self.currentCalendarDate;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    [components setMonth:components.month-1];
    NSDate *newDate = [calendar dateFromComponents:components];
    [self.calendarView setSelectedMonth:0 forDate:newDate];
    NSInteger month = [[self.calendarView month] month];
    [[self.calendarView month] setMonth:month];
    self.currentCalendarDate = newDate;
    [self updateMonthLabelMonth:[self.calendarView month]];
}

- (void)nextMonthButtonTapped:(id)sender {
    NSDate *date = self.currentCalendarDate;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    [components setMonth:components.month+1];
    NSDate *newDate = [calendar dateFromComponents:components];
    [self.calendarView setSelectedMonth:0 forDate:newDate];
    NSInteger month = [[self.calendarView month] month];
    [[self.calendarView month] setMonth:month];
    self.currentCalendarDate = newDate;
    [self updateMonthLabelMonth:[self.calendarView month]];
}

- (void)updateMonthLabelMonth:(NSDateComponents*)month {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MMMM yyyy";
    NSDate *date = [month.calendar dateFromComponents:month];
    [self calendarView:self.calendarView didChangeMonth:[[[formatter stringFromDate:date] stringByReplacingOccurrencesOfString:@"." withString:@""] uppercaseString]];
}



@end
