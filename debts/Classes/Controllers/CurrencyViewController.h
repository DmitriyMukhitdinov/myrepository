//
//  CurrencyViewController.h
//  debts
//
//  Created by Evgeny on 16.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//
#import "DTTableViewController.h"

@protocol CurrencyViewControllerDelegate <NSObject>

- (void)didSelectCurrency:(NSString *)code;

@end

#import <UIKit/UIKit.h>


@interface CurrencyViewController : DTTableViewController
<UISearchBarDelegate, UISearchDisplayDelegate>
{
    __weak id<CurrencyViewControllerDelegate> delegate;
}

@property (nonatomic, weak) id<CurrencyViewControllerDelegate> delegate;

@end
