//
//  ConductorsTableViewController.m
//  debts
//
//  Created by Evgeny on 13.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "ContractorsTVC.h"
#import "UIImage+FromDocsFolder.h"
#import "ContractorCell.h"
#import "Contractor.h"
#import "DebtVC.h"

@interface ContractorsTVC () <UISearchDisplayDelegate, UISearchBarDelegate>
@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, strong) NSArray *searchResults;
@end

@implementation ContractorsTVC
@synthesize tableData   = _tableData, delegate;
@synthesize debtType    = _debtType;
@synthesize isNoSelect  = _isNoSelect;


#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Remove left offcet on cells Only iOS7
    self.tableView.separatorInset = UIEdgeInsetsZero;
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    self.searchDisplayController.searchResultsTableView.rowHeight = self.tableView.rowHeight;
    if (!self.isNoSelect){
        self.isEditing = NO;
        
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 61.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Select") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(editTable:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
    }
    else {
        self.isEditing = YES;
    }
    [self updateTable];
    self.title = @"Контрагенты";
    
    __block NSInteger foundIndex = NSNotFound;
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DebtVC class]]) {
            foundIndex = idx;
            // stop the enumeration
            *stop = YES;
        }
    }];
    
    if (foundIndex != NSNotFound) {
        // You've found the first object of that class in the array
        self.navigationItem.prompt = @"выберите контрагента";
        //self.tableView.contentInset = UIEdgeInsetsMake(30.0f, 0, 0, 0);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIBarButtonItem *button = self.navigationItem.rightBarButtonItem;
    if (self.isEditing){
        [button setTitle:NSLocalizedString(@"Edit", @"")];
    }
    else{
        [button setTitle:NSLocalizedString(@"Select", @"")];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ContractorVC"]) {
        ContractorVC *contractorViewController = segue.destinationViewController;
        contractorViewController.delegate = self;
        Contractor *contractor;
        if ([self.searchDisplayController isActive]) {
            if(sender)contractor = self.searchResults[[(NSIndexPath *)sender row]];
        }
        else {
            if(sender)contractor = self.tableData[[(NSIndexPath *)sender row]];
        }
        contractorViewController.contractor = contractor;
    }
}


#pragma mark - Table view data source


- (void) updateTable
{
    NSMutableArray *array = [NSMutableArray arrayWithArray:
                             [Contractor MR_findByAttribute:@"type"
                                                  withValue:@"somebody"
                                                 andOrderBy:@"name"
                                                  ascending:YES]];
    NSSortDescriptor *isHomeTeam = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [array sortUsingDescriptors:@[isHomeTeam]];
    
    NSSortDescriptor *sortDescr = [NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                ascending:YES
                                                               comparator:^NSComparisonResult(id obj1, id obj2) {
                                                                   NSComparisonResult res = [obj1 caseInsensitiveCompare:obj2];
                                                                   return res;
                                                                }];
    
    [array sortUsingDescriptors:@[sortDescr]];
    if (![delegate respondsToSelector:@selector(type)]
        /*&&
                      self.debtType!=DebtTypeMoneyMine &&
                      self.debtType!=DebtTypeAssetMine*/)
    {
        Contractor *me = [Contractor MR_findFirstByAttribute:@"type" withValue:@"me"];
        [array insertObject:me atIndex:0];
    }
    self.tableData = array;
    if (self.tableData.count==0){
        self.isEditing = YES;
    }
    else{
        if (!self.isNoSelect) {
            self.isEditing = NO;
        }
    }
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([tableView isEqual:self.searchDisplayController.searchResultsTableView]) {
        return self.searchResults.count;
    }
    return self.tableData.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Select last row in table
    if (
        indexPath.section == tableView.numberOfSections-1 &&
        indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1 &&
        [tableView isEqual:self.tableView])
    {
        //Строка "Добавить"
        static NSString *cellIdentifier = @"addRowCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (nil == cell)
        {
            UINib *customCellNib = [UINib nibWithNibName:@"addRow" bundle:nil];
            [self.tableView registerNib:customCellNib forCellReuseIdentifier:cellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        UILabel *label = (UILabel *)[cell viewWithTag:1010];
        label.text = @"Добавить контрагента";
        return cell;
    }
    else{
        Contractor *contractor;
        if ([tableView isEqual:self.searchDisplayController.searchResultsTableView]) {
            contractor = self.searchResults[indexPath.row];
        }
        else {
            contractor = self.tableData[indexPath.row];
        }
        
        ContractorCell *cell;

        static NSString *CellIdentifierPhoto = @"ContractorCellPhoto";
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifierPhoto];
        if (nil == cell)
        {
            UINib *customCellNib = [UINib nibWithNibName:CellIdentifierPhoto bundle:nil];
            [self.tableView registerNib:customCellNib forCellReuseIdentifier:CellIdentifierPhoto];
            cell = (ContractorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPhoto];
        }
        if (contractor.photo) {
            dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(concurrentQueue, ^{
                UIImage *image = [UIImage imageFromDocsByName:contractor.photo];
                if (image != nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell.photoImageView setImage:image];
                    });
                }
            });
        }
        else{
            [cell.photoImageView setImage:[UIImage imageNamed:@"photoPlaceholder"]];
        }
        
        if (self.isEditing) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        NSString *name = [NSString stringWithFormat:@"%@ %@", (contractor.name!=nil)?contractor.name:@" ", (contractor.lastName!=nil)?contractor.lastName:@" "];
        cell.nameLabel.text = name;
        return cell;
    }
}


#pragma mark - UI Table View delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Select last row in table
    if (indexPath.section == tableView.numberOfSections-1 &&
        indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1 &&
        [tableView isEqual:self.tableView])
    {
        [self performSegueWithIdentifier:@"ContractorVC" sender:nil];
    }
    else if(!self.isEditing){
        Contractor *contractor;
        if ([tableView isEqual:self.searchDisplayController.searchResultsTableView]) {
            contractor = self.searchResults[indexPath.row];
        }
        else {
            contractor = self.tableData[indexPath.row];
        }
        
        [self.delegate didSelectContractor:contractor];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        //Select mode
        [self performSegueWithIdentifier:@"ContractorVC" sender:indexPath];
    }
}

#pragma mark - Search
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    self.searchResults = [self.tableData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name contains[cd] %@", searchString]];
    return YES;
}

 
#pragma mark - Contractor View Delegate
 
- (void)didSaveContractor
{
    if (self.delegate) {
        self.isEditing = NO;
    }
    [self updateTable];
}


#pragma mark - IB Actions

- (void)setIsEditing:(BOOL)isEditing
{
    _isEditing = isEditing;
    UIBarButtonItem *button = self.navigationItem.rightBarButtonItem;
    BarButton *btn = (BarButton *)button.customView;
    if (isEditing) {
        [btn setTitle:LSTRING(@"Select") forState:UIControlStateNormal];
    }
    else{
        [btn setTitle:LSTRING(@"Edit") forState:UIControlStateNormal];
    }
}

- (void) editTable:(id)sender
{
    //NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self.tableView indexPathsForVisibleRows]];
    if (self.isEditing) {
        self.isEditing = NO;
        //[self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.tableData.count inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        //[array removeLastObject];
    }
    else{
        self.isEditing = YES;
        //[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.tableData.count inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
    }
    //[self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView reloadData];
}

- (void)cancel:(id)sender
{
    if ([delegate respondsToSelector:@selector(didCancelSelect)]) {
        [self.delegate didCancelSelect];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
