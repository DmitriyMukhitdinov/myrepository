//
//  ImageCropController.m
//  debts
//
//  Created by Evgeny on 16.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "ImageCropController.h"
#import <QuartzCore/QuartzCore.h>
#import "GreenButton.h"

#define kInnerEdgeInset 15.0
@interface ImageCropController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *maskView;
@end

@implementation ImageCropController
@synthesize scrollView = _scrollView, imageView = _imageView, image = _image;
@synthesize photo = _photo;
@synthesize delegate;


#pragma mark - Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                target:self
                                                                                action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
//    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
//                                                                          target:self
//                                                                          action:@selector(save:)];
//    self.navigationItem.rightBarButtonItem = save;
    
    self.view.backgroundColor = [UIColor blackColor];
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 290.0f, 290.0f)];
    _scrollView.center = self.view.center;
    _scrollView.backgroundColor = [UIColor blackColor];
    _scrollView.maximumZoomScale = 3.0f;
    _scrollView.minimumZoomScale = 0.75f;
    _scrollView.clipsToBounds = NO;
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                              self.image.size.width,
                                                              self.image.size.height)];
    _scrollView.contentSize = self.image.size;
    _imageView.image = self.image;
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [_scrollView addSubview:_imageView];
    [self.view addSubview:_scrollView];
    
    self.title = LSTRING(@"Move and zoom");
    
    GreenButton *button = [[GreenButton alloc]init];
    button.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y + _scrollView.frame.size.height + ksViewsTopMargin, 290.0f, 30.0f);
    [button setTitle:LSTRING(@"Save") forState:UIControlStateNormal];
    [button addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    [self.view addSubview:_maskView];
    
    //[self updateScrollViewContentInset];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    // 4
    CGRect scrollViewFrame = self.scrollView.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.scrollView.contentSize.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.scrollView.contentSize.height;
    CGFloat minScale = MAX(scaleWidth, scaleHeight);
    self.scrollView.minimumZoomScale = minScale;
    
    // 5
    self.scrollView.maximumZoomScale = 2.0f;
    self.scrollView.zoomScale = minScale;
    
    // 6
    [self centerScrollViewContents];
    
    [super viewWillAppear:animated];
    UIImageView *maskImageView = [[UIImageView alloc] initWithImage:[self circularOverlayMask]];
    [self.view insertSubview:maskImageView aboveSubview:_scrollView];
}

/*
 * It is important to update the scroll view content inset, specilally after zooming.
 * This allows the user to move the image around with control, from edge to edge of the overlay masks.
 */
- (void)updateScrollViewContentInset
{
    CGFloat maskHeight = 290.0f-(kInnerEdgeInset*2);
    CGSize imageSize = self.image.size;
    
    CGFloat hInset = kInnerEdgeInset;
    CGFloat vInset = fabs((maskHeight-imageSize.height)/2);
    
    if (vInset == 0) vInset = 0.5;
    
    _scrollView.contentInset =  UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
}


/*
 * The circular overlay mask image to be displayed on top of the photo as cropping guideline.
 * Created with PaintCode. The source file is available inside of Resource folder.
 */
- (UIImage *)circularOverlayMask
{
    // Constants
    CGRect rect = self.navigationController.view.bounds;
    CGFloat width = rect.size.width;
    CGFloat height = rect.size.height;
    
    CGFloat diameter = width-(kInnerEdgeInset*2);
    CGFloat radius = diameter/2;
    CGPoint center = CGPointMake(width/2, height/2);
    UIColor *fillColor = [UIColor colorWithWhite:0 alpha:0.5];
    
    // Create the image context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    
    // Create the bezier paths
    UIBezierPath *clipPath = [UIBezierPath bezierPathWithRect:rect];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(center.x-radius, center.y-radius, diameter, diameter)];
    
    [clipPath appendPath:maskPath];
    clipPath.usesEvenOddFillRule = YES;
    
    [clipPath addClip];
    [fillColor setFill];
    [clipPath fill];
    
    UIImage *_maskedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return _maskedImage;
}


#pragma mark - UIScrollView Delegate Methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContents];
    
}

- (void)centerScrollViewContents {
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = self.imageView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.imageView.frame = contentsFrame;
}


#pragma mark - IB Actions

- (void)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)save:(id)sender
{
    CGRect visibleRect;
    float scale = 1.0f/self.scrollView.zoomScale;
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    visibleRect.origin.x    = self.scrollView.contentOffset.x * scale;
    visibleRect.origin.y    = self.scrollView.contentOffset.y * scale;
    visibleRect.size.width  = self.scrollView.bounds.size.width * scale * screenScale;
    visibleRect.size.height = self.scrollView.bounds.size.height * scale * screenScale;
    UIImage *image = [self imageByCropping:self.scrollView toRect:CGRectMake(0, 0, 280.0f, 280.0f)];
    [self saveImageToDocuments:image];
}

- (UIImage *)imageByCropping:(UIScrollView *)imageToCrop toRect:(CGRect)rect
{
    CGSize pageSize = rect.size;
    UIGraphicsBeginImageContext(pageSize);
    
    CGContextRef resizedContext = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(resizedContext, -imageToCrop.contentOffset.x, -imageToCrop.contentOffset.y);
    [imageToCrop.layer renderInContext:resizedContext];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}


#pragma mark - Saving to documents directory

- (void)saveImageToDocuments:(UIImage *)image
{
    NSString *uniqFileName = [NSString stringWithFormat:@"%@_at_%f_thumb.jpg",
                              [self genRandStringLength:5],
                              [[NSDate date]timeIntervalSince1970]];
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                            NSUserDomainMask,
                                                            YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", docDir, uniqFileName];
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];
    [imageData writeToFile:filePath atomically:YES];
    NSLog(@"*** Image saved with file path : %@", uniqFileName);
    
    //Save original
    NSString *originalpath = [filePath stringByReplacingOccurrencesOfString:@"_thumb" withString:@"_original"];
    NSData *imageDataOriginal = [NSData dataWithData:UIImageJPEGRepresentation(self.image, 1.0f)];
    [imageDataOriginal writeToFile:originalpath atomically:YES];
    
    [self.delegate didCropAndSaveImageToDocumentsWithName:uniqFileName];
    [self dismissViewControllerAnimated:YES completion:nil];
}

NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

-(NSString *) genRandStringLength: (int) len {
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}




@end
