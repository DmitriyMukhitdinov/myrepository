//
//  PreferencesVC.h
//  debts
//
//  Created by Evgeny on 15.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DTTableViewController.h"
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface PreferencesTVC : DTTableViewController
<MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *decimalSwith;

@end
