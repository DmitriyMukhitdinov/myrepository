//
//  CurrencyViewController.m
//  debts
//
//  Created by Evgeny on 16.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//
@interface Currency : NSObject
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *fullTitle;
@end

@implementation Currency
@synthesize code = _code;
@synthesize fullTitle = _fullTitle;
@end

#import "CurrencyViewController.h"
#import "AppHelper.h"
#import "Preferences.h"
#import "DoActionSheet.h"
#import "Rate.h"


@interface CurrencyViewController ()
@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) UISearchDisplayController *mySearchDisplayController;
@end

@implementation CurrencyViewController
@synthesize mySearchDisplayController = _mySearchDisplayController;
@synthesize tableData = _tableData, isEditing = _isEditing, currentIndex = _currentIndex;
@synthesize delegate;

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.delegate == nil) {
        self.isEditing = YES;
    }
    else{
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 61.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Edit") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(editTable:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        self.isEditing = NO;
    }
    
    //Adding search
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44.0f)];
    searchBar.placeholder = @"Поиск";
    searchBar.delegate = self;
//    searchBar.backgroundColor = [UIColor colorFromHexString:kMainColor];
//    [searchBar setBackgroundImage:[UIImage new]];
//    [searchBar setTranslucent:YES];
    self.tableView.tableHeaderView = searchBar;
    self.mySearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar
                                                                       contentsController:self];
    self.mySearchDisplayController.searchResultsDelegate = self;
    self.mySearchDisplayController.searchResultsDataSource = self;
    self.mySearchDisplayController.delegate = self;
    self.tableView.tableHeaderView = searchBar;
    [self prepareTableData];
    self.title = @"Валюты";
    [self.tableView registerNib:[UINib nibWithNibName:@"currencyCell" bundle:nil] forCellReuseIdentifier:@"CurrencyCell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource Delegate Methods

- (void)prepareTableData
{
    NSArray *primary = @[@"GBP",@"EUR",@"USD",@"RUB"];
    
    NSMutableArray *primaryArray   = [[NSMutableArray alloc] init];
    NSMutableArray *otherArray     = [[NSMutableArray alloc] init];
    
    for (NSString *code in [NSLocale ISOCurrencyCodes])
    {
        Currency *curr = [[Currency alloc] init];
        curr.code = code;
        curr.fullTitle = [[NSLocale currentLocale] displayNameForKey:NSLocaleCurrencyCode
                                                                   value:code];
        if ([primary containsObject:code]) {
            [primaryArray insertObject:curr atIndex:0];
        }
        else{
            [otherArray addObject:curr];
        }
    }
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:primaryArray];
    [array addObjectsFromArray:otherArray];
    Currency *currency = [[array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"code == %@",[Preferences defaultCurrencyCode]]] lastObject];
    [array removeObject:currency];
    self.tableData = array;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }
    return self.tableData.count;
}


#pragma mark - UITableViewDelegate Delegate Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CurrencyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
         [tableView registerNib:[UINib nibWithNibName:@"currencyCell" bundle:nil] forCellReuseIdentifier:@"CurrencyCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    UILabel *leftLabel = (UILabel *)[cell viewWithTag:1010];
    UILabel *rightLabel = (UILabel *)[cell viewWithTag:1011];
    
    NSLocale *locale = [NSLocale currentLocale];
    if (indexPath.section == 0) {
        leftLabel.text = [locale displayNameForKey:NSLocaleCurrencyCode value:[Preferences defaultCurrencyCode]];
        rightLabel.text = [locale displayNameForKey:NSLocaleCurrencySymbol value:[Preferences defaultCurrencyCode]];
        rightLabel.textColor = [UIColor colorFromHexString:kMainColor];
    }
    else{
        Currency *currency= self.tableData[indexPath.row];
        leftLabel.text = currency.fullTitle;
        rightLabel.text = [locale displayNameForKey:NSLocaleCurrencySymbol value:currency.code];
        rightLabel.textColor = [UIColor lightGrayColor];
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TableSection *sections = [[TableSection alloc] init];
    if (section == 0) {
        sections.titleLabel = LSTRING(@"Default currency");
    }
    else if (section == 1){
        sections.titleLabel = LSTRING(@"Other currencies");
    }
    
    return sections;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isEditing) {
        if (indexPath.section == 0) {
            return;
        }
        self.currentIndex = indexPath.row;
        DoActionSheet *vActionSheet = [[DoActionSheet alloc] init];
        [vActionSheet showC:nil
                     cancel:LSTRING(@"Cancel")
                    buttons:@[LSTRING(@"Make primary")]
                     result:^(int nResult) {
                         NSLog(@"---------------> result : %d", nResult);
                         switch (nResult) {
                             case 0:
                                 [self setCurrentCurrencyForIndex:self.currentIndex];
                                 if (tableView == self.searchDisplayController.searchResultsTableView) {
                                     [self.searchDisplayController setActive:NO animated:YES];
                                 }
                                 break;
                             default:
                                 break;
                         }
                     }];
    }
    else{
        if (indexPath.section == 0) {
            [self.delegate didSelectCurrency:[Preferences defaultCurrencyCode]];
        }
        else{
            Currency *curr = self.tableData[indexPath.row];
            [self.delegate didSelectCurrency:curr.code];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,0.22222f* NSEC_PER_SEC), dispatch_get_main_queue(),^{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    });
}

- (void) setCurrentCurrencyForIndex:(NSInteger)index
{
    Currency *curr = self.tableData[index];
    [Preferences setDefaultCurrencyCode:curr.code];
    [self prepareTableData];
    [self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
    if (self.delegate) {
        [self.delegate didSelectCurrency:curr.code];
    }
}




#pragma mark - Actions

- (void)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)editTable:(id)sender
{
    UIBarButtonItem *button = self.navigationItem.rightBarButtonItem;
    if (self.isEditing) {
        self.isEditing = NO;
        [button setTitle:NSLocalizedString(@"Edit", @"")];
    }
    else{
        self.isEditing = YES;
        [button setTitle:NSLocalizedString(@"Select", @"")];
    }
}


 
#pragma mark - Search Result Delegate

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    tableView.rowHeight = self.tableView.rowHeight;
}
 
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    
    
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text]
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:searchOption]];
    
    return YES;
}

- (void) searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView{
    
    [self prepareTableData];
    //[self.tableView reloadData];
}


- (void)filterContentForSearchText:(NSString*)searchText
                             scope:(NSString*)scope
{
    //search for text
    [self prepareTableData];
    if (searchText.length>0) {
        self.tableData = [self.tableData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"code contains[cd] %@ OR fullTitle contains[cd] %@", searchText, searchText]];
    }
}


-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self prepareTableData];
    //[self.tableView reloadData];
}


@end
