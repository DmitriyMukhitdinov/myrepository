//
//  MainNavigationController.h
//  debts
//
//  Created by Evgeny on 12.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainNavigationController : UINavigationController<UIGestureRecognizerDelegate>

@end
