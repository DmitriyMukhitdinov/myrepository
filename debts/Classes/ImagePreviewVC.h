//
//  ImagePreviewVC.h
//  debts
//
//  Created by Evgeny on 22.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NZCircularImageView.h"
//#import <DZNPhotoPickerController/DZNPhotoPickerController.h>

@interface ImagePreviewVC : UIViewController
@property (strong, nonatomic) UIImage *image;
@property (weak, nonatomic) IBOutlet NZCircularImageView *imageView;
@end
