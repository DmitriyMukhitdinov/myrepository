//
//  AssetVC.m
//  debts
//
//  Created by Evgeny on 25.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "AssetVC.h"
#import "Contractor.h"
#import "Asset.h"
#import "Amount.h"
#import "ContractorCell.h"
#import "HistoryTVC.h"
#import "DoActionSheet.h"
#import "TableSection.h"
#import "Debenture.h"

#define kCalcVCSegue           @"CalcVCSegue"

@interface AssetVC ()
@property (nonatomic, strong) Contractor    *contractor;
@property (strong, nonatomic) Amount        *amount;
@property (strong, nonatomic) NSString      *currencyCode;
@property (strong, nonatomic) NSString      *photoName;
@end

@implementation AssetVC
@synthesize delegate;

#pragma mark - Lifecycle
 
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.titleTextField.delegate = self;
    self.infoTextView.delegate = self;
    self.infoTextView.contentInset = UIEdgeInsetsMake(-3,-4,0,0);
    if (self.asset == nil) {
        self.historyCell.hidden = YES;
        self.title = @"Новое имущество";
        
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Save") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        rightButton.enabled = NO;

        self.priceLabel.text = [[AppHelper currencyFormatterWithCode:[Preferences defaultCurrencyCode]] stringFromNumber:[NSNumber numberWithInt:0]];
        self.infoTextView.text = @"";
        self.infoTextView.placeholder = @"Введите описание";
        BarButton *leftButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [leftButton setTitle:LSTRING(@"Cancel") forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
        self.navigationItem.leftBarButtonItem = leftBarButton;
        leftBarButton.enabled = YES;
        self.amount = [Amount MR_createEntity];
    }
    else{
        self.title = @"Редактирование";
        
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Delete") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        
        if (self.asset.photo) {
            self.photoImageView.image = [UIImage imageFromDocsByName:self.asset.photo];
            self.photoName = self.asset.photo;
            self.addPhotoLabel.text = @"";
        }
        self.titleTextField.text = self.asset.title;
        //Amount label
        if (self.asset.amount) {
            NSString *code = (self.asset.amount.code) ? self.asset.amount.code : [Preferences defaultCurrencyCode];
            self.priceLabel.text = [[AppHelper currencyFormatterWithCode:code] stringFromNumber:self.asset.amount.amount];
        }
        else{
            self.priceLabel.text = [[AppHelper currencyFormatterWithCode:[Preferences defaultCurrencyCode]] stringFromNumber:[NSNumber numberWithInt:0]];
        }
        self.infoTextView.text = self.asset.info;
        self.contractor = self.asset.owner;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.asset) {
        [self saveAsset:self.asset];
        [self.delegate updateTable];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ContractorsVC"]) {
        ContractorsTVC *contractorTVC = segue.destinationViewController;
        contractorTVC.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"HistoryVC"]){
        HistoryTVC *historyTVC = segue.destinationViewController;
        historyTVC.item = self.asset;
    }
    else if ([segue.identifier isEqualToString:kCalcVCSegue]){
        UINavigationController *destVC = [segue destinationViewController];
        ((CalcVC *)destVC.visibleViewController).delegate = self;
        ((CalcVC *)destVC.visibleViewController).amount = (self.asset.amount) ? self.asset.amount : self.amount;
        ((CalcVC *)destVC.visibleViewController).currencyCode = (self.asset.amount) ? self.asset.amount.code : self.amount.code;
    }
}
 
#pragma mark - Helpers
 
- (void)validate
{
    UIBarButtonItem *item = self.navigationItem.rightBarButtonItem;
    if (self.titleTextField.text.length>0 &&
        self.contractor) {
        item.enabled = YES;
    }
    else{
        item.enabled = NO;
    }
}

- (void)dismissKeyboard {
    //[self.calc hide];
    [self.view endEditing:YES];
}


- (void)delete:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Удаление имущества повлечет за собой удаление всех связанных с ним данных"
                                                       delegate:self
                                              cancelButtonTitle:@"Отмена"
                                              otherButtonTitles:@"Удалить", nil];
    alertView.delegate = self;
    [alertView show];
}

- (void)saveAsset:(Asset *)asset
{
    [self dismissKeyboard];
    asset.title = self.titleTextField.text;
    if (self.contractor) {
        [asset setOwner:self.contractor];
    }
    else{
        [asset setOwner:self.asset.owner];
    }
    if(!asset.amount)asset.amount = self.amount;
    if(self.photoName)asset.photo = self.photoName;
    asset.info      = self.infoTextView.text;
    //[asset setPhotos:[NSSet setWithArray:self.assetPhotos]];
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
}



 
#pragma mark - UITableView DataSource
 
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0 && [self contractor]) {
        ContractorCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"ContractorCellPhoto" owner:self options:nil]lastObject];
        NSString *name = [NSString stringWithFormat:@"%@ %@", (self.contractor.name!=nil)?self.contractor.name:@" ", (self.contractor.lastName!=nil)?self.contractor.lastName:@" "];
        cell.nameLabel.text = name;
        if (self.contractor.photo) {
            UIImage *image = [UIImage imageFromDocsByName:self.contractor.photo];
            [cell.photoImageView setImage:image];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    if (indexPath.section == 2 && indexPath.row==0 && !self.asset) {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        cell.separatorInset = UIEdgeInsetsMake(0, 320.0f, 0, 0);
    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}


 
#pragma mark - UITableView Delegate
 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self showImagesMenu];
        }
        else if (indexPath.row == 2)
        {
            [self performSegueWithIdentifier:kCalcVCSegue sender:nil];
        }
    }
    else if (indexPath.section == 1){
        if (indexPath.row == 0)
        {
            [self performSegueWithIdentifier:@"ContractorsVC" sender:nil];
        }
        else if (indexPath.row == 1)
        {
            [self performSegueWithIdentifier:@"HistoryVC" sender:nil];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0 && self.asset.photo) {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath]-30.0f;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1 && self.contractor){
        return 20.0f;
    }
    else if (section == 2){
        return 20.0f;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1 && self.contractor) {
        TableSection *tableSection = [[TableSection alloc] init];
        tableSection.titleLabel = @"владелец";
        return tableSection;
    }
    else if (section == 2){
        TableSection *tableSection = [[TableSection alloc] init];
        tableSection.titleLabel = @"информация";
        return tableSection;
    }
    return [UIView new];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self dismissKeyboard];
}

 
#pragma mark - Contractor Table View Delegate
 
- (void)didSelectContractor:(Contractor *)contractor
{
    [self setContractor:contractor];
    [self validate];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 1:
            for (Debenture *assetDebt in self.asset.history) {
                [assetDebt MR_deleteEntity];
            }
            [self.asset MR_deleteEntity];
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            self.asset = nil;
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate updateTable];
            break;
            
        default:
            break;
    }
}

 
#pragma mark - Actions


- (void)save:(id)sender
{
    Asset *asset = [Asset MR_createEntity];
    [self saveAsset:asset];
    [self.delegate didSaveAsset];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Calc Delegate Methods

- (void)calcDidPressEnterWithAmount:(Amount *)amount
{
    [self dismissKeyboard];
    if(self.asset)self.asset.amount = amount;
    
    self.priceLabel.text = [[AppHelper currencyFormatterWithCode:amount.code] stringFromNumber:amount.amount];
}



#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self validate];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

 
#pragma mark - UITextView Delegate
 
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // For some reason, the 'range' parameter isn't always correct when backspacing through a phone number
    // This calculates the proper range from the text field's selection range.
    
    if (range.location == textField.text.length && [string isEqualToString:@" "]) {
        // ignore replacement string and add your own
        textField.text = [textField.text stringByAppendingString:@"\u00a0"];
        return NO;
    }
    return YES;
    
}

#pragma mark - Work With Images

- (void) showImagesMenu
{
    if (self.photoName)
    {
        DoActionSheet *vActionSheet = [[DoActionSheet alloc] init];
        vActionSheet.nDestructiveIndex = 3;
        [vActionSheet showC:nil
                     cancel:LSTRING(@"Cancel")
                    buttons:@[LSTRING(@"Take a picture"),LSTRING(@"Browse gallery"),LSTRING(@"Edit"),LSTRING(@"Delete")]
                     result:^(int nResult) {
                         NSLog(@"---------------> result : %d", nResult);
                         switch (nResult) {
                             case 0:
                                 [self openPhotoCameraController];
                                 break;
                             case 1:
                                 [self openPhotoGalleryController];
                                 break;
                             case 2:
                                 [self showCroppControllerForCurrentPhoto];
                                 break;
                             case 3:
                                 //Удаление фотографии
                                 self.asset.photo = nil;
                                 self.photoName = nil;
                                 [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                                 self.photoImageView.image = [UIImage imageNamed:@"photoPlaceholder"];
                                 break;
                             default:
                                 break;
                         }
                     }];
    }
    else{
        DoActionSheet *vActionSheet = [[DoActionSheet alloc] init];
        [vActionSheet showC:nil
                     cancel:LSTRING(@"Cancel")
                    buttons:@[LSTRING(@"Take a picture"), LSTRING(@"Browse gallery")]
                     result:^(int nResult) {
                         NSLog(@"---------------> result : %d", nResult);
                         switch (nResult) {
                             case 0:
                                 [self openPhotoCameraController];
                                 break;
                             case 1:
                                 [self openPhotoGalleryController];
                                 break;
                             case 2:
                                 [self showCroppControllerForCurrentPhoto];
                                 break;
                             default:
                                 break;
                         }
                     }];
    }
}


- (void)showCroppControllerForCurrentPhoto
{
    NSString *originalpath = [self.photoName stringByReplacingOccurrencesOfString:@"_thumb"
                                                                  withString:@"_original"];
    UIImage *image = [UIImage imageFromDocsByName:originalpath];
    
    [self openImageCropperWithImage:image];
}

- (void)openImageCropperWithImage:(UIImage *)image
{
    ImageCropController *imageCropController = [[ImageCropController alloc] init];
    imageCropController.image = image;
    imageCropController.delegate = self;
    MainNavigationController *mvc = [[MainNavigationController alloc] initWithRootViewController:imageCropController];
    [self presentViewController:mvc animated:YES completion:nil];
}


#pragma mark - Camera

- (void)openPhotoCameraController
{
    UIImagePickerController *photosCameraPickerController = [[UIImagePickerController alloc] init];
    photosCameraPickerController.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        photosCameraPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        photosCameraPickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self.navigationController presentViewController:photosCameraPickerController
                                            animated:YES
                                          completion:nil];
}


#pragma mark - Gallery

- (void)openPhotoGalleryController
{
    UIImagePickerController *photosCameraPickerController = [[UIImagePickerController alloc] init];
    photosCameraPickerController.delegate = self;
    photosCameraPickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    [self.navigationController presentViewController:photosCameraPickerController
                                            animated:YES
                                          completion:nil];
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    originalImage = [originalImage fixOrientation];
    [picker dismissViewControllerAnimated:YES completion:^{
        ImageCropController *imageCropController = [[ImageCropController alloc] init];
        imageCropController.image = originalImage;
        imageCropController.delegate = self;
        MainNavigationController *mvc = [[MainNavigationController alloc] initWithRootViewController:imageCropController];
        [self presentViewController:mvc animated:YES completion:nil];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didCropAndSaveImageToDocumentsWithName:(NSString *)filename
{
    if ( self.photoName) {
        // TODO: remove old images
    }
    self.photoName = filename;
    self.photoImageView.image = [UIImage imageFromDocsByName:filename];
    
}

@end
