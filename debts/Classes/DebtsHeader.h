//
//  DebtsHeader.h
//  debts
//
//  Created by Evgeny on 21.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebtsHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *intAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;

@end
