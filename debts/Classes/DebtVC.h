//
//  DebtVC.h
//  debts
//
//  Created by Evgeny on 26.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DTViewController.h"
#import "ContractorsTVC.h"
#import "AssetsTVC.h"
#import "CurrencyViewController.h"
#import "CalendarViewController.h"
#import "UIPlaceHolderTextView.h"
#import "CalcVC.h"
@protocol DebtVCDelegate <NSObject>
@optional
- (void) updateTable;
@end

@interface DebtVC : DTViewController
<UITextViewDelegate,
UIAlertViewDelegate,
ContractorTableViewDelegate,
AssetsTableViewDelegate,
CalcVCDelegate,
CalendarViewControllerDelegate,
UITabBarDelegate,
UITableViewDataSource,
UITableViewDelegate>
{
    __weak id<DebtVCDelegate> delegate;
}
@property (nonatomic, weak)   id<DebtVCDelegate> delegate;
@property (nonatomic, assign) DebtType type;
@property (nonatomic, strong) Debenture *debt;



@end
