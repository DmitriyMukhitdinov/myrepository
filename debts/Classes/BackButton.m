//
//  BackButton.m
//  debts
//
//  Created by Evgeny on 22.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "BackButton.h"

@implementation BackButton


+ (BackButton *)button
{
    BackButton *backButton = [[BackButton alloc] initWithFrame: CGRectMake(0, 0, 8.0f, 15.0f)];
    UIImage *backImage = [UIImage imageNamed:@"backArraw"];
    [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [backButton setBackgroundImage:backImage  forState:UIControlStateHighlighted];
    [backButton setTitle:@"" forState:UIControlStateNormal];
    return backButton;
}

- (UIEdgeInsets)alignmentRectInsets {
    UIEdgeInsets insets;
    insets = UIEdgeInsetsMake(0, 9.0f, 0, 0);
    return insets;
}

@end
