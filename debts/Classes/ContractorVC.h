//
//  ContractorVC.h
//  debts
//
//  Created by Evgeny on 25.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DTTableViewController.h"
#import "Contractor.h"
#import <AddressBookUI/AddressBookUI.h>
#import "GreenButton.h"
#import "ImageCropController.h"

@protocol ContractorVCDelegate <NSObject>
@optional
- (void) updateTable;
- (void) didSaveContractor;
@end

@interface ContractorVC : DTTableViewController
<ABPeoplePickerNavigationControllerDelegate,
UIGestureRecognizerDelegate,
UITextFieldDelegate,
UIImagePickerControllerDelegate,
ImageCropControllerDelegate,
UINavigationControllerDelegate>
{
    __weak id<ContractorVCDelegate> delegate;
}
@property (nonatomic, weak)   id<ContractorVCDelegate> delegate;
@property (strong, nonatomic) Contractor *contractor;

@property (weak, nonatomic) IBOutlet GreenButton *contactsListButton;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *addPhotoLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@property (weak, nonatomic) IBOutlet UITableViewCell *historyCell;
@end
