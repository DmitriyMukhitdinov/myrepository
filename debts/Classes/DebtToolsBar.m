//
//  DebtToolsBar.m
//  debts
//
//  Created by Evgeny on 26.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DebtToolsBar.h"

@implementation DebtToolsBar
-(void)awakeFromNib
{
    self.barTintColor = [UIColor whiteColor];
    self.barStyle = UIBarStyleBlack;
    [self setTranslucent:NO];
    self.backgroundImage = [UIImage imageNamed:@"tabBar"];
    self.tintColor = [UIColor whiteColor];
    self.selectedImageTintColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];
    self.selectionIndicatorImage = [UIImage imageNamed:@"selectedTabItem"];
    for (UITabBarItem *item in self.items) {
        UIImage *image = [item.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item.image = image;
        item.selectedImage = image;

        item.titlePositionAdjustment = UIOffsetMake(0, -5);
    }
}


@end
