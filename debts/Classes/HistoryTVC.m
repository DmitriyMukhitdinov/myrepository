//
//  HistoryTVC.m
//  debts
//
//  Created by Evgeny on 29.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "HistoryTVC.h"
#import "Contractor.h"
#import "Asset.h"
#import "Refund.h"
#import "Debenture.h"
#import "Amount.h"
#import "Photos.h"
#import "DateChange.h"
#import "Rate.h"

@interface HistoryItem: NSObject
@property (nonatomic, strong) NSString  *text1;
@property (nonatomic, strong) NSString  *text2;
@property (nonatomic, strong) NSString  *text3;
@property (nonatomic, strong) NSDate    *date;
@end

@implementation HistoryItem

@end

@interface HistoryTVC ()
@property (nonatomic, strong) NSArray   *tableData;
@property (nonatomic, strong) NSString  *status;
@property (nonatomic, strong) NSArray   *assetDebts;
@property (nonatomic, strong) NSMutableArray *anotherCurrencyRows;
@end

@implementation HistoryTVC

 
#pragma mark - History
 
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateTableData];
    self.title = LSTRING(@"History");
	//Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //Dispose of any resources that can be recreated.
}

- (Contractor *)contractor
{
    return (Contractor *)self.item;;
}
 
#pragma mark - Data
 
- (void)updateTableData
{
    NSMutableArray *elements = [[NSMutableArray alloc] init];
    NSNumber *total = [NSNumber numberWithInt:0];
    NSMutableArray *activeAssetDebts = [NSMutableArray new];
    if ([self.item isKindOfClass:[Contractor class]]) {
        Contractor *contractor = (Contractor *)self.item;
        for (Debenture *debt in contractor.history) {
            HistoryItem *debtStart = [[HistoryItem alloc] init];
            debtStart.date = debt.created_at;
            if ([debt.type isEqualToString:DebtType_toString[DebtTypeAssetMine]]) {
                if ([debt.contractor isWoman]) {
                    debtStart.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"she take my asset"),debt.asset.title];
                }
                else debtStart.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"take my asset"),debt.asset.title];
                debtStart.text2 = @"";
                for (Refund *refund in debt.refunds) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date     = refund.created_at;
                    if ([debt.contractor isWoman]) {
                        refundItem.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"she return asset to me"),debt.asset.title];
                    }
                    else refundItem.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"return asset to me"),debt.asset.title];
                    refundItem.text2    = @"";
                    [elements addObject:refundItem];
                }
                //перенос дат
                for (DateChange *dateChange in debt.dateChanges) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date = dateChange.created_at;
                    if (dateChange.toDate) {
                        if ([debt.contractor isWoman]) {
                            refundItem.text1 = [NSString stringWithFormat:LSTRING(@"she change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                        }
                        else refundItem.text1 = [NSString stringWithFormat:LSTRING(@"change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                    }
                    else{
                        if ([contractor isWoman]) {
                            refundItem.text1 = LSTRING(@"she Cancel return date");
                        }
                        else{
                            refundItem.text1 = LSTRING(@"Cancel return date");
                        }
                    }
                    [elements addObject:refundItem];
                }
                if (![debt.finished isEqualToNumber:[NSNumber numberWithInt:1]])
                {
                    [activeAssetDebts addObject:debt];
                }
            }
            else if ([debt.type isEqualToString:DebtType_toString[DebtTypeAssetSomeone]]){
                if ([debt.contractor isWoman]) {
                    debtStart.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"she give me asset"),debt.asset.title];
                }
                else debtStart.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"give me asset"),debt.asset.title];
                debtStart.text2 = @"";
                for (Refund *refund in debt.refunds) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date     = refund.created_at;
                    if ([debt.contractor isWoman]) {
                        refundItem.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"she get asset back"),debt.asset.title];
                    }
                    else refundItem.text1    = [NSString stringWithFormat:@"%@ %@",LSTRING(@"get asset back"),debt.asset.title];
                    refundItem.text2    = @"";
                    [elements addObject:refundItem];
                }
                //перенос дат
                for (DateChange *dateChange in debt.dateChanges) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date = dateChange.created_at;
                    if (dateChange.toDate) {
                        if ([debt.contractor isWoman]) {
                            refundItem.text1 = [NSString stringWithFormat:LSTRING(@"she change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                        }
                        else refundItem.text1 = [NSString stringWithFormat:LSTRING(@"change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                    }
                    else{
                        if ([contractor isWoman]) {
                            refundItem.text1 = LSTRING(@"she Cancel return date");
                        }
                        else{
                            refundItem.text1 = LSTRING(@"Cancel return date");
                        }
                    }
                    [elements addObject:refundItem];
                }
                if (![debt.finished isEqualToNumber:[NSNumber numberWithInt:1]])
                {
                    [activeAssetDebts addObject:debt];
                }
            }
            else if ([debt.type isEqualToString:DebtType_toString[DebtTypeMoneyMine]]){
                if ([debt.contractor isWoman]) {
                    debtStart.text1 = LSTRING(@"she take my money");
                }
                else debtStart.text1 = LSTRING(@"take my money");
                debtStart.text2 = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:debt.money];
                
                if (![debt.finished isEqual:[NSNumber numberWithInt:1]]) {
                    if ([debt.total.code isEqualToString:[Preferences defaultCurrencyCode]]) {
                        total = [NSNumber numberWithFloat:[total floatValue] + [[debt debtBalance] floatValue]];
                    }
                    else{
                        total = [NSNumber numberWithFloat:[total floatValue] + [[debt debtBalance] floatValue]*[[[Rate sharedInstance] rateForCurrencyCode:debt.total.code] floatValue]];
                    }
                }
                
                for (Refund *refund in debt.refunds) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date     = refund.created_at;
                    if ([debt.contractor isWoman]) {
                        refundItem.text1    = LSTRING(@"she return to me");
                    }
                    else refundItem.text1    = LSTRING(@"return to me");
                    refundItem.text2    = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:refund.amount.amount];
                    [elements addObject:refundItem];
                }
                //перенос дат
                for (DateChange *dateChange in debt.dateChanges) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date = dateChange.created_at;
                    if (dateChange.toDate) {
                        if ([debt.contractor isWoman]) {
                            refundItem.text1 = [NSString stringWithFormat:LSTRING(@"she change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                        }
                        else refundItem.text1 = [NSString stringWithFormat:LSTRING(@"change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                    }
                    else{
                        if ([contractor isWoman]) {
                            refundItem.text1 = LSTRING(@"she Cancel return date");
                        }
                        else{
                            refundItem.text1 = LSTRING(@"Cancel return date");
                        }
                    }
                    [elements addObject:refundItem];
                }
            }
            else if ([debt.type isEqualToString:DebtType_toString[DebtTypeMoneySomeone]]){
                if (![debt.finished isEqual:[NSNumber numberWithInt:1]]) {
                    if ([debt.total.code isEqualToString:[Preferences defaultCurrencyCode]]) {
                        total = [NSNumber numberWithFloat:[total floatValue] - [[debt debtBalance] floatValue]];
                    }
                    else{
                        total = [NSNumber numberWithFloat:[total floatValue] - [[debt debtBalance] floatValue]*[[[Rate sharedInstance] rateForCurrencyCode:debt.total.code] floatValue]];
                    }
                }
                if ([debt.contractor isWoman]) {
                    debtStart.text1 = LSTRING(@"she give mone to me");
                }
                else debtStart.text1 = LSTRING(@"give mone to me");
                debtStart.text2 = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:debt.money];
                for (Refund *refund in debt.refunds) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date     = refund.created_at;
                    if ([debt.contractor isWoman]) {
                        refundItem.text1    = LSTRING(@"she get");
                    }
                    else refundItem.text1    = LSTRING(@"get");
                    refundItem.text2    = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:refund.amount.amount];
                    [elements addObject:refundItem];
                }
                //перенос дат
                for (DateChange *dateChange in debt.dateChanges) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date = dateChange.created_at;
                    if (dateChange.toDate) {
                        if ([debt.contractor isWoman]) {
                            refundItem.text1 = [NSString stringWithFormat:LSTRING(@"she change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                        }
                        else refundItem.text1 = [NSString stringWithFormat:LSTRING(@"change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                    }
                    else{
                        if ([contractor isWoman]) {
                            refundItem.text1 = LSTRING(@"she Cancel return date");
                        }
                        else{
                            refundItem.text1 = LSTRING(@"Cancel return date");
                        }
                    }
                    [elements addObject:refundItem];
                }
            }
            [elements addObject:debtStart];
        }
        
        NSArray *contractorOwnDebts = [[contractor.history filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"finished !=1 AND type =='DebtTypeMoneySomeone'"]]allObjects];
        NSArray *ownToContractorDebts = [[contractor.history filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"finished !=1 AND type =='DebtTypeMoneyMine'"]]allObjects];
        NSMutableArray *rateRows = [NSMutableArray new];
        float totalF = 0;
        for (Debenture * obj in ownToContractorDebts) {
            //Должны контрагенту
            if ([obj.total.code isEqualToString:[Preferences defaultCurrencyCode]]) {
                continue;
            }
            NSNumber *rate = [[Rate sharedInstance] rateForCurrencyCode:obj.total.code];
            if (!rate) {
                //Добавляется строка для этой валюты
                if (![rateRows containsObject:obj.total.code]) {
                    [rateRows addObject:obj.total.code];
                }
            }
            else{
                //Конвертация
                totalF+=[[obj debtBalance] floatValue]*[rate floatValue];
            }
        }
        
        for (Debenture * obj in contractorOwnDebts) {
            //Контрагент должен
            if ([obj.total.code isEqualToString:[Preferences defaultCurrencyCode]]) {
                continue;
            }
            NSNumber *rate = [[Rate sharedInstance] rateForCurrencyCode:obj.total.code];
            if (!rate) {
                //Добавляется строка для этой валюты
                if (![rateRows containsObject:obj.total.code]) {
                    [rateRows addObject:obj.total.code];
                }
            }
            else{
                //Конвертация
                totalF-=[[obj debtBalance] floatValue]*[rate floatValue];
            }
        }
        
        if (totalF==0 && activeAssetDebts.count>0 && rateRows.count==0) {
            self.status = nil;
        }
        else{
            self.status = [self statusForSum:[total floatValue]];
            for (NSString *currCode in rateRows) {
                float sumForCurrency = 0;
                NSArray *debtForCurrency = [ownToContractorDebts filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"total.code == %@",currCode]];
                for (Debenture *debtObj in debtForCurrency) {
                    sumForCurrency+=[[debtObj debtBalance] floatValue];
                }
                NSArray *debtForCurrency1 = [contractorOwnDebts filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"total.code == %@",currCode]];
                for (Debenture *debtObj in debtForCurrency1) {
                    sumForCurrency-=[[debtObj debtBalance] floatValue];
                }
                if (sumForCurrency!=0) {
                    [self.anotherCurrencyRows addObject:@{@"currency": currCode, @"sum":[NSNumber numberWithFloat:sumForCurrency]}];
                }
            }
        }
    }
    else{//Asset
        Asset *asset = (Asset *)self.item;
        for (Debenture *debt in asset.history) {
            HistoryItem *debtStart = [[HistoryItem alloc] init];
            debtStart.date = debt.created_at;
            if ([debt.type isEqualToString:DebtType_toString[DebtTypeAssetMine]]) {
                if ([debt.contractor isWoman]) {
                    debtStart.text1 = [NSString stringWithFormat:LSTRING(@"she take my %@"),debt.contractor.name];
                }
                else debtStart.text1 = [NSString stringWithFormat:LSTRING(@"take my %@"),debt.contractor.name];
                debtStart.text2 = @"";
                for (Refund *refund in debt.refunds) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date     = refund.created_at;
                    if ([debt.contractor isWoman]) {
                        refundItem.text1    = [NSString stringWithFormat:LSTRING(@"she return to me %@"),debt.contractor.name];
                    }
                    else refundItem.text1    = [NSString stringWithFormat:LSTRING(@"return to me %@"),debt.contractor.name];
                    refundItem.text2    = @"";//debt.asset.title;
                    refundItem.text3 = @"";
                    [elements addObject:refundItem];
                }
            }
            else if ([debt.type isEqualToString:DebtType_toString[DebtTypeAssetSomeone]]){
                if ([debt.contractor isWoman]) {
                    debtStart.text1 = [NSString stringWithFormat:LSTRING(@"she give to me %@"),debt.contractor.name];
                }
                else debtStart.text1 = [NSString stringWithFormat:LSTRING(@"give to me %@"),debt.contractor.name];
                debtStart.text2 = @"";
                for (Refund *refund in debt.refunds) {
                    HistoryItem *refundItem = [[HistoryItem alloc] init];
                    refundItem.date     = refund.created_at;
                    refundItem.text1    = [NSString stringWithFormat:LSTRING(@"i retutrn to %@"),debt.contractor.name];
                    refundItem.text2    = @"";//debt.asset.title;
                    refundItem.text3 = @"";
                    [elements addObject:refundItem];
                }
            }
            [elements addObject:debtStart];
        }
        //self.assetStatusLabel.text = [self statusForAsset];
    }
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    [elements sortUsingDescriptors:@[sd]];
    self.tableData = elements;
    _assetDebts = activeAssetDebts;
    
}

- (NSMutableArray *)anotherCurrencyRows
{
    if (!_anotherCurrencyRows) {
        _anotherCurrencyRows = [NSMutableArray new];
    }
    return _anotherCurrencyRows;
}

- (NSString *)statusForSum:(float)total
{
    static NSString *status = nil;
    
    if (total>0) {
        if ([[self contractor] isWoman]) {
            status = LSTRING(@"she own to me");
        }
        else status = LSTRING(@"he own to me");
    }
    else if(total<0)
    {
        if ([[self contractor] isWoman]) {
            status = LSTRING(@"i own to her");
        }
        else status = LSTRING(@"i own to him");
    }
    else{
        return nil; //LSTRING(@"we are done");
    }
    return [NSString stringWithFormat:@"%@ %@", status,[[AppHelper currencyFormatterWithCode:nil] stringFromNumber:[NSNumber numberWithFloat:fabsf(total)]]];
}



- (NSString *)statusForAsset
{
    Asset *asset = (Asset *)self.item;
    NSArray *debts = [[asset.history filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"finished !=true"]] allObjects];
    if ([debts lastObject]) {
        Debenture *debt = [debts lastObject];
        if ([asset.owner isEqual:[AppHelper contractorMe]]) {
            return [NSString stringWithFormat:LSTRING(@"now contractor %@"),debt.contractor.name];
        }
        else{
            return [NSString stringWithFormat:LSTRING(@"now contractor %@"),@"меня"];
        }
    }
    else{
        if ([asset.owner isEqual:[AppHelper contractorMe]]) {
            return [NSString stringWithFormat:LSTRING(@"now contractor %@"),@"меня"];
        }
        else{
            return [NSString stringWithFormat:LSTRING(@"now contractor %@"),asset.owner.name];
        }
    }
    return @"";
}

 
#pragma mark - UITableView Datasourse
 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }
    else if (section==1){
        if ([self.item isKindOfClass:[Asset class]]) {
            return 1;
        }
        if (self.status) {
            return self.assetDebts.count+self.anotherCurrencyRows.count+1;
        }
        else if(self.status == nil && self.anotherCurrencyRows.count==0 && self.assetDebts.count == 0){
            return 1;
        }
        else{
            return self.assetDebts.count+self.anotherCurrencyRows.count;
        }
    }
    return self.tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell"];
        UILabel *label1 = (UILabel *)[cell viewWithTag:1010];
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1011];
        if ([self.item isKindOfClass:[Contractor class]]) {
            Contractor *contractor = (Contractor *)self.item;
            label1.text = contractor.name;
            if (contractor.photo) {
                imageView.image = [UIImage imageFromDocsByName:contractor.photo];
            }
        }
        else{
            //Asset
            Asset *asset = (Asset *)self.item;
            label1.text = asset.title;
            if (asset.photo) {
                imageView.image = [UIImage imageFromDocsByName:asset.photo];
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    if (indexPath.section==1) {
        if((indexPath.row==0 && self.status) || [self.item isKindOfClass:[Asset class]]){
            static NSString *CellIdentifier = @"StatusCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UILabel *statusLabel      = (UILabel *)[cell viewWithTag:1010];
            if (![self.item isKindOfClass:[Asset class]]) {
                statusLabel.text = self.status;
            }
            else{
                statusLabel.text = [self statusForAsset];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else {
            if (indexPath.row == 0) {
                if (self.status ||
                    (self.status == nil && self.anotherCurrencyRows.count==0) ||
                    [self.item isKindOfClass:[Asset class]])
                {
                    if (!self.status && self.assetDebts.count == 0) {
                        self.status = @"мы в расчете";
                    }
                    else if (self.assetDebts.count>0)
                    {
                        Debenture *debt = [self.assetDebts lastObject];
                        if ([debt debtType] == DebtTypeAssetMine) {
                            if ([[self contractor] isWoman]) {
                                self.status = [NSString stringWithFormat:@"должна мне %@", debt.asset.title];
                            }
                            else self.status = [NSString stringWithFormat:@"должен мне %@", debt.asset.title];
                        }
                        else{
                            if ([[self contractor] isWoman]) {
                                self.status = [NSString stringWithFormat:@"должен ей %@", debt.asset.title];
                            }
                            else self.status = [NSString stringWithFormat:@"должен ему %@", debt.asset.title];
                        }
                    }
                    static NSString *CellIdentifier = @"StatusCell";
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    UILabel *statusLabel      = (UILabel *)[cell viewWithTag:1010];
                    if ([self.item isKindOfClass:[Contractor class]]) {
                        statusLabel.text = self.status;
                    }
                    else{
                        statusLabel.text = [self statusForAsset];
                    }
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
            }
            long rowIndex = indexPath.row-((self.status)?1:0);
            if (rowIndex<self.anotherCurrencyRows.count) {
                
                static NSString *CellIdentifier = @"StatusCell";
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                UILabel *statusLabel      = (UILabel *)[cell viewWithTag:1010];
                
                id obj = self.anotherCurrencyRows[rowIndex];
                NSNumber *debtValue = [obj objectForKey:@"sum"];
                if ([debtValue floatValue]>0) {
                    debtValue = [NSNumber numberWithFloat:fabsf([debtValue floatValue])];
                    if ([[self contractor] isWoman]) {
                        statusLabel.text = [NSString stringWithFormat:@"должна мне %@", [[AppHelper currencyFormatterWithCode:[obj objectForKey:@"currency"]] stringFromNumber:debtValue]];
                    }
                    else statusLabel.text = [NSString stringWithFormat:@"должен мне %@", [[AppHelper currencyFormatterWithCode:[obj objectForKey:@"currency"]] stringFromNumber:debtValue]];
                }
                else{
                    debtValue = [NSNumber numberWithFloat:fabsf([debtValue floatValue])];
                    if ([[self contractor] isWoman]) {
                        statusLabel.text = [NSString stringWithFormat:@"должен ей %@", [[AppHelper currencyFormatterWithCode:[obj objectForKey:@"currency"]] stringFromNumber:debtValue]];
                    }
                    else statusLabel.text = [NSString stringWithFormat:@"должен ему %@", [[AppHelper currencyFormatterWithCode:[obj objectForKey:@"currency"]] stringFromNumber:debtValue]];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else{
                static NSString *CellIdentifier = @"StatusCell";
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                UILabel *statusLabel      = (UILabel *)[cell viewWithTag:1010];
                
                //rowIndex+=self.anotherCurrencyRows.count;
                Debenture *debt = self.assetDebts[rowIndex-self.anotherCurrencyRows.count];
                if ([debt debtType] == DebtTypeAssetMine) {
                    if ([[self contractor] isWoman]) {
                        statusLabel.text = [NSString stringWithFormat:@"должна мне %@", debt.asset.title];
                    }
                    else statusLabel.text = [NSString stringWithFormat:@"должен мне %@", debt.asset.title];
                }
                else{
                    if ([[self contractor] isWoman]) {
                        statusLabel.text = [NSString stringWithFormat:@"должен ей %@", debt.asset.title];
                    }
                    else statusLabel.text = [NSString stringWithFormat:@"должен ему %@", debt.asset.title];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
    }
    else{
        static NSString *CellIdentifier = @"HistoryCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *labelDate      = (UILabel *)[cell viewWithTag:1010];
        UILabel *labelStatus    = (UILabel *)[cell viewWithTag:1011];
        UILabel *labelItem      = (UILabel *)[cell viewWithTag:1012];
        
        HistoryItem *item   = self.tableData[indexPath.row];
        labelDate.text      = [[AppHelper dateFormatter] stringFromDate:item.date];
        labelStatus.text    = item.text1;
        [labelStatus sizeToFit];
        labelItem.text      = item.text2;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

 
#pragma mark - UITableView Delegate
 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 135.0f;
    }
    else{
        return 50.0f;
    }
}



@end
