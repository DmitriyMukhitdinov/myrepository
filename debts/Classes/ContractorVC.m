//
//  ContractorVC.m
//  debts
//
//  Created by Evgeny on 25.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "ContractorVC.h"
#import "HistoryTVC.h"
#import "DoActionSheet.h"
#import "RMPhoneFormat.h"
#import "GenderTVC.h"
/*
 ======================================
                Phone
 ======================================
 */

@interface Phone: NSObject
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *numberType;
@end
@implementation Phone
@synthesize number, numberType;
@end

//=====================================


@interface ContractorVC ()
{
    NSMutableCharacterSet *_phoneChars;
}
@property (nonatomic, strong) NSArray       *pickerViewDataSource;
@property (nonatomic, strong) UIView        *pickerViewContainer;
@property (nonatomic, strong) UIPickerView  *pickerView;
@property (nonatomic, strong) NSString      *photoName;
@property (nonatomic, strong) RMPhoneFormat *phoneFormatter;
@property (nonatomic, strong) NSManagedObjectContext *context;
@end

@implementation ContractorVC
@synthesize delegate;

 
#pragma mark - Lifecycle
 
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.nameTextField.delegate = self;
    self.phoneTextField.delegate = self;
    
    self.phoneFormatter = [[RMPhoneFormat alloc] init];
    _phoneChars = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
    [_phoneChars addCharactersInString:@"+*#,"];
    
    if (self.contractor == nil) {
        _context = [NSManagedObjectContext MR_context];
        self.contractor = [Contractor MR_createInContext:self.context];
        self.historyCell.hidden = YES;
        self.title = @"Новый контрагент";
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Save") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        rightBarButton.enabled = NO;
        
        self.nameTextField.text     = @"";
        self.phoneTextField.text    = @"";
        
        BarButton *leftButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [leftButton setTitle:LSTRING(@"Cancel") forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
        self.navigationItem.leftBarButtonItem = leftBarButton;
        leftBarButton.enabled = YES;
    }
    else{
        _context = [NSManagedObjectContext MR_contextForCurrentThread];
        self.title = @"Редактирование";
        self.contactsListButton.hidden = YES;
        NSString *name = [NSString stringWithFormat:@"%@ %@", (self.contractor.name!=nil)?self.contractor.name:@" ", (self.contractor.lastName!=nil)?self.contractor.lastName:@" "];
        self.nameTextField.text = name;
        self.phoneTextField.text = self.contractor.phone;
        if (self.contractor.photo) {
            self.photoImageView.image = [UIImage imageFromDocsByName:self.contractor.photo];
            self.photoName = self.contractor.photo;
            self.addPhotoLabel.text = @"";
        }
        if (self.contractor!= [AppHelper contractorMe])
        {
            BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
            [rightButton setTitle:LSTRING(@"delete") forState:UIControlStateNormal];
            [rightButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
            self.navigationItem.rightBarButtonItem = rightBarButton;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.contractor) {
        [self saveContractor:self.contractor];
        [self.delegate updateTable];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"HistoryVC"]){
        HistoryTVC *historyTVC = segue.destinationViewController;
        historyTVC.item = self.contractor;
    }
}
 
#pragma mark - Helpers
 
- (void)validate
{
    UIBarButtonItem *item = self.navigationItem.rightBarButtonItem;
    if (self.nameTextField.text.length>0) {
        item.enabled = YES;
    }
    else{
        item.enabled = NO;
    }
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint touchLocation = [touch locationInView:self.view];
    if (CGRectContainsPoint(self.pickerViewContainer.frame, touchLocation)) {
        return NO;
    }
    return YES;
}

- (void)saveContractor:(Contractor *)conractor
{
    conractor.name  = self.nameTextField.text;
    conractor.phone = self.phoneTextField.text;
    conractor.photo = self.photoName;
    [self.context MR_saveToPersistentStoreAndWait];
}


#pragma mark - UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // For some reason, the 'range' parameter isn't always correct when backspacing through a phone number
    // This calculates the proper range from the text field's selection range.
    
    if (![textField isEqual:self.phoneTextField]) {
        if (range.location == textField.text.length && [string isEqualToString:@" "]) {
            // ignore replacement string and add your own
            textField.text = [textField.text stringByAppendingString:@"\u00a0"];
            return NO;
        }
        return YES;
    }
    
    UITextRange *selRange = textField.selectedTextRange;
    UITextPosition *selStartPos = selRange.start;
    UITextPosition *selEndPos = selRange.end;
    NSInteger start = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStartPos];
    NSInteger end = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selEndPos];
    NSRange repRange;
    if (start == end) {
        if (string.length == 0) {
            repRange = NSMakeRange(start - 1, 1);
        } else {
            repRange = NSMakeRange(start, end - start);
        }
    } else {
        repRange = NSMakeRange(start, end - start);
    }
    
    // This is what the new text will be after adding/deleting 'string'
    NSString *txt = [textField.text stringByReplacingCharactersInRange:repRange withString:string];
    // This is the newly formatted version of the phone number
    NSString *phone = [self.phoneFormatter format:txt];
    BOOL valid = [self.phoneFormatter isPhoneNumberValid:phone];
    
    textField.textColor = valid ? [UIColor blackColor] : [UIColor redColor];
    
    // If these are the same then just let the normal text changing take place
    if ([phone isEqualToString:txt]) {
        return YES;
    } else {
        // The two are different which means the adding/removal of a character had a bigger effect
        // from adding/removing phone number formatting based on the new number of characters in the text field
        // The trick now is to ensure the cursor stays after the same character despite the change in formatting.
        // So first let's count the number of non-formatting characters up to the cursor in the unchanged text.
        int cnt = 0;
        for (NSUInteger i = 0; i < repRange.location + string.length; i++) {
            if ([_phoneChars characterIsMember:[txt characterAtIndex:i]]) {
                cnt++;
            }
        }
        
        // Now let's find the position, in the newly formatted string, of the same number of non-formatting characters.
        int pos = [phone length];
        int cnt2 = 0;
        for (NSUInteger i = 0; i < [phone length]; i++) {
            if ([_phoneChars characterIsMember:[phone characterAtIndex:i]]) {
                cnt2++;
            }
            
            if (cnt2 == cnt) {
                pos = i + 1;
                break;
            }
        }
        
        // Replace the text with the updated formatting
        textField.text = phone;
        
        // Make sure the caret is in the right place
        UITextPosition *startPos = [textField positionFromPosition:textField.beginningOfDocument offset:pos];
        UITextRange *textRange = [textField textRangeFromPosition:startPos toPosition:startPos];
        textField.selectedTextRange = textRange;
        
        return NO;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self validate];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

 
#pragma mark - Actions
 
- (IBAction)openContactsBook:(id)sender
{
    ABPeoplePickerNavigationController *picker =
    [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)donePhoneSelection:(id)sender // Called by phone picker toolbar button
{
    CGRect frame = self.pickerViewContainer.frame;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height;
    [UIView animateWithDuration:0.3f animations:^{
        self.pickerViewContainer.frame = frame;
        [self validate];
    }];
}

- (void)save:(id)sender
{
    self.contractor.type = @"somebody";
    [self saveContractor:self.contractor];
    [self.navigationController popViewControllerAnimated:YES];
    [self.delegate didSaveContractor];
}

- (void)delete:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Удаление контрагента повлечет за собой удаление всех связанных с ним данных"
                                                       delegate:self
                                              cancelButtonTitle:@"Отмена"
                                              otherButtonTitles:@"Удалить", nil];
    
    alertView.delegate = self;
    [alertView show];
}


#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 1:
            [self.contractor MR_deleteEntity];
            self.contractor = nil;
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate updateTable];
            break;
            
        default:
            break;
    }
}



#pragma mark - ABPeople Delegate Methods

-(void)openABPeoplePickerForDelegate
{
    ABPeoplePickerNavigationController *picker =
    [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    [picker setDisplayedProperties:[NSArray arrayWithObject:[NSNumber numberWithLong:kABPersonPhoneProperty]]];
    [self presentViewController:picker animated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    [peoplePicker setDisplayedProperties:[NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]]];
    return YES;
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier
{
    if (property == kABPersonPhoneProperty) {
        ABMultiValueRef emails = ABRecordCopyValue(person, property);
        CFStringRef phonenumberselected = ABMultiValueCopyValueAtIndex(emails, identifier);
        CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(emails, identifier);
        NSString *aNSString = (__bridge NSString *)phonenumberselected;
        NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
        
        NSString *firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
        NSString *lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
        NSString *biz = (__bridge NSString *)ABRecordCopyValue(person, kABPersonOrganizationProperty);
        
        NSString *contractorName = nil;
        if (firstName || lastName) {
            //Если человек
            contractorName =
            [NSString stringWithFormat:@"%@ %@",(lastName)?lastName:@"",(firstName)?firstName:@""];
            self.nameTextField.text = contractorName;
        }
        else if (biz){
            //Если предприятие
            contractorName = biz;
            self.nameTextField.text = biz;
        }
        Phone *phone = [[Phone alloc] init];
        phone.number = aNSString;
        phone.numberType = phoneLabel;
        self.phoneTextField.text = phone.number;
        [self dismissViewControllerAnimated:YES completion:nil];
        [self validate];
        return NO;
    }
    
    return NO;
    
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    [peoplePicker setDisplayedProperties:[NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]]];
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}
/*
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    NSString *firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    NSString *biz = (__bridge NSString *)ABRecordCopyValue(person, kABPersonOrganizationProperty);
    
    NSString *contractorName = nil;
    if (firstName || lastName) {
        //Если человек
        contractorName =
        [NSString stringWithFormat:@"%@ %@",(lastName)?lastName:@"",(firstName)?firstName:@""];
        self.nameTextField.text = contractorName;
    }
    else if (biz){
        //Если предприятие
        contractorName = biz;
        self.nameTextField.text = biz;
    }

    NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
    NSMutableArray *titles = [NSMutableArray new];
    ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
    {
        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
        CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
        NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
        NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
        if(phoneNumberRef)CFRelease(phoneNumberRef);
        if(locLabel)CFRelease(locLabel);
        Phone *phone = [[Phone alloc] init];
        phone.number = phoneNumber;
        phone.numberType = phoneLabel;
        [phoneNumbers addObject:phone];
        [titles addObject:[NSString stringWithFormat:@"%@ (%@)", phone.number, phone.numberType]];
    }
    if (phoneNumbers.count>1) {
        self.pickerViewDataSource = phoneNumbers;
        
        [self dismissViewControllerAnimated:YES completion:^{
            DoActionSheet *vActionSheet = [[DoActionSheet alloc] init];
            [vActionSheet showC:nil
                         cancel:LSTRING(@"Cancel")
                        buttons:titles
                         result:^(int nResult) {
                             NSLog(@"---------------> result : %d", nResult);
                             if (nResult>=0) {
                                 Phone *phone = self.pickerViewDataSource[nResult];
                                 self.phoneTextField.text = phone.number;
                                 [self validate];
                             }
                         }];
        }];
    }
    else if (phoneNumbers.count==1){
        Phone *phone = [phoneNumbers lastObject];
        self.phoneTextField.text = phone.number;
        [self dismissViewControllerAnimated:YES completion:nil];
        [self validate];
    }
    
    [peoplePicker setDisplayedProperties:[NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]]];
    return YES;
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    
    return NO;
}
*/
 
#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 3) {
        UILabel *label = (UILabel *)[cell viewWithTag:1010];
        if (self.contractor.sex && [self.contractor.sex intValue] == 1) {
            label.text = @"Мужской";
        }
        else if (self.contractor.sex && [self.contractor.sex intValue] == 0) {
            label.text = @"Женский";
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    if (indexPath.row == 5)
    {
        [self performSegueWithIdentifier:@"HistoryVC" sender:nil];
    }
    else if (indexPath.row == 1)
    {
        [self showImagesMenu];
    }
    else if (indexPath.row==3){
        GenderTVC *genderTVC = [[GenderTVC alloc] init];
        genderTVC.contractor = self.contractor;
        [self.navigationController pushViewController:genderTVC animated:YES];
    }
}
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && [self.context isEqual:[NSManagedObjectContext MR_contextForCurrentThread]]) {
        return 0;
    }
    if (indexPath.row == 1 && self.contractor.photo) {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath]-30.0f;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self dismissKeyboard];
}


#pragma mark - Work With Images

- (void) showImagesMenu
{
    if (self.contractor.photo )
    {
        DoActionSheet *vActionSheet = [[DoActionSheet alloc] init];
        vActionSheet.nDestructiveIndex = 3;
        [vActionSheet showC:nil
                     cancel:LSTRING(@"Cancel")
                    buttons:@[LSTRING(@"Take a picture"),LSTRING(@"Browse gallery"),LSTRING(@"Edit"),LSTRING(@"Delete")]
                     result:^(int nResult) {
                         NSLog(@"---------------> result : %d", nResult);
                         switch (nResult) {
                             case 0:
                                 [self openPhotoCameraController];
                                 break;
                             case 1:
                                 [self openPhotoGalleryController];
                                 break;
                             case 2:
                                 [self showCroppControllerForCurrentPhoto];
                                 break;
                             case 3:
                                 //Удаление фотографии
                                 self.contractor.photo = nil;
                                 self.photoName = nil;
                                 [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
                                 self.photoImageView.image = [UIImage imageNamed:@"photoPlaceholder"];
                                 break;
                             default:
                                 break;
                         }
                     }];
    }
    else{
        DoActionSheet *vActionSheet = [[DoActionSheet alloc] init];
        [vActionSheet showC:nil
                     cancel:LSTRING(@"Cancel")
                    buttons:@[LSTRING(@"Take a picture"), LSTRING(@"Browse gallery")]
                     result:^(int nResult) {
                         NSLog(@"---------------> result : %d", nResult);
                         switch (nResult) {
                             case 0:
                                 [self openPhotoCameraController];
                                 break;
                             case 1:
                                 [self openPhotoGalleryController];
                                 break;
                             case 2:
                                 [self showCroppControllerForCurrentPhoto];
                                 break;
                             default:
                                 break;
                         }
                     }];
    }
}


- (void)showCroppControllerForCurrentPhoto
{
    NSString *photoPath = nil;
    if (self.contractor.photo) {
        photoPath = self.contractor.photo;
    }
    else{
        //photoPath = self.photoName;
    }
    NSString *originalpath = [photoPath stringByReplacingOccurrencesOfString:@"_thumb"
                                                                  withString:@"_original"];
    UIImage *image = [UIImage imageFromDocsByName:originalpath];
    
    [self openImageCropperWithImage:image];
}

- (void)openImageCropperWithImage:(UIImage *)image
{
    ImageCropController *imageCropController = [[ImageCropController alloc] init];
    imageCropController.image = image;
    imageCropController.delegate = self;
    MainNavigationController *mvc = [[MainNavigationController alloc] initWithRootViewController:imageCropController];
    [self presentViewController:mvc animated:YES completion:nil];
}


#pragma mark - Camera

- (void)openPhotoCameraController
{
    UIImagePickerController *photosCameraPickerController = [[UIImagePickerController alloc] init];
    photosCameraPickerController.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        photosCameraPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        photosCameraPickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self.navigationController presentViewController:photosCameraPickerController
                                            animated:YES
                                          completion:nil];
}


#pragma mark - Gallery

- (void)openPhotoGalleryController
{
    UIImagePickerController *photosCameraPickerController = [[UIImagePickerController alloc] init];
    photosCameraPickerController.delegate = self;
    photosCameraPickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    [self.navigationController presentViewController:photosCameraPickerController
                                            animated:YES
                                          completion:nil];
}

#pragma -----------------------------------------------------
#pragma mark - UIImagePickerControllerDelegate
#pragma -----------------------------------------------------

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    originalImage = [originalImage fixOrientation];
    [picker dismissViewControllerAnimated:YES completion:^{
        
        ImageCropController *imageCropController = [[ImageCropController alloc] init];
        imageCropController.image = originalImage;
        imageCropController.delegate = self;
        MainNavigationController *mvc = [[MainNavigationController alloc] initWithRootViewController:imageCropController];
        [self presentViewController:mvc animated:YES completion:nil];
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didCropAndSaveImageToDocumentsWithName:(NSString *)filename
{
 
    if ( self.contractor.photo) {
        // TODO: remove old images
    }
    self.photoName = filename;
    self.photoImageView.image = [UIImage imageFromDocsByName:filename];
    
}

@end
