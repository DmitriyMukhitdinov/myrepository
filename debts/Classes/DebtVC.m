//
//  DebtVC.m
//  debts
//
//  Created by Evgeny on 26.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DebtVC.h"
#import "Refund.h"
#import "Debenture.h"
#import "Amount.h"
#import "Contractor.h"
#import "Asset.h"
#import "DateChange.h"
#import  <QuartzCore/QuartzCore.h>
#import "AssetCellViewCell.h"
#import "ContractorCell.h"
#import "DebtToolsBar.h"
#import "DateChange.h"
#import "RMPhoneFormat.h"
#import "PushTracking.h"

#define kCalendarDocumentDateTag    1010
#define kCalendarDeadLineTag        1011

#define kTopSectionIndex            0
#define kRefundsSectionIndex        1
#define kInfoSectionIndex           2

#define kCalcVCSegue           @"CalcVCSegue"

static NSString *kCreatedAtCellID       = @"CreatedAtCell";
static NSString *kContractorCellID      = @"ContractorCell";
static NSString *kDebtTypeCellID        = @"DebtTypeCell";
static NSString *kDebtTypeAssetCellID   = @"DebtTypeAssetCell";
static NSString *kRefundCellID          = @"RefundDateCell";
static NSString *kReturnDateCellID      = @"ReturnDateCell";
static NSString *kStatusCellID          = @"StatusCell";
static NSString *kAssetCellID           = @"AssetCell";
static NSString *kInfoCellID            = @"InfoCell";

typedef enum RecordType{
    RecordTypeNew,
    RecordTypeExist
}RecordType;



#pragma mark -

@interface DebtVC (){
    NSMutableCharacterSet *_phoneChars;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableToBottom;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) DebtToolsBar  *toolBar;
@property (nonatomic, strong) DateChange    *dateChange;
@property (nonatomic, readonly) NSArray     *refunds;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, assign) RecordType recordType;
@property (nonatomic, strong) NSArray *historyArray;
@property (nonatomic, strong) RMPhoneFormat *phoneFormatter;
@end

@implementation DebtVC
@synthesize delegate;
@synthesize refunds = _refunds;

#pragma mark - Lifecycle
 
- (void)viewDidLoad
{
    [super viewDidLoad];
	self.phoneFormatter = [[RMPhoneFormat alloc] init];
    _phoneChars = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
    [_phoneChars addCharactersInString:@"+*#,"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"refundCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kRefundCellID];
    [self.tableView registerNib:[UINib nibWithNibName:@"AssetCellPhoto" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kAssetCellID];
    self.tableView.tableFooterView = [UIView new];
    
    if (self.debt == nil) {
        self.recordType = RecordTypeNew;
        _context = [NSManagedObjectContext MR_context];
        self.debt = [Debenture MR_createInContext:self.context];
        self.debt.created_at = [NSDate date];
        self.debt.deadline = [AppHelper combineDate:[NSDate date] withTime:[Preferences defaultTime]];
        self.debt.type       = DebtType_toString[self.type];
        self.title = @"Новый";
        
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Save") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        rightBarButton.enabled = NO;
        
        BarButton *leftButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [leftButton setTitle:LSTRING(@"Cancel") forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
        self.navigationItem.leftBarButtonItem = leftBarButton;
        leftBarButton.enabled = YES;
    }
    else{
        self.recordType = RecordTypeExist;
        _context = [NSManagedObjectContext MR_contextForCurrentThread];
        self.title = @"Редактирование";
        UIEdgeInsets inset = UIEdgeInsetsMake(0, 0, 45, 0);
        self.tableView.contentInset = inset;

        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"delete") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        [self addToolBar];
    }
    [self updateHistoryArray];
    
    if (self.isModal) {
        BarButton *leftButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [leftButton setTitle:LSTRING(@"Cancel") forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
        self.navigationItem.leftBarButtonItem = leftBarButton;
        leftBarButton.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)updateHistoryArray
{
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:[self.debt.refunds allObjects]];
    [array addObjectsFromArray:[self.debt.dateChanges allObjects]];
    [array sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES]]];
    self.historyArray = array;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWasShown:)
     name:UIKeyboardDidShowNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(64.0, 0.0, keyboardSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height-64.0f, 1, 1) animated:YES];
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Helpers
 
- (void)validate
{
    UIBarButtonItem *item = self.navigationItem.rightBarButtonItem;
    BOOL enable = YES;
    if (!self.debt.contractor) {
        enable = NO;
        item.enabled = NO;
        return;
    }
    switch (self.type) {
        case DebtTypeMoneyMine:
        case DebtTypeMoneySomeone:
            if(![self.debt total].amount ){
                enable = NO;
            }
            break;
        case DebtTypeAssetMine:
        case DebtTypeAssetSomeone:
            if (self.debt.asset == nil) {
                enable = NO;
            }
            break;
    }
    if (enable) {
        item.enabled = YES;
    }
    else{
        item.enabled = NO;
    }
    
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

 
#pragma mark - Setters/Getters


- (NSArray *)refunds
{
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    _refunds = [self.debt.refunds sortedArrayUsingDescriptors:@[sd]];
    return _refunds;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ContractorsVC"]) {
        ContractorsTVC *contractorTVC = segue.destinationViewController;
        contractorTVC.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"AssetsVC"]){
        AssetsTVC *assetTVC = segue.destinationViewController;
        assetTVC.delegate = self;
    }
    else if ([segue.identifier isEqualToString:kCalcVCSegue]){
        UINavigationController *destNavigationsVC = [segue destinationViewController];
        CalcVC *calcVC = [[destNavigationsVC viewControllers] firstObject];
        calcVC.amount = sender;
        Amount *amount = (Amount *)sender;
        if (amount.amount.floatValue == 0.0f) {
            calcVC.placeholderNumber = [NSNumber numberWithFloat:[[self.debt debtBalance] floatValue]];
        }
        else {
            calcVC.placeholderNumber = [NSNumber numberWithFloat:[amount.amount floatValue]];
        }
        
        calcVC.delegate = self;
    }
}


#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            if ([[self.debt finished]intValue]==1) return 4;
            return 5;
            break;
        case 1: //Возвраты
            [self updateHistoryArray];
            if (self.historyArray.count>0 && [self.debt finished]!=[NSNumber numberWithInt:1]) return self.historyArray.count+1;
            return self.historyArray.count;
            break;
        case 2: //Информация
        default:
            return 1;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (indexPath.section == kTopSectionIndex) {
        if (indexPath.row == 0) {
            //Строка статуса
            cell = [tableView dequeueReusableCellWithIdentifier:kStatusCellID];
            UILabel *statusLabel = (UILabel *)[cell viewWithTag:1010];
            switch ([self.debt statusForDebt]) {
                case DebtStatusFinished:
                    statusLabel.text = @"Документ оплачен";
                    cell.backgroundColor = [UIColor colorFromHexString:kMainColor];
                    break;
                case DebtStatusOutdated:
                    statusLabel.text = @"Документ просрочен";
                    cell.backgroundColor = [UIColor colorFromHexString:kOutdatedCellColor];
                    break;
                case DebtStatusActive:
                default:
                    break;
            }
            statusLabel.text = [statusLabel.text uppercaseString];
        }
        else if (indexPath.row == 1) {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kCreatedAtCellID];
            //UILabel *textLabel = (UILabel *)[cell viewWithTag:1010];
            UILabel *dateLabel = (UILabel *)[cell viewWithTag:1011];
            NSString *dateString = nil;
            if (self.debt.created_at)dateString = [[AppHelper dateFormatter]stringFromDate:self.debt.created_at];
            else dateString = @"не указана";
            dateLabel.text = dateString;
        }
        else if (indexPath.row == 2) {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kContractorCellID];
            Contractor *contractor = self.debt.contractor;
            if (contractor) {
                UIImageView *contractorImageView = (UIImageView *)[cell viewWithTag:1010];
                if (contractor.photo) {
                    contractorImageView.image = [UIImage imageFromDocsByName:contractor.photo];
                }
                else{
                    contractorImageView.image = [UIImage imageNamed:@"photoPlaceholder"];
                }
                UILabel *textLabel = (UILabel *)[cell viewWithTag:1011];
                textLabel.text = contractor.name;
            }
        }
        else if (indexPath.row == 3)
        {
            if ([self.debt isAssetDebt]) {
                Asset *asset = self.debt.asset;
                if (asset != nil) {
                    cell = [tableView dequeueReusableCellWithIdentifier:kAssetCellID];
                    AssetCellViewCell *assetCell = (AssetCellViewCell *)cell;
                    if (asset.photo)
                        assetCell.photoImageView.image = [UIImage imageFromDocsByName:asset.photo];
                    else
                        assetCell.photoImageView.image = [UIImage imageNamed:@"assetPlaceholder"];
                    assetCell.titleLabel.text = asset.title;
                    assetCell.contractorLabel.text = @"стоимостью:";
                    assetCell.dateLabel.text = [[AppHelper currencyFormatterWithCode:asset.amount.code] stringFromNumber:asset.amount.amount];
                    assetCell.dateLabel.textColor = [UIColor colorFromHexString:kMainColor];
                }
                else{
                    cell = [self.tableView dequeueReusableCellWithIdentifier:kDebtTypeAssetCellID];
                    UILabel *rightLabel = (UILabel *)[cell viewWithTag:1010];
                    UILabel *leftLabel = (UILabel *)[cell viewWithTag:1011];
                    rightLabel.text = [TextHelper debtStatusCellForDebtType:self.type withGender:self.debt.contractor.sex];
                    leftLabel.text = @"";
                }
            }
            else{
                cell = [self.tableView dequeueReusableCellWithIdentifier:kDebtTypeCellID];
                UILabel *rightLabel = (UILabel *)[cell viewWithTag:1010];
                UILabel *leftLabel = (UILabel *)[cell viewWithTag:1011];
                rightLabel.text = [TextHelper debtStatusCellForDebtType:self.type withGender:self.debt.contractor.sex];
                NSNumber *amount = (self.debt.money) ? self.debt.money : [NSNumber numberWithInt:0];
                NSString *currencyCode = (self.debt.total.amount) ? self.debt.total.code : [Preferences defaultCurrencyCode];
                NSString *amountString = [[AppHelper currencyFormatterWithCode:currencyCode] stringFromNumber:amount];
                leftLabel.text = amountString;
            }
        }
        else {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kReturnDateCellID];
            UILabel *rightLabel = (UILabel *)[cell viewWithTag:1011];
            UILabel *leftLabel = (UILabel *)[cell viewWithTag:1010];
            if ([self.debt isAssetDebt]) {
                leftLabel.text = @"Дата возврата:";
            }
            NSString *dateString = nil;
            if (self.debt.deadline)dateString = [[AppHelper dateFormatter]stringFromDate:self.debt.deadline];
            else dateString = @"не указана";
            rightLabel.text = dateString;
        }
    }
    else if (indexPath.section == kRefundsSectionIndex){
        cell = [tableView dequeueReusableCellWithIdentifier:kRefundCellID];
        UILabel *label1 = (UILabel *)[cell viewWithTag:1010];
        UILabel *label2 = (UILabel *)[cell viewWithTag:1011];
        UIButton *button = (UIButton *)[cell viewWithTag:1099];
        if (indexPath.row<self.historyArray.count) {
            id object = self.historyArray[indexPath.row];
            if ([object isKindOfClass:[Refund class]]) {
                
                [button addTarget:self action:@selector(refundRowButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                
                Refund *r = object;
                label1.text = [[AppHelper dateFormatter] stringFromDate:r.created_at];
                if ([self.debt isAssetDebt]) {
                    label2.text = @"имущество возвращено";
                }
                else{
                    if ([self.debt debtType] == DebtTypeMoneyMine) {
                        if ([self.debt.contractor isWoman]) {
                            label2.text = [NSString stringWithFormat:@"возвратила мне %@",[[AppHelper currencyFormatterWithCode:self.debt.total.code] stringFromNumber:r.amount.amount]];
                        }
                        else label2.text = [NSString stringWithFormat:@"возвратил мне %@",[[AppHelper currencyFormatterWithCode:self.debt.total.code] stringFromNumber:r.amount.amount]];
                    }
                    else{
                        if ([[AppHelper contractorMe] isWoman]) {
                            label2.text = [NSString stringWithFormat:@"я возвратила %@",[[AppHelper currencyFormatterWithCode:self.debt.total.code] stringFromNumber:r.amount.amount]];
                        }
                        else label2.text = [NSString stringWithFormat:@"я возвратил %@",[[AppHelper currencyFormatterWithCode:self.debt.total.code] stringFromNumber:r.amount.amount]];
                    }
                }
            }
            else{
                DateChange *dateChange = object;
                
                [button removeTarget:self
                              action:@selector(refundRowButtonTapped:)
                    forControlEvents:UIControlEventTouchUpInside];
                
                if (dateChange.toDate) {
                    label1.text = [[AppHelper dateFormatter] stringFromDate:dateChange.created_at];
                    if ([self.debt.contractor isWoman]) {
                        label2.text = [NSString stringWithFormat:LSTRING(@"she change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                    }
                    else label2.text = [NSString stringWithFormat:LSTRING(@"change my return date %@"),[[AppHelper dateFormatter] stringFromDate:dateChange.toDate]];
                }
                else{
                    label1.text = [[AppHelper dateFormatter] stringFromDate:dateChange.created_at];
                    if ([self.debt.contractor isWoman]) {
                        label2.text = LSTRING(@"she Cancel return date");
                    }
                    else label2.text = LSTRING(@"Cancel return date");
                }
            }
            
        }
        else{
            [button removeTarget:self
                          action:@selector(refundRowButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
            
            label1.text = @"Остаток долга:";
            label2.text = [[AppHelper currencyFormatterWithCode:self.debt.total.code] stringFromNumber:[self.debt debtBalance]];
        }
    }
    else { //Information
        cell = [self.tableView dequeueReusableCellWithIdentifier:kInfoCellID];
        UIPlaceHolderTextView *textView = (UIPlaceHolderTextView *)[cell viewWithTag:1010];
        if (self.debt.info) {
            textView.text = self.debt.info;
        }
        else{
            textView.placeholder = @"Введите описание";
        }
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark -
#pragma mark UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (indexPath.section == kTopSectionIndex) {
        if (indexPath.row == 0) {
            //Строка статуса
            if (([self.debt statusForDebt]==DebtStatusActive || self.recordType == RecordTypeNew) &&
                [self.debt.finished intValue]!=1) {
                return 0;
            }
            return 21.0f;
        }
        else if (indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:kCreatedAtCellID];
        }
        else if (indexPath.row == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:kContractorCellID];
        }
        else if (indexPath.row == 3) {
            if ([self.debt isAssetDebt]) {
                if (self.debt.asset != nil) {
                    cell = [tableView dequeueReusableCellWithIdentifier:kAssetCellID];
                }
                else{
                    cell = [tableView dequeueReusableCellWithIdentifier:kDebtTypeCellID];
                }
            }
            else{
                cell = [tableView dequeueReusableCellWithIdentifier:kDebtTypeCellID];
            }
        }
        else{
            cell = [tableView dequeueReusableCellWithIdentifier:kReturnDateCellID];
        }
    }
    else if (indexPath.section == kRefundsSectionIndex){
        cell = [tableView dequeueReusableCellWithIdentifier:kRefundCellID];
    }
    else { //Information
        cell = [tableView dequeueReusableCellWithIdentifier:kInfoCellID];
    }
    if (cell)return cell.frame.size.height;
    else return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==kRefundsSectionIndex && self.debt.refunds.count==0 && self.debt.dateChanges.count==0) return 0;
    else if (section == kTopSectionIndex) return 0;
    return 20.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    
    if (indexPath.section == kTopSectionIndex) {
        if (indexPath.row == 1) {
            //Created at calendar
            [self openCalendarForDate:self.debt.created_at andObject:self.debt andDeadline:NO];
        }
        else if (indexPath.row == 2) {
            //Select contractor
            [self performSegueWithIdentifier:@"ContractorsVC" sender:nil];
        }
        else if (indexPath.row == 3) {
            if ([self.debt isAssetDebt]) {
                //Asset select
                [self performSegueWithIdentifier:@"AssetsVC" sender:nil];
            }
            else{
               //Calc
                if (self.recordType == RecordTypeNew && ![self.debt isAssetDebt] && self.debt.total == nil) {
                    self.debt.total = [Amount MR_createInContext:self.context];
                }
                [self performSegueWithIdentifier:kCalcVCSegue sender:self.debt.total];
            }
        }
        else{
            //Refund date calendar
            [self openCalendarForDate:self.debt.deadline andObject:self.debt andDeadline:YES];
        }
    }
    else if (indexPath.section == kRefundsSectionIndex){
        if (indexPath.row<self.historyArray.count && ![self.debt isAssetDebt]) {
            id object = self.historyArray[indexPath.row];
            if ([object isKindOfClass:[Refund class]]) {
                Refund *r = self.historyArray[indexPath.row];
                [self performSegueWithIdentifier:kCalcVCSegue sender:r.amount];
            }
            else{
                //Date change
                DateChange *dateChange = object;
                [self openCalendarForDate:dateChange.toDate andObject:dateChange andDeadline:NO];
            }
        }
    }
    else { //Information
        //Do nothing
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == kRefundsSectionIndex) {
        if (self.debt.refunds.count>0 || self.debt.dateChanges.count>0) {
            TableSection *tableSection = [[TableSection alloc] init];
            tableSection.titleLabel = @"история";
            return tableSection;
        }
    }
    else if (section==2){
        TableSection *tableSection = [[TableSection alloc] init];
        tableSection.titleLabel = @"информация";
        return tableSection;
    }
    return [UIView new];
}

#pragma mark -
#pragma mark Asset select

- (void)didSelectAsset:(Asset *)asset
{
    self.debt.asset = [asset MR_inContext:self.context];
    [self.tableView reloadData];
    [self validate];
}

#pragma mark Contractor select
- (void)didSelectContractor:(Contractor *)contractor
{
    [self.debt setContractor:[contractor MR_inContext:self.context]];
    [self.tableView reloadData];
    [self validate];
}

#pragma mark -
#pragma mark Calc Delegate Methods


- (void)calcDidPressEnterWithAmount:(Amount *)amount
{
    [self dismissKeyboard];
    if (self.recordType == RecordTypeNew) {
        self.debt.money = self.debt.total.amount;
        [self validate];
    }
    else{
        if ([amount isEqual:self.debt.total]) {
            //Изменение суммы долга
            //NSNumber *balance = [NSNumber numberWithFloat:([self.debt.money floatValue]-[self.debt.total.amount floatValue])];
            self.debt.money = self.debt.total.amount;
            if ([[self.debt debtBalance]floatValue]>0) {
                self.debt.finished = [NSNumber numberWithInt:0];
                [PushTracking cancelPushWithWserInfo:@{@"debtID": self.debt.debtID}];
                [self createPushNotificationForDebt:self.debt];
            }
            //self.debt.total.amount = [NSDecimalNumber decimalNumberWithDecimal:[balance decimalValue]];
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [self updateToolBarButtons];
        }
        else if(amount.refund){
            //Возвраты
            NSComparisonResult compareResult = [[self.debt debtBalance] compare:[NSDecimalNumber decimalNumberWithDecimal:[amount.amount decimalValue]]];
            if ((compareResult == NSOrderedSame || compareResult == NSOrderedDescending) &&
                [amount.amount intValue]!=0)
            {
                if (![self.debt.refunds containsObject:amount.refund]) {
                    [self.debt addRefundsObject:amount.refund];
                }
                NSDecimalNumber *debtBalance = [self.debt debtBalance];
                if (![Preferences isDecimalEnabled] && [debtBalance floatValue]<1.0f) {
                    NSDecimal zero = [NSDecimalNumber zero].decimalValue;
                    self.debt.total.amount = [NSDecimalNumber decimalNumberWithDecimal:zero];
                }
                if ([debtBalance compare:[NSNumber numberWithInt:0]] == NSOrderedSame) {
                    self.debt.finished = [NSNumber numberWithInt:1];
                    [PushTracking cancelPushWithWserInfo:@{@"debtID": self.debt.debtID}];
                }
                else{
                    //[self changePushNotificationForDebt:self.debt];
                }
            }
            else{
                [amount.refund MR_deleteEntity];
                [amount MR_deleteEntity];
            }
            [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
            [self updateToolBarButtons];
            [self updateHistoryArray];
        }
        else{
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"something go wrong"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
        }
    }
    [self.tableView reloadData];
}



#pragma mark -
#pragma mark Calendar View Controller Delegate Methods

- (void)calendar:(CalendarViewController *)calendar didSelectDate:(NSDate *)date forDeadline:(BOOL)isDeadline
{
    if (isDeadline)
    {
        if(self.recordType == RecordTypeNew){
            self.debt.deadline = [AppHelper combineDate:date withTime:[Preferences defaultTime]];
        }
        else{
            if (self.dateChange) {
                [self.debt removeDateChangesObject:self.dateChange];
                [self.dateChange MR_deleteEntity];
                self.dateChange = nil;
            }
            self.debt.deadline = date;
            //Изменение даты
            self.dateChange = [DateChange MR_createEntity];
            self.dateChange.created_at = [NSDate date];
            self.dateChange.toDate = date;
            [self.debt addDateChangesObject:self.dateChange];
            //Обновление пуша
            [self changePushNotificationForDebt:self.debt];
            
            NSDate *date = self.debt.deadline;
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
            [components setHour:23];
            [components setMinute:59];
            NSDate *newDate = [calendar dateFromComponents:components];
            self.debt.deadline = newDate;
        }
    }
    else {
        if ([calendar.object isKindOfClass:[Refund class]]) {
            ((Refund *)calendar.object).created_at = date;
        }
        else if ([calendar.object isKindOfClass:[Debenture class]]){
            self.debt.created_at = date;
        }
        else if ([calendar.object isKindOfClass:[DateChange class]]){
            ((DateChange *)calendar.object).toDate = date;
        }
    }
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfAndWait];
    [self.tableView reloadData];
}

- (void)calendar:(CalendarViewController *)calendar didCancelDateSelectForDeadline:(BOOL)isDeadline
{
    if (isDeadline) {
        if (self.recordType == RecordTypeNew) {
            self.debt.deadline = nil;
        }
        else{
            //Отмена даты возрата
            if (self.debt.deadline) {
                if (self.dateChange) {
                    [self.debt removeDateChangesObject:self.dateChange];
                    [self.dateChange MR_deleteEntity];
                    self.dateChange = nil;
                }
                self.debt.deadline = nil;
                //Изменение даты
                self.dateChange = [DateChange MR_createEntity];
                self.dateChange.created_at = [NSDate date];
                self.dateChange.toDate = nil;
                [self.debt addDateChangesObject:self.dateChange];
                //Обновление пуша
                [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfAndWait];
                [self cancelPushNotificationForDebt:self.debt];
            }
        }
    }
    else self.debt.created_at = nil;
        
   
    [self.tableView reloadData];
}


#pragma mark -
#pragma mark UITextView Delegate
 
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    UITableViewCell *aCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    CGSize cellSize = aCell.frame.size;
    [self.tableView setContentOffset:CGPointMake(0, cellSize.height) animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.debt.info = textView.text;
    [self.tableView setContentOffset:CGPointMake(0, 0)];
}

#pragma mark -
#pragma mark Actions
- (void)refundRowButtonTapped:(id)sender
{
    UIView *view = (UIView *)sender;
    UITableViewCell *cell = (UITableViewCell *)view.superview.superview.superview;
    NSIndexPath *ip = [self.tableView indexPathForCell:cell];
    long index = ip.row;
    Refund *r = self.historyArray[index];
    [self openCalendarForDate:r.created_at andObject:r andDeadline:NO];
}
 
- (void)openCalendarForDate:(NSDate *)date andObject:(id)object andDeadline:(BOOL)deadline
{
    CalendarViewController *calendar = [[CalendarViewController alloc] init];
    calendar.delegate = self;
    calendar.currentDate = date;
    calendar.deadlineDate = deadline;
    calendar.object = object;
    [self.navigationController pushViewController:calendar animated:YES];
}

- (void)save:(id)sender
{
    self.debt.debtID = [AppHelper makeUniqueString];
    [self goBack];
    [self createPushNotificationForDebt:self.debt];
    if (self.debt.deadline) {
        NSDate *date = self.debt.deadline;
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
        [components setHour:23];
        [components setMinute:59];
        NSDate *newDate = [calendar dateFromComponents:components];
        self.debt.deadline = newDate;
    }
    [self.context MR_saveToPersistentStoreAndWait];
    [self.delegate updateTable];
}

- (void) delete:(id)sender
{
    //Alert
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                 message:LSTRING(@"alert remove debt")
                                                delegate:self
                                       cancelButtonTitle:LSTRING(@"Cancel")
                                       otherButtonTitles:LSTRING(@"Remove"), nil];
    av.delegate = self;
    [av show];
}

- (void)deleteDebt
{
    if(self.debt.debtID)[PushTracking cancelPushWithWserInfo:@{@"debtID": self.debt.debtID}];
    [self.debt MR_deleteEntity];
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
    [self.delegate updateTable];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark ToolBar
 
- (void)addToolBar
{
    if (!self.toolBar) {
        self.tableToBottom.constant = 49;
        [self.tableView updateConstraints];
        
        self.toolBar = [[[NSBundle mainBundle] loadNibNamed:@"tools"
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.toolBar.delegate = self;
        //self.toolBar.debt = self.debt;
        self.toolBar.frame = CGRectMake(0, self.view.frame.size.height-49.0f, self.view.frame.size.width, 49.0f);
        [self.view addSubview:self.toolBar];
        [self updateToolBarButtons];
        if ([self.debt isAssetDebt]) {
            self.toolBar.payItem.title = @"Возврат";
        }
    }
}

- (void)updateToolBarButtons
{
    if ([self.debt.finished isEqualToNumber:[NSNumber numberWithInt:1]]) {
        self.toolBar.payItem.enabled = NO;
        self.toolBar.redateItem.enabled = NO;
    }
    else{
        self.toolBar.payItem.enabled = YES;
        self.toolBar.redateItem.enabled = YES;
    }
    if (self.debt.contractor.phone.length>0) {
        self.toolBar.smsItem.enabled = YES;
        self.toolBar.callItem.enabled = YES;
    }
    else{
        self.toolBar.smsItem.enabled = NO;
        self.toolBar.callItem.enabled = NO;
    }
}

- (void)didPayAmount:(NSNumber *)amount
{
    if (self.type == DebtTypeAssetSomeone || self.type == DebtTypeAssetMine) {
        [PushTracking cancelPushWithWserInfo:@{@"debtID": self.debt.debtID}];
        Refund *refund = [Refund MR_createEntity];
        refund.created_at = [NSDate date];
        refund.type = @"asset";
        [self.debt addRefundsObject:refund];
        self.debt.finished = [NSNumber numberWithInt:1];
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
        [self.tableView reloadData];
        [self updateToolBarButtons];
    }
    else{
        Refund *refund = [Refund MR_createEntity];
        refund.amount = [Amount MR_createEntity];
        refund.amount.code = self.debt.total.code;
        refund.created_at = [NSDate date];
        [self performSegueWithIdentifier:kCalcVCSegue sender:refund.amount];
    }
}

#pragma mark -
#pragma mark Push Notification
 
- (void)createPushNotificationForDebt:(Debenture *)debt
{
    if (debt.deadline && self.debt.debtID) {
        [PushTracking createPushWithText:[TextHelper titleForNotoficationForDebtType:self.type debt:debt]
                                fireDate:[AppHelper combineDate:debt.deadline withTime:[Preferences defaultTime]]
                                userInfo:@{@"debtID": self.debt.debtID}];
    }
}

- (void)changePushNotificationForDebt:(Debenture *)debt
{
    [PushTracking changeDateForPushWithUserInfo:@{@"debtID": debt.debtID} toFireDate:Nil];
}

- (void)cancelPushNotificationForDebt:(Debenture *)debt //When finish debt
{
    [PushTracking cancelPushWithWserInfo:@{@"debtID": self.debt.debtID}];
}



#pragma mark -
#pragma mark UITabBar Delegate
 
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSUInteger index = [tabBar.items indexOfObject:item];
    switch (index) {
        case 0:
            if (self.debt.contractor.phone.length>0) {
                [[UIApplication sharedApplication] openURL:[self phoneUrl:self.debt.contractor.phone]];
            }
            break;
        case 1:
            if (self.debt.contractor.phone.length>2) {
                [[UIApplication sharedApplication] openURL:[self smsUrl:self.debt.contractor.phone]];
            }
            break;
        case 2:
            [self openCalendarForDate:self.debt.deadline andObject:self.debt andDeadline:YES];
            break;
        case 3:
        default:
            [self didPayAmount:nil];
            break;
    }
    [tabBar setSelectedItem:nil];
}

- (NSString *)phoneNumber:(NSString *)number
{
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"+" withString:@""];
    return number;
}

- (NSURL *)phoneUrl:(NSString *)phoneNumber{
    NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    return phoneURL;
}

- (NSURL *)smsUrl:(NSString *)phoneNumber{
    NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"sms:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    return phoneURL;
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            //Cancel
            break;
            
        default:
            //Remove
            [self deleteDebt];
            break;
    }
}

@end
