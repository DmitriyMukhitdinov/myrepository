//
//  DebtsTableViewController.m
//  debts
//
//  Created by Evgeny on 12.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "DebtsTableViewController.h"
#import "PreferencesTVC.h"
#import "DebtsHeader.h"
#import "Amount.h"
#import "Debenture.h"
#import "Contractor.h"
#import "DebtCell.h"
#import "DebtCellPhoto.h"
#import "AssetCellViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Rate.h"


typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


static NSString *kAddDebtCellID      = @"addDebtCell";
static NSString *kAssetDebtCellID    = @"AssetCell";
static NSString *kMoneyDebtCellID    = @"moneyCell";

#pragma mark -

@interface DebtsTableViewController ()
@property (nonatomic, strong) NSArray   *tableData;
@property (nonatomic, strong) NSTimer   *timer;
@property (nonatomic, strong) NSNumber  *debtsSumm;
@property (nonatomic, assign) BOOL      isUpdateFailed;
@end

@implementation DebtsTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorFromHexString:kGrayColor];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    //self.tableView.contentInset = UIEdgeInsetsMake(-21.5f, 0, 0, 0);
    
    // Remove left offcet on cells Only iOS7
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UINib *customCellNib = [UINib nibWithNibName:@"addRow" bundle:nil];
    [self.tableView registerNib:customCellNib forCellReuseIdentifier:kAddDebtCellID];
    
    UINib *customCellNib1 = [UINib nibWithNibName:@"AssetCellPhoto" bundle:nil];
    [self.tableView registerNib:customCellNib1 forCellReuseIdentifier:kAssetDebtCellID];
    
    UINib *customCellNib2 = [UINib nibWithNibName:@"debtCellPhoto" bundle:nil];
    [self.tableView registerNib:customCellNib2 forCellReuseIdentifier:kMoneyDebtCellID];
    
    [self updateTable];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    //Top view setup
    UIView *preferencesView = [[UIView alloc] initWithFrame: CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
    preferencesView.backgroundColor = [UIColor whiteColor];
    
    UIView *preferences = [[[NSBundle mainBundle] loadNibNamed:@"preferencesView"
                                                         owner:self
                                                       options:nil] lastObject];
    CGRect frame = preferences.frame;
    frame.origin.y = self.tableView.bounds.size.height-frame.size.height;
    preferences.frame = frame;
    preferences.center = CGPointMake(preferencesView.center.x, preferences.center.y);
    [preferencesView addSubview:preferences];
    [self.tableView addSubview:preferencesView];
    
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ratesUpdated:)
                                                 name:@"ratesUpdated"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ratesStartUpdate:)
                                                 name:@"ratesStartUpdate"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ratesUpdateFailed)
                                                 name:@"ratesUpdateFailed"
                                               object:nil];
}

- (void)ratesUpdateFailed
{
    _isUpdateFailed = YES;
    self.debtsSumm = [self summInDefaultCurrencyIgnoringNil];
    [self.tableView reloadData];
}

- (void)ratesUpdated:(id)obj
{
    //remove loader, show money
    self.debtsSumm = [self summInDefaultCurrencyIgnoringNil];
    self.isUpdateFailed = NO;
    [self.tableView reloadData];
}

- (void)ratesStartUpdate:(id)obj
{
    //Show loader
    //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
    //              withRowAnimation:UITableViewRowAnimationNone];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigations
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DebtVC"]) {
        DebtVC *debtVC = [segue destinationViewController];
        NSInteger inx = [self.tableView indexPathForSelectedRow].row;
        if (inx < self.tableData.count) {
            debtVC.debt = self.tableData[inx];
        }
        debtVC.type = self.type;
        debtVC.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"PreferencesVC"]){
//        UIViewController *viewController = [segue destinationViewController];
//        viewController.modalPresentationStyle=UIModalPresentationFormSheet;
    }
}


#pragma mark - Setters

- (void)setType:(DebtType)type
{
    _type=type;
    switch (type) {
        case DebtTypeMoneyMine:
            self.title = NSLocalizedString(@"own to me", @"");
            break;
        case DebtTypeMoneySomeone:
            if ([[AppHelper contractorMe] isWoman]) {
                 self.title = @"я должна";
            }
            else self.title = NSLocalizedString(@"i own", @"");
            break;
        case DebtTypeAssetMine:
            self.title = NSLocalizedString(@"pick up", @"");
            break;
        default:
            self.title = NSLocalizedString(@"give", @"");
            break;
    }
}

#pragma mark - Badges

- (void)updateBadgeValues
{
    NSUInteger count = 0;
    if (self.tableData == nil) {
        self.tableData = [Debenture MR_findAllSortedBy:@"created_at" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"type = %@",DebtType_toString[self.type]] ];
    }
    count = [self.tableData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"finished !=true AND deadline<%@", [NSDate date]]].count;
    if (count>0) {
        self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)count];
    }
    else{
        self.tabBarItem.badgeValue = nil;
    }
}


#pragma mark - Table view data source

- (void)updateTable
{
    NSPredicate *predicate = nil;
    if (![Preferences isShowFinishedEnabled]) {
        predicate =[NSPredicate predicateWithFormat:@"type = %@ AND finished != 1",DebtType_toString[self.type]];
    }
    else{
        predicate = [NSPredicate predicateWithFormat:@"type = %@",DebtType_toString[self.type]];
    }
    self.tableData = [Debenture MR_findAllSortedBy:@"created_at" ascending:NO withPredicate:predicate];
    [self updateBadgeValues];
    self.debtsSumm = [self summInDefaultCurrencyIgnoringNil];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.tableData.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.tableData.count)
    {
        //Строка "Добавить"
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAddDebtCellID];
        UILabel *label = (UILabel *)[cell viewWithTag:1010];
        label.text = @"Добавить новый долг";
        return cell;
    }
    else{
        Debenture *debt = self.tableData[indexPath.row];
        //Имущество
        if (self.type == DebtTypeAssetMine || self.type == DebtTypeAssetSomeone)
        {
            AssetCellViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAssetDebtCellID];
            Asset *asset = debt.asset;
            cell = [tableView dequeueReusableCellWithIdentifier:kAssetDebtCellID];

            if (asset.photo) {
                dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(concurrentQueue, ^{
                    UIImage *image = [UIImage imageFromDocsByName:asset.photo];
                    if (image != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [cell.photoImageView setImage:image];
                        });
                    }
                });
            }
            else{
                cell.photoImageView.image = [UIImage imageNamed:@"assetPlaceholder"];
            }
            
            if (self.isEditing) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            cell.titleLabel.text = asset.title;
            cell.backgroundColor = [UIColor whiteColor];
            
            if (asset.history.count>0) {
                
                if ([debt.finished isEqual:[NSNumber numberWithInt:1]]) {
                    //Находится у владельца
                    cell.dateLabel.text = @"";
                    cell.contractorLabel.text = @"возвращено";
                    cell.backgroundColor = [UIColor whiteColor];
                    /*
                    if (asset.owner == [AppHelper contractorMe])
                        cell.contractorLabel.text = @"у меня";
                    else{
                        NSString *name = [NSString stringWithFormat:@"%@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@"у ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                        cell.contractorLabel.text = name;
                    }
                    */
                }
                else{
                    //Вещь находится у контрагента
                    if (debt.deadline && [debt.deadline timeIntervalSince1970]<[[NSDate date] timeIntervalSince1970] && debt.finished!=[NSNumber numberWithInt:1]) {
                        //прострочено
                        cell.backgroundColor = [UIColor colorFromHexString:kOutdatedCellColor];
                        cell.dateLabel.highlighted = YES;
                        cell.titleLabel.highlighted = YES;
                        cell.contractorLabel.highlighted = YES;
                    }
                    else{
                        cell.backgroundColor = [UIColor whiteColor];
                        cell.dateLabel.highlighted = NO;
                        cell.titleLabel.highlighted = NO;
                        cell.contractorLabel.highlighted = NO;
                    }
                    
                    if (debt.asset.owner == [AppHelper contractorMe]) {
                        NSString *name = [NSString stringWithFormat:@"у %@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                        cell.contractorLabel.text = name;
                    }
                    else{
                        NSString *name = [NSString stringWithFormat:@"владелец %@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                        cell.contractorLabel.text = name;
                    }
                    cell.dateLabel.text = [[AppHelper dateFormatter] stringFromDate:debt.deadline];
                }
            }
            else{
                //Находится у владельца
                cell.dateLabel.text = @"";
                if (asset.owner == [AppHelper contractorMe])
                    cell.contractorLabel.text = @"у меня";
                else{
                    NSString *name = [NSString stringWithFormat:@"%@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                    cell.contractorLabel.text = name;
                }
            }
            return cell;
        }
        else{
            DebtCellPhoto *cell = [tableView dequeueReusableCellWithIdentifier:kMoneyDebtCellID];
            if (debt.contractor.photo) {
                cell.photoImageView.image = [UIImage imageFromDocsByName:debt.contractor.photo];
            }
            else{
                cell.photoImageView.image = [UIImage imageNamed:@"photoPlaceholder"];
            }
            NSString *name = [NSString stringWithFormat:@"%@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
            cell.nameLabel.text = name;
            if (debt.deadline) {
                cell.dateLabel.text =[NSString stringWithFormat:@"возврат %@", [[AppHelper dateFormatter]stringFromDate:debt.deadline]];
            }
            else{
                cell.dateLabel.text = LSTRING(@"Without refund date");
            }
            cell.amountLabel.text = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:[debt debtBalance]];
            if (debt.deadline && [debt.deadline timeIntervalSince1970]<[[NSDate date] timeIntervalSince1970]
                && debt.finished!=[NSNumber numberWithInt:1]) {
                //прострочено
                cell.backgroundColor = [UIColor colorFromHexString:kOutdatedCellColor];
                cell.dateLabel.highlighted = YES;
                cell.amountLabel.highlighted = YES;
                cell.nameLabel.highlighted = YES;
            }
            else if ([debt.finished isEqual:[NSNumber numberWithInt:1]]){
                cell.dateLabel.text = @"возвращено";
                cell.amountLabel.text = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:debt.money];
            }
            else{
                cell.backgroundColor = [UIColor whiteColor];
                cell.dateLabel.highlighted = NO;
                cell.amountLabel.highlighted = NO;
                cell.nameLabel.highlighted = NO;
            }
            return cell;
        }
    }
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"DebtVC" sender:nil];    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,1.5555* NSEC_PER_SEC), dispatch_get_main_queue(),^{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    });
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    DebtsHeader *tableHeader = [[[NSBundle mainBundle] loadNibNamed:@"debtsHeader"
                                                              owner:self
                                                            options:nil] lastObject];
    /*
    if (self.type == DebtTypeMoneyMine) {
        tableHeader.infoButton.hidden = NO;
    }
    else*/
    tableHeader.infoButton.hidden = YES;
    if (self.type == DebtTypeMoneyMine || self.type == DebtTypeMoneySomeone){
        //Count sum
        tableHeader.typeLabel.text = self.title;
        if (!self.debtsSumm) {
            if (self.isUpdateFailed) {
                tableHeader.intAmountLabel.hidden = NO;
                [tableHeader.activityIndicator stopAnimating];
                tableHeader.activityIndicator.hidden = YES;
                NSString *errorText = [[AppHelper currencyFormatterWithCode:nil] stringFromNumber:[self summForDebtsInDefaultCurrency]];
                tableHeader.intAmountLabel.text = [NSString stringWithFormat:@"%@*",errorText];
            }
            else{
                tableHeader.intAmountLabel.hidden = YES;
                tableHeader.activityIndicator.hidden = NO;
                [tableHeader.activityIndicator startAnimating];
            }
        }
        else{
            tableHeader.intAmountLabel.hidden = NO;
            [tableHeader.activityIndicator stopAnimating];
            tableHeader.activityIndicator.hidden = YES;
            tableHeader.intAmountLabel.text = [[AppHelper currencyFormatterWithCode:nil] stringFromNumber:self.debtsSumm];
        }
    }
    else{
        //Just text
        tableHeader.typeLabel.text = @"";
        tableHeader.intAmountLabel.text = self.title;
    }
    return tableHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.type == DebtTypeMoneyMine || self.type == DebtTypeMoneySomeone) {
        return 95.0f;
    }
    return 60.0f;
}

 
#pragma mark - Views

- (NSNumber *)summInDefaultCurrency
{
    if (self.type == DebtTypeMoneyMine || self.type == DebtTypeMoneySomeone) {
        float total = 0;
        for (Debenture *debt in [self.tableData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"finished != 1"]]) {
           //Если валюта долга = дефолтной валюте
           if ([debt.total.code isEqualToString:[Preferences defaultCurrencyCode]]) {
               total = total + [[debt debtBalance] floatValue];
           }
           else{
               //Other currency
               Rate *rateManager = [Rate sharedInstance];
               NSNumber *rateForCurrency = [rateManager rateForCurrencyCode:debt.total.code];
               if (rateForCurrency) {
                   total += [[debt debtBalance] floatValue]*[rateForCurrency floatValue];
               }
               else {
                   //Валюта не найдена
                   return nil;
               }
            }
        }
        return [NSNumber numberWithFloat:total];
    }
    return nil;
}

- (NSNumber *)summInDefaultCurrencyIgnoringNil
{
    if (self.type == DebtTypeMoneyMine || self.type == DebtTypeMoneySomeone) {
        float total = 0;
        for (Debenture *debt in [self.tableData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"finished != 1"]]) {
            //Если валюта долга = дефолтной валюте
            if ([debt.total.code isEqualToString:[Preferences defaultCurrencyCode]]) {
                total = total + [[debt debtBalance] floatValue];
            }
            else{
                //Other currency
                Rate *rateManager = [Rate sharedInstance];
                NSNumber *rateForCurrency = [rateManager rateForCurrencyCode:debt.total.code];
                if (rateForCurrency) {
                    total += [[debt debtBalance] floatValue]*[rateForCurrency floatValue];
                }
                else {
                    //Валюта не найдена
                }
            }
        }
        return [NSNumber numberWithFloat:total];
    }
    return nil;
}

- (NSNumber *)summForDebtsInDefaultCurrency
{
    if (self.type == DebtTypeMoneyMine || self.type == DebtTypeMoneySomeone) {
        float total = 0;
        for (Debenture *debt in [self.tableData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"finished != 1"]]) {
            //Если валюта долга = дефолтной валюте
            if ([debt.total.code isEqualToString:[Preferences defaultCurrencyCode]]) {
                total = total + [debt.total.amount floatValue];
            }
        }
        return [NSNumber numberWithFloat:total];
    }
    return nil;
}

 
#pragma mark - Table Scrolling
float previousScrollValue;
ScrollDirection currentDirection = ScrollDirectionCrazy;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollDirection currentDirection;
    if (scrollView.contentOffset.y<0) {
        //NSLog(@"Таблица смещена вниз");
        if (previousScrollValue > scrollView.contentOffset.y){
            if (currentDirection!=ScrollDirectionDown) {
                currentDirection = ScrollDirectionDown;
                //NSLog(@"startTimer");
                
                if (![self.timer isValid])[self startTimer];
            }
        }
    }
    else{
        //NSLog(@"Таблица смещена вверх");
        [self cancelTimer];
        currentDirection = ScrollDirectionCrazy;
    }
    previousScrollValue = scrollView.contentOffset.y;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
    if(translation.y > 0 && self.tableView.contentOffset.y < 25)
    {
        [self startTimer];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //end timer
    [self cancelTimer];
}

 
#pragma mark - Timer
 
- (void)startTimer
{
    [self cancelTimer];
    self.timer = [NSTimer timerWithTimeInterval:0.74f target:self selector:@selector(openConfigController) userInfo:Nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];

}

- (void)cancelTimer
{
    if ([self.timer isValid]) {
        [self.timer invalidate];
    }
}

#pragma mark - Preferences

- (void)openConfigController
{
    [self performSegueWithIdentifier:@"PreferencesVC" sender:nil];
}

@end
