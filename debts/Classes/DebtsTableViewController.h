//
//  DebtsTableViewController.h
//  debts
//
//  Created by Evgeny on 12.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DebtVC.h"

@interface DebtsTableViewController : UIViewController
<DebtVCDelegate,
UITableViewDataSource,
UITableViewDelegate>

@property (nonatomic, assign) DebtType type;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)updateBadgeValues;

@end
