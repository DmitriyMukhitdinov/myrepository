//
//  DebtToolsBar.h
//  debts
//
//  Created by Evgeny on 26.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebtToolsBar : UITabBar
@property (weak, nonatomic) IBOutlet UITabBarItem *callItem;
@property (weak, nonatomic) IBOutlet UITabBarItem *smsItem;
@property (weak, nonatomic) IBOutlet UITabBarItem *redateItem;
@property (weak, nonatomic) IBOutlet UITabBarItem *payItem;

@end
