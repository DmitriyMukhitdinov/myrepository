//
//  DTViewController.h
//  debts
//
//  Created by Evgeny on 25.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarButton.h"
@interface DTViewController : UIViewController
@property (assign, nonatomic) BOOL isModal;
- (void)back;
@end
