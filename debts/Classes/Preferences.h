//
//  Preferences.h
//  debts
//
//  Created by Evgeny on 18.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Preferences : NSObject

+ (NSDate *)defaultTime;
+ (void)setDefaultTime:(NSDate *)time;

+ (BOOL)isDecimalEnabled;
+ (void)setDecimalEnabled:(NSNumber *)enabled;

+ (NSString *)defaultCurrencyCode;
+ (void)setDefaultCurrencyCode:(NSString *)code;

+ (BOOL)isShowFinishedEnabled;
+ (void)setFinishedShowEnabled:(NSNumber *)enabled;

+ (void)changeAllNotificationsToTime:(NSDate *)time;
@end
