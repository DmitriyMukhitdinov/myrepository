//
//  AssetVC.h
//  debts
//
//  Created by Evgeny on 25.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DTTableViewController.h"
#import "ContractorsTVC.h"
#import "ImageCropController.h"
#import "CurrencyViewController.h"
#import "ContractorsTVC.h"
#import "CurrencyViewController.h"
#import "NZCircularImageView.h"
#import "UIPlaceHolderTextView.h"
#import "CalcVC.h"

@protocol AssetVCDelegate <NSObject>
@optional
- (void) updateTable;
- (void) didSaveAsset;
@end

@interface AssetVC : DTTableViewController
<UITextFieldDelegate,
UIGestureRecognizerDelegate,
ContractorTableViewDelegate,
CurrencyViewControllerDelegate,
UITextViewDelegate,
ImageCropControllerDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
CalcVCDelegate>
{
    __weak id<AssetVCDelegate> delegate;
}
@property (nonatomic, weak)   id<AssetVCDelegate> delegate;
@property (nonatomic, strong) Asset *asset;

@property (weak, nonatomic) IBOutlet UITableViewCell *ownerCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *historyCell;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *infoTextView;
@property (weak, nonatomic) IBOutlet NZCircularImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *addPhotoLabel;



@end
