//
//  BarButton.m
//  debts
//
//  Created by Evgeny on 22.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "BarButton.h"

@implementation BarButton

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    UIImage *image = [[UIImage imageNamed:@"whiteBarButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(6.0f, 7.0f, 6.0f, 7.0f)];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    [self setBackgroundImage:image forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor colorFromHexString:kDarkGrayColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorFromHexString:kDarkGrayColor] forState:UIControlStateHighlighted];
    [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:11.0f]];
}


- (UIEdgeInsets)alignmentRectInsets {
    UIEdgeInsets insets;
    insets = UIEdgeInsetsMake(0, 9.0f, 0, 9.0f);
    return insets;
}

@end
