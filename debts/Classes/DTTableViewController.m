//
//  DTTableViewController.m
//  debts
//
//  Created by Evgeny on 25.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "DTTableViewController.h"
#import "BackButton.h"
#import "BarButton.h"


@interface DTTableViewController ()

@end

@implementation DTTableViewController
@synthesize isModal = _isModal;

 
#pragma mark - Lifecycle
 
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //модальное представление
    if (self.isModal){
        BarButton *rightButton = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 81.0f, 25.0f)];
        [rightButton setTitle:LSTRING(@"Close") forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.leftBarButtonItem = rightBarButton;
    }
    else{
        BackButton *btn = [BackButton button];
        [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.leftBarButtonItem = backButton;
    }

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = 60.0f;
    self.tableView.sectionHeaderHeight = 20.0f;
}


 
#pragma mark - Navigation
 
- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}




@end
