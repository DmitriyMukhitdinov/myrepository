//
//  ImagePreviewVC.m
//  debts
//
//  Created by Evgeny on 22.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "ImagePreviewVC.h"


@interface ImagePreviewVC ()

@end

@implementation ImagePreviewVC
@synthesize image = _image;
@synthesize imageView = _imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.imageView.image = self.image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 
#pragma mark - IBOutlets
 
- (IBAction)takePhoto:(id)sender {
}
- (IBAction)selectFromGallery:(id)sender {
}
- (IBAction)cancel:(id)sender {
}
- (IBAction)edit:(id)sender {
//    DZNPhotoPickerController *picker = [[DZNPhotoPickerController alloc] init];
//    picker.editingMode = DZNPhotoEditViewControllerCropModeCircular;
//    picker.allowsEditing = YES;
//    picker.delegate = self;
//    [self presentViewController:picker animated:YES completion:NO];
    
    //DZNPhotoEditViewController *viewController = [[DZNPhotoEditViewController alloc] initWithImage:self.image cropMode:DZNPhotoEditViewControllerCropModeCircular];
    //[self presentViewController:viewController animated:YES completion:nil];
    //[self.navigationController pushViewController:viewController animated:YES];
}




@end
