//
//  TableSection.h
//  debts
//
//  Created by Evgeny on 23.01.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableSection : UIView
@property (nonatomic, strong) NSString *titleLabel;
@end
