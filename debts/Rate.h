//
//  Rate.h
//  debts
//
//  Created by Evgeny on 02.02.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataStore.h"

@interface Rate : NSObject
@property (strong, nonatomic) NSOperationQueue *queue;
@property (nonatomic, strong) NSMutableArray *loadedCodes;

+ (id)sharedInstance;
- (void)updateAllRates;
- (void)deletePList;
- (NSNumber *)rateForCurrencyCode:(NSString *)code;
- (void)setUpRatesPlist;
- (void)updateRateWithCode:(NSString *)code;
@end
