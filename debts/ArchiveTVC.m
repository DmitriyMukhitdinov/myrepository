//
//  ArchiveTVC.m
//  debts
//
//  Created by Evgeny on 31.05.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

#import "ArchiveTVC.h"
#import "Amount.h"
#import "Debenture.h"
#import "Contractor.h"
#import "DebtCell.h"
#import "DebtCellPhoto.h"
#import "AssetCellViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Rate.h"
#import "DebtVC.h"

static NSString *kAssetDebtCellID    = @"AssetCell";
static NSString *kMoneyDebtCellID    = @"moneyCell";

@interface ArchiveTVC ()
@property (nonatomic, strong) NSArray *tableData;
@end

@implementation ArchiveTVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateTable];
    UINib *customCellNib1 = [UINib nibWithNibName:@"AssetCellPhoto" bundle:nil];
    [self.tableView registerNib:customCellNib1 forCellReuseIdentifier:kAssetDebtCellID];
    
    UINib *customCellNib2 = [UINib nibWithNibName:@"debtCellPhoto" bundle:nil];
    [self.tableView registerNib:customCellNib2 forCellReuseIdentifier:kMoneyDebtCellID];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;

}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateTable];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (void)updateTable
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type == %@ AND finished == 1",DebtType_toString[self.type]];
    self.tableData = [Debenture MR_findAllSortedBy:@"created_at" ascending:NO withPredicate:predicate];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Debenture *debt = self.tableData[indexPath.row];
    //Имущество
    if (self.type == DebtTypeAssetMine || self.type == DebtTypeAssetSomeone)
    {
        AssetCellViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAssetDebtCellID];
        Asset *asset = debt.asset;
        cell = [tableView dequeueReusableCellWithIdentifier:kAssetDebtCellID];
        
        if (asset.photo) {
            dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(concurrentQueue, ^{
                UIImage *image = [UIImage imageFromDocsByName:asset.photo];
                if (image != nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell.photoImageView setImage:image];
                    });
                }
            });
        }
        else{
            cell.photoImageView.image = [UIImage imageNamed:@"assetPlaceholder"];
        }
        
        if (self.isEditing) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.titleLabel.text = asset.title;
        cell.backgroundColor = [UIColor whiteColor];
        
        if (asset.history.count>0) {
            
            if ([debt.finished isEqual:[NSNumber numberWithInt:1]]) {
                //Находится у владельца
                cell.dateLabel.text = @"";
                cell.contractorLabel.text = @"возвращено";
                cell.backgroundColor = [UIColor whiteColor];
                /*
                 if (asset.owner == [AppHelper contractorMe])
                 cell.contractorLabel.text = @"у меня";
                 else{
                 NSString *name = [NSString stringWithFormat:@"%@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@"у ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                 cell.contractorLabel.text = name;
                 }
                 */
            }
            else{
                //Вещь находится у контрагента
                if (debt.deadline && [debt.deadline timeIntervalSince1970]<[[NSDate date] timeIntervalSince1970] && debt.finished!=[NSNumber numberWithInt:1]) {
                    //прострочено
                    cell.backgroundColor = [UIColor colorFromHexString:kOutdatedCellColor];
                    cell.dateLabel.highlighted = YES;
                    cell.titleLabel.highlighted = YES;
                    cell.contractorLabel.highlighted = YES;
                }
                else{
                    cell.backgroundColor = [UIColor whiteColor];
                    cell.dateLabel.highlighted = NO;
                    cell.titleLabel.highlighted = NO;
                    cell.contractorLabel.highlighted = NO;
                }
                
                if (debt.asset.owner == [AppHelper contractorMe]) {
                    NSString *name = [NSString stringWithFormat:@"у %@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                    cell.contractorLabel.text = name;
                }
                else{
                    NSString *name = [NSString stringWithFormat:@"владелец %@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                    cell.contractorLabel.text = name;
                }
                cell.dateLabel.text = [[AppHelper dateFormatter] stringFromDate:debt.deadline];
            }
        }
        else{
            //Находится у владельца
            cell.dateLabel.text = @"";
            if (asset.owner == [AppHelper contractorMe])
                cell.contractorLabel.text = @"у меня";
            else{
                NSString *name = [NSString stringWithFormat:@"%@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
                cell.contractorLabel.text = name;
            }
        }
        return cell;
    }
    else{
        DebtCellPhoto *cell = [tableView dequeueReusableCellWithIdentifier:kMoneyDebtCellID];
        if (debt.contractor.photo) {
            cell.photoImageView.image = [UIImage imageFromDocsByName:debt.contractor.photo];
        }
        else{
            cell.photoImageView.image = [UIImage imageNamed:@"photoPlaceholder"];
        }
        NSString *name = [NSString stringWithFormat:@"%@ %@", (debt.contractor.name!=nil)?debt.contractor.name:@" ", (debt.contractor.lastName!=nil)?debt.contractor.lastName:@" "];
        cell.nameLabel.text = name;
        if (debt.deadline) {
            cell.dateLabel.text =[NSString stringWithFormat:@"возврат %@", [[AppHelper dateFormatter]stringFromDate:debt.deadline]];
        }
        else{
            cell.dateLabel.text = LSTRING(@"Without refund date");
        }
        cell.amountLabel.text = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:[debt debtBalance]];
        if (debt.deadline && [debt.deadline timeIntervalSince1970]<[[NSDate date] timeIntervalSince1970]
            && debt.finished!=[NSNumber numberWithInt:1]) {
            //прострочено
            cell.backgroundColor = [UIColor colorFromHexString:kOutdatedCellColor];
            cell.dateLabel.highlighted = YES;
            cell.amountLabel.highlighted = YES;
            cell.nameLabel.highlighted = YES;
        }
        else if ([debt.finished isEqual:[NSNumber numberWithInt:1]]){
            cell.dateLabel.text = @"возвращено";
            cell.amountLabel.text = [[AppHelper currencyFormatterWithCode:debt.total.code] stringFromNumber:debt.money];
        }
        else{
            cell.backgroundColor = [UIColor whiteColor];
            cell.dateLabel.highlighted = NO;
            cell.amountLabel.highlighted = NO;
            cell.nameLabel.highlighted = NO;
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"DebtVC" sender:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,1.5555* NSEC_PER_SEC), dispatch_get_main_queue(),^{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    });
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DebtVC"]) {
        DebtVC *debtVC = [segue destinationViewController];
        NSInteger inx = [self.tableView indexPathForSelectedRow].row;
        if (inx < self.tableData.count) {
            debtVC.debt = self.tableData[inx];
        }
        debtVC.type = self.type;
        debtVC.delegate = self;
    }
}

@end
