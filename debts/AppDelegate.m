//
//  AppDelegate.m
//  debts
//
//  Created by Evgeny on 12.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "AppDelegate.h"


#import "Contractor.h"
#import "Debenture.h"
#import "Rate.h"
#import <Crashlytics/Crashlytics.h>
#import "PushTracking.h"
#import "UIAlertView+Blocks.h"
#import "DebtVC.h"
#import "CJPAdController.h"
#import "IAPHelper.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Crashlytics startWithAPIKey:@"2ea542cec20f90f6f294173536b62b5324071120"];
    [application setStatusBarHidden:NO];
    [application setStatusBarStyle:UIStatusBarStyleDefault];
    self.window.backgroundColor = [UIColor colorFromHexString:kGrayColor];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_Storyboard"
                                                         bundle:[NSBundle mainBundle]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    
    if (![[Engine appEngine] isPaid]) {
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"RootNavController"];
        [CJPAdController sharedInstance].adNetworks = @[@(CJPAdNetworkiAd), @(CJPAdNetworkAdMob)];
        [CJPAdController sharedInstance].adPosition = CJPAdPositionBottom;
        [CJPAdController sharedInstance].initialDelay = 2.0;
        // AdMob specific
        [CJPAdController sharedInstance].adMobUnitID = @"ca-app-pub-1798585663224500/9440696878";
        [[CJPAdController sharedInstance] startWithViewController:viewController];
        self.window.rootViewController = [CJPAdController sharedInstance];
    }
    
    //Setup MagicalRecord
    [MagicalRecord setupAutoMigratingCoreDataStack];
    //[[Rate sharedInstance] setUpRatesPlist];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
                   ^{
                       [[Rate sharedInstance] updateAllRates];
                   });
    
    if ([Contractor MR_countOfEntities] == 0)
    {
        Contractor *me = [Contractor MR_createEntity];
        me.name = @"Я";
        me.type = @"me";
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
    }
    
    // Override point for customization after application launch.

    
    //UINavigationBar
    //Title font
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor blackColor],NSForegroundColorAttributeName,
                                               FONT(15.0f), NSFontAttributeName,
                                               nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    
    [[UITableView appearance] setSeparatorColor:[UIColor colorFromHexString:kSeparatorColor]];
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"backArraw"]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"backArraw"]];
    [[UINavigationBar appearance] setTintColor:[UIColor colorFromHexString:kMainColor]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor colorFromHexString:kMainColor]];
    
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor colorFromHexString:kDarkGrayColor]];
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:FONT(12.0f)];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorFromHexString:kMainColor];
    [[UITableViewCell appearance] setSelectedBackgroundView:bgColorView];
    

    [[UITabBar appearance] setShadowImage:[UIImage new]];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],NSForegroundColorAttributeName,
                                               FONT(9.0f), NSFontAttributeName,
                                               nil];
    [[UITabBarItem appearance] setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    
    if (launchOptions != nil) {
        // Launched from push notification
        UILocalNotification *localNotification = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
        Debenture *debt = [PushTracking debtForUserInfo:localNotification.userInfo];
        if (!debt) {
            return YES;
        }
        [self.window makeKeyAndVisible];
        
        DebtVC *debViewController;
        if ([debt.type isEqualToString:DebtType_toString[DebtTypeMoneyMine]] || [debt.type isEqualToString:DebtType_toString[DebtTypeMoneySomeone]]) {
            debViewController = [storyboard instantiateViewControllerWithIdentifier:@"DebtScene"];
        }
        else{
            debViewController = [storyboard instantiateViewControllerWithIdentifier:@"DebtScene"];
        }
        
        DebtType types[4] = {DebtTypeMoneyMine, DebtTypeMoneySomeone, DebtTypeAssetMine, DebtTypeAssetSomeone};
        for (int i=0; i<4; i++) {
            if ([debt.type isEqualToString:DebtType_toString[types[i]]]) {
                debViewController.type = types[i];
            }
        }
        debViewController.debt = debt;
        debViewController.isModal = YES;
        MainNavigationController *navController = [[MainNavigationController alloc] initWithRootViewController:debViewController];
        
        [self.window makeKeyAndVisible];
        [self.window.rootViewController presentViewController:navController animated:YES completion:nil];
    }
    
    
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    Debenture *debt = [PushTracking debtForUserInfo:notification.userInfo];
    if (!debt) {
        return;
    }
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        //opened from a push notification when the app was on background
        if(debt)[AppHelper pushViewControllerForDebt:debt];
    }
    else{
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:notification.alertBody
                                                     message:@"Перейти к договой расписке"
                                                    delegate:self
                                           cancelButtonTitle:@"Нет"
                                           otherButtonTitles:@"Да", nil];
        av.didDismissBlock=^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == [alertView cancelButtonIndex]) {
                NSLog(@"Cancelled");
            } else if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Да"]) {
                
                if(debt)[AppHelper pushViewControllerForDebt:debt];
            }
        };
        [av show];
    }
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


- (void)productPurchased:(NSNotification *)notification {
    NSString * productIdentifier = notification.object;
    NSLog(@"product %@ did purchased", productIdentifier);
    [[CJPAdController sharedInstance] removeAdsAndMakePermanent:YES andRemember:YES];
}




@end
