//
//  ContractorCell.h
//  debts
//
//  Created by Evgeny on 19.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface ContractorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
