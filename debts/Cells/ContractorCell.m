//
//  ContractorCell.m
//  debts
//
//  Created by Evgeny on 19.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import "ContractorCell.h"

@implementation ContractorCell
@synthesize photoImageView  = _photoImageView;
@synthesize nameLabel       = _nameLabel;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
