//
//  AssetsTableViewController.h
//  debts
//
//  Created by Evgeny on 15.12.13.
//  Copyright (c) 2013 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetVC.h"
#import "Asset.h"
#import "DTViewController.h"
@protocol AssetsTableViewDelegate <NSObject>

@optional
- (void) didSelectAsset:(Asset *)asset;
@end

@interface AssetsTVC : DTTableViewController
<AssetVCDelegate,
UITableViewDataSource,
UITableViewDelegate>
{
    __weak id<AssetsTableViewDelegate> delegate;
}
@property (nonatomic, weak)   id<AssetsTableViewDelegate> delegate;
@property (nonatomic, assign) DebtType debtType;
@property (nonatomic, assign) BOOL isNoSelect;
@end
