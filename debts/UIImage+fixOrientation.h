//
//  UIImage+fixOrientation.h
//  maaas
//
//  Created by Evgeny Nazarov on 24.04.13.
//  Copyright (c) 2013 mac_1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)
- (UIImage *)fixOrientation;
@end
